package com.tydic.recruit.utils;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/21 23:30
 */
public class AllPriceUtil {

    private static double allPriceMoney;
    public static Double allPrice(Integer number,double price){
        allPriceMoney = number*price;
        return allPriceMoney;
    }
}
