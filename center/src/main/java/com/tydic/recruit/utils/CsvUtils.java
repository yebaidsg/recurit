package com.tydic.recruit.utils;

import com.alibaba.fastjson.JSONObject;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.tydic.recruit.utils.bean.CsvBean;
import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

/**
 * @标题 CsvUtils
 * @说明 csv文件转化为对象
 * @作者 胡中宝
 * @时间 2021/5/2 18:02
 */
public class CsvUtils {
    /**
     * 日志对象
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(CsvUtil.class);
    /**
     *
     */
    /**
     * 解析csv文件并转成bean
     * @param file csv文件
     * @param clazz 类
     * @param <T> 泛型
     * @return 泛型bean集合
     */
    public <T> List<T> getCsvData(MultipartFile file, Class<T> clazz) {
        InputStreamReader in = null;
        try {
            in = new InputStreamReader(file.getInputStream(), "utf8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(clazz);

        CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(in)
                .withSeparator(',')
                .withQuoteChar('\'')
                .withMappingStrategy(strategy).build();
        return csvToBean.parse();
    }

    public static void main(String[] args) throws IOException {
        CsvUtils csvUtils = new CsvUtils();
        File file = new File("D:\\Gitworkplace\\recurity\\51job15.csv");
        FileInputStream fileInputStream = new FileInputStream(file);

        MultipartFile multipartFile = new MockMultipartFile(file.getName(), file.getName(),
                ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
        List<CsvBean> csvBeans = csvUtils.getCsvData(multipartFile,CsvBean.class);
        System.out.println(JSONObject.toJSONString(csvBeans));
    }
}
