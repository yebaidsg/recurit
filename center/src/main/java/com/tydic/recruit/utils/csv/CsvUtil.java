package com.tydic.recruit.utils.csv;

import com.alibaba.fastjson.JSONObject;
import com.tydic.recruit.utils.bean.CsvBean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 CsvTest1
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/29 1:15
 */
public class CsvUtil {
    public static List<CsvBean> readCsv(String inpath) {
        List<CsvBean> list = new ArrayList<>();
        try {
            File file = new File(inpath);
            if (!file.exists()) {
                System.out.println("文件不存在！");
            } else {
                System.out.println("文件存在！");
                BufferedReader reader = new BufferedReader(new FileReader(inpath));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] row = line.split(",", -1);
                    CsvBean infos = new CsvBean();
                    infos.setJobTitle(row[0]);
                    infos.setCompanyName(row[1]);
                    infos.setWorkPlace(row[2]);
                    infos.setEducation(row[3]);
                    infos.setExperience(row[4]);
                    infos.setQuantity(row[5]);
                    infos.setJobInfo(row[6]);
                    infos.setLowSalary(row[7]);
                    infos.setHighSalary(row[8]);
                    list.add(infos);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    /**

     * 读取csv文件用list对象存储的公共调用方法

     * @param inpath csv文件存储路径

     * @param obj 和csv文件对应的实体类

     * @return 返回List<Object>对象

     */
    public static List<Object> readCsv2(String inpath, Object obj) {
        List<Object> list = new ArrayList<Object>();
        try {
            File file = new File(inpath);
            if (!file.exists()) {
                System.out.println("文件不存在！");
            } else {
                System.out.println("文件存在！");
                BufferedReader reader = new BufferedReader(new FileReader(inpath));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] row = line.split("\\|", -1);
                    Class clazz = obj.getClass();
                    Object infos = clazz.newInstance();
                    Field[] fs = infos.getClass().getDeclaredFields();
                    for (int i = 0; i < fs.length; i++) {
                        Field f = fs[i];
                        f.setAccessible(true);
                        String type = f.getType().toString();
                        if (type.endsWith("String")) {
                            f.set(infos, row[i]);
                        }
                    }
                    list.add(infos);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        String inpath = "D:\\Gitworkplace\\recurit\\51job15.csv";
        List<CsvBean> list2 = CsvUtil.readCsv(inpath);
        System.out.println(JSONObject.toJSONString(list2));
//        for (Object rule : list2) { // 输出
//            System.out.println(rule.toString());
//        }
    }
}
