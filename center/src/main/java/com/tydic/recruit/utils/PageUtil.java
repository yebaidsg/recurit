package com.tydic.recruit.utils;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tydic.recruit.common.base.bo.ReqPageBo;
import com.tydic.recruit.common.base.bo.RspPageBo;
import com.tydic.recruit.common.base.bo.RspPageDataBo;

import java.util.List;

/**
 * 描述
 *
 * @author tgy
 * @date 2020/4/10 18:01
 */
public class PageUtil {

    /**
     * 正序
     */
    public static final String ORDER_ASC = "asc";
    /**
     * 逆序
     */
    public static final String ORDER_DESC = "desc";


    /**
     * 描述:开始翻页
     *
     * @return com.github.pagehelper.Page<java.lang.Object>
     * @param: [pageBo] 入参
     * @author tgy
     * @date 2020/5/7 13:12
     */
    public static Page<Object> startPage(ReqPageBo pageBo) {
        Page<Object> page = PageHelper.startPage(pageBo.getPageNo(), pageBo.getPageSize());
        String sortStr = "";
        if (!StrUtil.isEmpty(pageBo.getSortName())) {
            String orderName = pageBo.getSortName();
            if (!ORDER_ASC.equals(orderName) && !ORDER_DESC.equals(orderName)) {
                throw new IllegalArgumentException("sortName只能是(" + ORDER_ASC + "," + ORDER_DESC + ")之一");
            }
            sortStr = pageBo.getSortName();
            if (!StrUtil.isEmpty(pageBo.getSortOrder())) {
                sortStr += " " + pageBo.getSortOrder();
            }
            page.setOrderBy(sortStr);
        }
        return page;
    }

    /**
     * 描述:填充到出参
     *
     * @return void
     * @param: [rspPageBo, list] 入参
     * @author tgy
     * @date 2020/5/7 13:13
     */
    public static void fillPage(RspPageBo rspPageBo, List list) {
        fillPageWithRspPageDataBo(rspPageBo.getData(), list);
    }


    /**
     * 描述:填充到出参
     *
     * @return void
     * @param: [rspPageDataBo, list] 入参
     * @author tgy
     * @date 2020/5/7 13:13
     */
    public static void fillPageWithRspPageDataBo(RspPageDataBo rspPageDataBo, List list) {
        PageInfo pageInfo = PageInfo.of(list);
        rspPageDataBo.setPageNo(pageInfo.getPageNum());
        rspPageDataBo.setRecordsTotal((int) pageInfo.getTotal());
        rspPageDataBo.setTotal(pageInfo.getPages());
    }

}
