package com.tydic.recruit.utils.recommed;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author huzb
 * @标题 UserRecommed
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 18:30
 */
@Data
public class UserRecommed implements Serializable {
    private List<String> like;
    private double seem;//相似度

    public List<String> getLike() {
        return like;
    }

    public void setLike(List<String> like) {
        this.like = like;
    }

    public double getSeem() {
        return seem;
    }

    public void setSeem(double seem) {
        this.seem = seem;
    }
}
