package com.tydic.recruit.utils.recommed;

import java.util.*;

/**
 * @author huzb
 * @标题 Recommed
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 18:31
 */
public class Recommed {
    public static void main(String[] args) {
        UserRecommed userM1 = new UserRecommed();
        UserRecommed userM2 = new UserRecommed();
        UserRecommed userM3 = new UserRecommed();
        UserRecommed userM4 = new UserRecommed();
        List<String> list1 = Arrays.asList("5","5","5");
        List<String> list2 = Arrays.asList("4","5","6","1","0");
        List<String> list3 = Arrays.asList("2","1","1","2","5");
        List<String> list4 = Arrays.asList("4","5","6","6");
        userM1.setLike(list1);
        userM2.setLike(list2);
        userM3.setLike(list3);
        userM4.setLike(list4);
        List<UserRecommed> list = new ArrayList<>();
        list.add(userM1);
        list.add(userM2);
        list.add(userM3);
        list.add(userM4);
        List<UserRecommed> userMS = searchUserM(userM1, list);
        for(int i = 0 ; i < userMS.size() ; i++){
            userMS.get(i).setSeem(compare(userM1,userMS.get(i)));
            System.out.println(userMS.get(i).toString());
        }

        Map<String, Double> recommend = recommend(userM1, userMS);
        System.out.println(recommend);
    }



    /**
     *
     * @param userM1 当前登录用户
     * @param userM2 要进行相似度比较的用户
     * @return 相似度
     *      //比较两个集合，相同的即放在union，又放在intersection
     *     //不同的，放在union
     *     //用杰卡德相似系数求相似度--杰卡德系数 = 交集/并集
     */
    public static double compare(UserRecommed userM1,UserRecommed userM2){
        //并集
        List<String> union = new ArrayList<>();
        //交集
        List<String> intersection = new ArrayList<>();

        List<String> like1 = userM1.getLike();
        List<String> like2 = userM2.getLike();

        //将userM1的like先放入并集
        for(int i = 0 ; i < like1.size() ; i++){
            union.add(like1.get(i));
        }
        //将like2与并集进行对比
        for(int i = 0 ; i < like2.size() ; i++){
            if(union.contains(like2.get(i))){
                //如果并集中存在，加入到交集中
                intersection.add(like2.get(i));
            }else{
                //并集中不存在，加到并集中
                union.add(like2.get(i));
            }
        }
        //相似度
        double likes = (double)intersection.size()/union.size();

        return likes;
    }

    /**
     *
     * @param userM 当前用户
     * @param list 与当前用户相关的用户
     * @return 对当前用户的电影的推荐度
     */
    public static Map<String,Double> recommend(UserRecommed userM, List<UserRecommed> list){
        List<String> recommends = new ArrayList<>();
        Map<String,Double> map = new HashMap<>();
        for(int i = 0 ; i < list.get(0).getLike().size() ; i++){
            recommends.add(list.get(0).getLike().get(i));
            map.put(list.get(0).getLike().get(i),list.get(0).getSeem());
        }

        for(int j = 1 ; j < list.size() ; j++){
            for(int i = 0 ; i < list.get(j).getLike().size() ; i++){
                //如果recommends里面存在，即有交集
                if(recommends.contains(list.get(j).getLike().get(i))){
                    //如果有，修改值
                    map.replace(list.get(j).getLike().get(i),map.get(list.get(j).getLike().get(i))+list.get(j).getSeem());
                }else{
                    //recommends中不存在
                    map.put(list.get(j).getLike().get(i),list.get(j).getSeem());
                }
            }
        }

        return map;
    }

    /**
     *
     * @param userM  当前登录用户
     * @param list   全部用户
     * @return 与当前登录用户相关的用户
     */
    public static List<UserRecommed> searchUserM(UserRecommed userM,List<UserRecommed> list){
        List<String> like = new ArrayList<>();//存放当前登录用户喜欢的类型
        List<UserRecommed> listU = new ArrayList<>();//存放与当前登录用户相关的用户

        for(int i = 0 ; i < userM.getLike().size() ; i++){
            like.add(userM.getLike().get(i));
        }

        for(int i = 0 ; i < list.size() ; i++){
            for(int j = 0 ; j < list.get(i).getLike().size() ; j++){
                if(like.contains(list.get(i).getLike().get(j))){
                    listU.add(list.get(i));
                    break;
                }else{
                    continue;
                }
            }
        }

        return listU;
    }
}
