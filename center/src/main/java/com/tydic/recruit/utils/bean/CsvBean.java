package com.tydic.recruit.utils.bean;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

import java.io.Serializable;

/**
 * @标题 CsvBean
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 18:20
 */
@Data
public class CsvBean implements Serializable {
    /**
     * 职位名
     */
    @CsvBindByName(column = "JobTitle")
    private String jobTitle;
    /**
     * 公司名称
     */
    @CsvBindByName(column = "CompanyName")
    private String companyName;
    /**
     * 工作地点
     */
    @CsvBindByName(column = "Workplace")
    private String workPlace;
    /**
     * 需求人数
     */
    @CsvBindByName(column = "quantity")
    private String quantity;
    /**
     * 学历
     */
    @CsvBindByName(column = "education")
    private String education;
    /**
     * 最低薪资
     */
    @CsvBindByName(column = "LowSalary")
    private String lowSalary;
    /**
     * 最高薪资
     */
    @CsvBindByName(column = "HighSalary")
    private String highSalary;
    /**
     * 职位描述
     */
    @CsvBindByName(column = "JobInfo")
    private String jobInfo;
    /**
     * 经验
     */
    @CsvBindByName(column = "experience")
    private String experience;
    /**
     * 职位来源
     */
    private String positionResource;

}
