package com.tydic.recruit.utils.es;

import lombok.Data;

import java.util.List;

@Data
public class EsPageRspBo<T> {

    //总条数
    private int total;

    //当前页
    private int cPage;

    //总页数
    private int tPage;

    private List<T> data;

}
