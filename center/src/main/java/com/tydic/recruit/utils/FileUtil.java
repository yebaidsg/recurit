package com.tydic.recruit.utils;

import com.ohaotian.plugin.base.exception.ZTBusinessException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @标题 FileUtil
 * @说明 文件地址转话为文件对象
 * @作者 胡中宝
 * @时间 2021/5/2 18:16
 */
public class FileUtil {
    public File getFileByUrl(String fileUrl) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        BufferedOutputStream stream = null;
        InputStream inputStream = null;
        File file = null;
        try {
            URL imageUrl = new URL(fileUrl);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            inputStream = conn.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            file = File.createTempFile("file", ".pdf");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            stream = new BufferedOutputStream(fileOutputStream);
            stream.write(outStream.toByteArray());
        } catch (Exception e) {
            throw new ZTBusinessException("url转文件失败");
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (stream != null) {
                    stream.close();
                }
                outStream.close();
            } catch (Exception e) {
            }
        }
        return file;
    }

    public static void main(String[] args) {
        FileUtil fileUtil = new FileUtil();
        File file = fileUtil.getFileByUrl("E:\\QQ\\QQDownLoad\\final1.csv");
        System.out.println(file);
    }
}
