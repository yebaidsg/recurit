package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateCertificateAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateCertificateAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateCertificateAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:32
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateCertificateAbilityServiceImpl implements GraduationUpdateCertificateAbilityService {
    @Autowired
    private GraduationUpdateCertificateBusiService graduationUpdateCertificateBusiService;
    @Override
    public GraduationUpdateCertificateAbilityRspBO updateCertificate(GraduationUpdateCertificateAbilityReqBO reqBO) {
        GraduationUpdateCertificateBusiReqBO busiReqBO = new GraduationUpdateCertificateBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateCertificateBusiRspBO busiRspBO = graduationUpdateCertificateBusiService.updateCertificate(busiReqBO);
        GraduationUpdateCertificateAbilityRspBO rspBO = new GraduationUpdateCertificateAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
