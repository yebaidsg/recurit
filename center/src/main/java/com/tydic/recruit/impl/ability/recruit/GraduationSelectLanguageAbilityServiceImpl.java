package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectLanguageAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectLanguageAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectLanguageAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:10
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectLanguageAbilityServiceImpl implements GraduationSelectLanguageAbilityService {
    @Autowired
    private GraduationSelectLanguageBusiService graduationSelectLanguageBusiService;
    @Override
    public GraduationSelectLanguageAbilityRspBO selectLanguage(GraduationSelectLanguageAbilityReqBO reqBO) {
        GraduationSelectLanguageBusiReqBO busiReqBO = new GraduationSelectLanguageBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectLanguageBusiRspBO busiRspBO = graduationSelectLanguageBusiService.selectLanguage(busiReqBO);
        GraduationSelectLanguageAbilityRspBO rspBO = new GraduationSelectLanguageAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
