package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.ProjectExperienceMapper;
import com.tydic.recruit.dao.recurit.po.ProjectExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectProjectExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:25
 */
@Service("GraduationSelectProjectExperienceBusiService")
public class GraduationSelectProjectExperienceBusiServiceImpl implements GraduationSelectProjectExperienceBusiService {
    @Autowired
    private ProjectExperienceMapper projectExperienceMapper;
    @Override
    public GraduationSelectProjectExperienceBusiRspBO selectProject(GraduationSelectProjectExperienceBusiReqBO reqBO) {
        GraduationSelectProjectExperienceBusiRspBO rspBO = new GraduationSelectProjectExperienceBusiRspBO();
        ProjectExperiencePO projectExperiencePO = new ProjectExperiencePO();
        BeanUtils.copyProperties(reqBO,projectExperiencePO);
        ProjectExperiencePO experiencePO = projectExperienceMapper.select(projectExperiencePO);
        if (null != experiencePO){
            BeanUtils.copyProperties(experiencePO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
