package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.PersonalInformationMapper;
import com.tydic.recruit.dao.recurit.po.PersonalInformationPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @标题 GraduationRecruitAddResumeBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 22:46
 */
@Service("GraduationRecruitAddResumeBusiService")
public class GraduationRecruitAddResumeBusiServiceImpl implements GraduationRecruitAddResumeBusiService {

    @Autowired
    private PersonalInformationMapper personalInformationMapper;
    @Override
    public GraduationRecruitAddResumeBusiRspBO addResume(GraduationRecruitAddResumeBusiReqBO reqBO) {
        PersonalInformationPO personalInformationPO = new PersonalInformationPO();
        BeanUtils.copyProperties(reqBO,personalInformationPO);
        if (null != reqBO.getAddress() && reqBO.getAddress().length>0){
            personalInformationPO.setPersonProvinceCode(reqBO.getAddress()[0]);
            personalInformationPO.setPersonCityCode(reqBO.getAddress()[1]);
            personalInformationPO.setPersonAreaCode(reqBO.getAddress()[2]);
        }
        if (null != reqBO.getJuzhu() && reqBO.getJuzhu().length >0){
            personalInformationPO.setPersonAddressProvinceCode(reqBO.getJuzhu()[0]);
            personalInformationPO.setPersonAddressCityCode(reqBO.getJuzhu()[1]);
            personalInformationPO.setPersonAddressAreaCode(reqBO.getJuzhu()[2]);
        }
        /**
         * 通过userId查询个人信息
         */
        PersonalInformationPO po = new PersonalInformationPO();
        po.setUserId(reqBO.getUserId());
        PersonalInformationPO informationPO = personalInformationMapper.select(po);
        if (null != informationPO){
            int updateCheck = personalInformationMapper.update(personalInformationPO);
            if (updateCheck <1){
                throw new BusinessException("8888","更新失败");
            }
        } else {
            personalInformationPO.setPersonId(Sequence.getInstance().nextId());
            int insertCheck = personalInformationMapper.insert(personalInformationPO);
            if (insertCheck < 1){
                throw new BusinessException("8888","插入失败");
            }
        }
        GraduationRecruitAddResumeBusiRspBO rspBO = new GraduationRecruitAddResumeBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
