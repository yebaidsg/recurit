package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.EducationalExperienceMapper;
import com.tydic.recruit.dao.recurit.po.EducationalExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitUpdateEducationBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:10
 */
@Service("GraduationRecruitUpdateEducationBusiService")
public class GraduationRecruitUpdateEducationBusiServiceImpl implements GraduationRecruitUpdateEducationBusiService {
    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;
    @Override
    public GraduationRecruitUpdateEducationBusiRspBO updateEducation(GraduationRecruitUpdateEducationBusiReqBO reqBO) {
        EducationalExperiencePO educationalExperiencePO = new EducationalExperiencePO();
        BeanUtils.copyProperties(reqBO,educationalExperiencePO);
        int update = educationalExperienceMapper.update(educationalExperiencePO);
        if (update < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationRecruitUpdateEducationBusiRspBO rspBO = new GraduationRecruitUpdateEducationBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
