package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectJobIntentionalAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitSelectJobIntentionalAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitSelectJobIntentionalAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:39
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitSelectJobIntentionalAbilityServiceImpl implements GraduationRecruitSelectJobIntentionalAbilityService {
    @Autowired
    private GraduationRecruitSelectJobIntentionalBusiService graduationRecruitSelectJobIntentionalBusiService;
    @Override
    public GraduationRecruitSelectJobIntentionalAbilityRspBO selectJob(GraduationRecruitSelectJobIntentionalAbilityReqBO reqBO) {
        GraduationRecruitSelectJobIntentionalBusiReqBO busiReqBO = new GraduationRecruitSelectJobIntentionalBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitSelectJobIntentionalBusiRspBO busiRspBO = graduationRecruitSelectJobIntentionalBusiService.selectJob(busiReqBO);
        GraduationRecruitSelectJobIntentionalAbilityRspBO rspBO = new GraduationRecruitSelectJobIntentionalAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
