package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.busi.recruit.PositionRecommendBusiService;
import com.tydic.recruit.api.busi.recruit.bo.EvaluateCommendBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionRecommendBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionRecommendBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.EvaluateCommendPO;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import java.io.*;
import java.util.*;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import java.io.File;
import java.io.IOException;

/**
 * @author huzb
 * @标题 PositionRecommendBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:22
 */
@HTServiceImpl
@Slf4j
public class PositionRecommendBusiServiceImpl implements PositionRecommendBusiService {
    //临近的用户个数
    final static int NEIGHBORHOOD_NUM = 2;
    //推荐物品的最大个数
    final static int RECOMMENDER_NUM = 3;
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public PositionRecommendBusiRspBO selectCommend(PositionRecommendBusiReqBO reqBO) {
        PositionRecommendBusiRspBO rspBO = new PositionRecommendBusiRspBO();
        PositionPO positionPO = new PositionPO();
        List<EvaluateCommendPO> evaluateCommendPOS = positionMapper.selectAll(positionPO);
        /**
         * 类型转话
         */
        List<EvaluateCommendBO> commendBOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(evaluateCommendPOS)){
            for (EvaluateCommendPO evaluateCommendPO : evaluateCommendPOS) {
                EvaluateCommendBO evaluateCommendBO = new EvaluateCommendBO();
                BeanUtils.copyProperties(evaluateCommendPO,evaluateCommendBO);
                if (null != evaluateCommendPO.getUserId()){
                    evaluateCommendBO.setUserId(evaluateCommendPO.getUserId().toString());
                }
                commendBOS.add(evaluateCommendBO);
            }
        }
        /**
         * 数据头
         */
        LinkedHashMap map = new LinkedHashMap();
        map.put("1","1");
        map.put("2","2");
        map.put("3","3");
        /**
         * 数据
         */
        List exportData = new ArrayList<Map>();
        List<List<Object>> lists = new ArrayList<>();
        for (EvaluateCommendBO commendBO : commendBOS) {
            Map row = new LinkedHashMap<String, String>();
            row.put("1",commendBO.getUserId());
            row.put("2",commendBO.getPositionId().toString());
            row.put("3",commendBO.getEvaluateStart());
            exportData.add(row);
        }
        /**
         * 输出路径
         */
        String out = "D:\\Gitworkplace\\recurit\\csvFile\\";
        /**
         * 文件名
         */
        String fileName = "evaluateCommend";
        /**
         * 生成csv文件
         */
        String name =  POIUtil3(exportData,map,out,fileName);
        /**
         * 调用推荐算法
         */
        try{
            String path = out + name;
            List<Integer> ids =  userCF(path);
            if (!CollectionUtils.isEmpty(ids)){
                PositionPO po = new PositionPO();
                po.setPositionIds(ids);
                Page page = new Page(-1,-1);
                List<PositionPO> list = positionMapper.selectList(po,page);
                List<PositionBO> boLists = new ArrayList<>();
                if (!CollectionUtils.isEmpty(list)){
                    for (PositionPO po1 : list) {
                        PositionBO positionBO = new PositionBO();
                        BeanUtils.copyProperties(po1,positionBO);
                        positionBO.setSalary(po1.getLowSalary()+"K"+"-"+po1.getHighSalary()+"K");
                        if (null != po.getPositionResource()){
                            if ("1".equals(po.getPositionResource())){
                                positionBO.setPositionResourceStr("前程无忧网");
                            } else if ("2".equals(po.getPositionResource())){
                                positionBO.setPositionResourceStr("汇博人才网");
                            } else if ("3".equals(po.getPositionResource())){
                                positionBO.setPositionResourceStr("中华英才网");
                            }
                        }
                        boLists.add(positionBO);
                    }
                }
                rspBO.setRows(boLists);
                rspBO.setCode(RspConstantBo.SuccRspCode);
                rspBO.setMessage(RspConstantBo.SuccRspDesc);
                rspBO.setPageNo(page.getPageNo());
                rspBO.setTotal(positionMapper.selectCount(positionPO));
                rspBO.setRecordsTotal(page.getTotalCount());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return rspBO;
    }


    public static void createCSVFile(List<Object> head, List<List<Object>> dataList, String outPutPath, String filename) {

        File csvFile = null;
        BufferedWriter csvWtriter = null;
        try {
            csvFile = new File(outPutPath + File.separator + filename + ".csv");
            File parent = csvFile.getParentFile();
            if (parent != null && !parent.exists()) {
                parent.mkdirs();
            }
            csvFile.createNewFile();

            // GB2312使正确读取分隔符","
            csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                    csvFile), "utf-8"), 1024);
            // 写入文件头部
            //writeRow(head, csvWtriter);

            // 写入文件内容
            for (List<Object> row : dataList) {
                writeRow(row, csvWtriter);
            }
            csvWtriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                csvWtriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 写一行数据方法
     * @param row
     * @param csvWriter
     */
    private static void writeRow(List<Object> row, BufferedWriter csvWriter) throws IOException {
        // 写入文件头部
        for (Object data : row) {
            StringBuffer sb = new StringBuffer();
            String rowStr = sb.append("\"").append(data).append("\",").toString();
            csvWriter.write(rowStr);
        }
        csvWriter.newLine();
    }



    List<Integer> userCF(String file) throws IOException, TasteException {
        List<Integer> resultList = new ArrayList<>();
//        String file = "E:\\evaluate_position.csv";
        //数据模型
        File file1 = new File(file);
        DataModel model = new FileDataModel(file1);
        //用户相识度算法
        UserSimilarity user = new EuclideanDistanceSimilarity(model);
        NearestNUserNeighborhood neighbor = new NearestNUserNeighborhood(NEIGHBORHOOD_NUM, user, model);
        //用户近邻算法
        Recommender r = new GenericUserBasedRecommender(model, neighbor, user);//用户推荐算法
        LongPrimitiveIterator iter = model.getUserIDs();///得到用户ID

        while (iter.hasNext()) {
            long uid = iter.nextLong();
            List<RecommendedItem> list = r.recommend(uid, RECOMMENDER_NUM);
            System.out.printf("uid:%s", uid);
            for (RecommendedItem ritem : list) {
                resultList.add(Integer.parseInt(String.valueOf(ritem.getItemID())));
                System.out.println("itemID:" + ritem.getItemID());
                System.out.println("value:" + ritem.getValue());
//                System.out.printf("(%s,%f)", ritem.getItemID(), ritem.getValue());
            }
            System.out.println();
        }
        return resultList;
    }

    String POIUtil3(List exportData, LinkedHashMap map, String outPutPath, String fileName){
        File csvFile = null;
        BufferedWriter csvFileOutputStream = null;
        try {
            File file = new File(outPutPath);
            if (!file.exists()) {
                file.mkdir();
            }
            //定义文件名格式并创建
            csvFile = File.createTempFile(fileName, ".csv", new File(outPutPath));
            // UTF-8使正确读取分隔符","
            //如果生产文件乱码，windows下用gbk，linux用UTF-8
            csvFileOutputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
            // 写入文件头部
            for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator.hasNext(); ) {
                Map.Entry propertyEntry = (Map.Entry) propertyIterator.next();
                csvFileOutputStream.write(propertyEntry.getValue() != null ? (String) propertyEntry.getValue() : "");
                if (propertyIterator.hasNext()) {
                    csvFileOutputStream.write(",");
                }
            }
            csvFileOutputStream.newLine();
            // 写入文件内容
            for (Iterator iterator = exportData.iterator(); iterator.hasNext(); ) {
                Object row = (Object) iterator.next();
                for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator.hasNext(); ) {
                    Map.Entry propertyEntry = (Map.Entry) propertyIterator.next();
//                    BeanUtils.getProperty(row, (String) propertyEntry.getKey()));
                    csvFileOutputStream.write((String) org.apache.commons.beanutils.BeanUtils.getProperty(row, (String) propertyEntry.getKey()));
                    if (propertyIterator.hasNext()) {
                        csvFileOutputStream.write(",");
                    }
                }
                if (iterator.hasNext()) {
                    csvFileOutputStream.newLine();
                }
            }
            csvFileOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                csvFileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return csvFile.getName();
    }

    public static void main(String[] args) throws IOException, TasteException {
        PositionRecommendBusiServiceImpl positionRecommendBusiService = new PositionRecommendBusiServiceImpl();
        System.out.println(JSONObject.toJSONString(positionRecommendBusiService.userCF("E:\\evaluateCommend8064363946130845835.csv")));
    }
}
