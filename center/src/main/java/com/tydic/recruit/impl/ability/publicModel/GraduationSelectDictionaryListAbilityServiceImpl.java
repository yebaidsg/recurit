package com.tydic.recruit.impl.ability.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectDictionaryListAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryListAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryListAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectDictionaryListBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectDictionaryListAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:34
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectDictionaryListAbilityServiceImpl implements GraduationSelectDictionaryListAbilityService {

    @Autowired
    private GraduationSelectDictionaryListBusiService graduationSelectDictionaryListBusiService;
    @Override
    public GraduationSelectDictionaryListAbilityRspBO selectDictionaryList(GraduationSelectDictionaryListAbilityReqBO reqBO) {
        GraduationSelectDictionaryListBusiReqBO busiReqBO = new GraduationSelectDictionaryListBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectDictionaryListBusiRspBO busiRspBO = graduationSelectDictionaryListBusiService.selectDictionaryList(busiReqBO);
        String jsonString = JSONObject.toJSONString(busiRspBO, SerializerFeature.WriteMapNullValue);
        GraduationSelectDictionaryListAbilityRspBO rspBO = JSONObject.parseObject(jsonString,GraduationSelectDictionaryListAbilityRspBO.class);
        return rspBO;
    }
}
