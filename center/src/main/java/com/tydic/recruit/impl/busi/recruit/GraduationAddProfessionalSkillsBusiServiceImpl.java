package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationAddProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.ProfessionalSkillsMapper;
import com.tydic.recruit.dao.recurit.po.ProfessionalSkillsPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationAddProfessionalSkillsBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:37
 */
@Service("GraduationAddProfessionalSkillsBusiService")
public class GraduationAddProfessionalSkillsBusiServiceImpl implements GraduationAddProfessionalSkillsBusiService {
    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;
    @Override
    public GraduationAddProfessionalSkillsBusiRspBO addProfession(GraduationAddProfessionalSkillsBusiReqBO reqBO) {
        ProfessionalSkillsPO professionalSkillsPO = new ProfessionalSkillsPO();
        BeanUtils.copyProperties(reqBO,professionalSkillsPO);
        if (null != reqBO.getSkillsId()){
            professionalSkillsMapper.update(professionalSkillsPO);
        } else {
            professionalSkillsPO.setSkillsId(Sequence.getInstance().nextId());
            int insertCheck = professionalSkillsMapper.insert(professionalSkillsPO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationAddProfessionalSkillsBusiRspBO rspBO = new GraduationAddProfessionalSkillsBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
