package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectEducationListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.EducationListBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEducationListBusiReqBo;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEducationListBusiRspBo;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.EducationalExperienceMapper;
import com.tydic.recruit.dao.recurit.po.EducationalExperiencePO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xddf.usermodel.chart.RadarStyle;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectEducationListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:16
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectEducationListBusiServiceImpl implements GraduationSelectEducationListBusiService {
    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;
    @Override
    public GraduationSelectEducationListBusiRspBo selectList(GraduationSelectEducationListBusiReqBo reqBo) {
        GraduationSelectEducationListBusiRspBo rspBo = new GraduationSelectEducationListBusiRspBo();
        EducationalExperiencePO educationalExperiencePO = new EducationalExperiencePO();
        educationalExperiencePO.setPersonId(reqBo.getPersonId());
        SimpleDateFormat temp=new SimpleDateFormat("yyyy-MM-dd");
        List<EducationalExperiencePO> list = educationalExperienceMapper.selectList(educationalExperiencePO);
        List<EducationListBO> educationListBOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            for (EducationalExperiencePO experiencePO : list) {
                EducationListBO educationListBO  = new EducationListBO();
                BeanUtils.copyProperties(experiencePO,educationListBO);
                if (null != experiencePO.getEducation()){
                    switch (experiencePO.getEducation()){
                        case "1":
                            educationListBO.setEducation("高中");
                            break;
                        case "2":
                            educationListBO.setEducation("中专/中技");
                            break;
                        case "3":
                            educationListBO.setEducation("大专");
                            break;
                        case "4":
                            educationListBO.setEducation("本科");
                            break;
                        case "5":
                            educationListBO.setEducation("硕士");
                            break;
                        case "6":
                            educationListBO.setEducation("MBA/EMBA");
                            break;
                        case "7":
                            educationListBO.setEducation("博士");
                            break;
                        default:
                            educationListBO.setEducation("未知");
                            break;
                    }
                }
                if (null != experiencePO.getEduBeginTime()){
                    educationListBO.setEduBeginTime(temp.format(experiencePO.getEduBeginTime()));
                }
                if (null != experiencePO.getEduEndTime()){
                    educationListBO.setEduEndTime(temp.format(experiencePO.getEduEndTime()));
                }
                educationListBO.setEduId(experiencePO.getEduId().toString());
                educationListBOS.add(educationListBO);
            }
            rspBo.setEducationListBOS(educationListBOS);
        }
        rspBo.setCode(RspConstantBo.SuccRspCode);
        rspBo.setMessage(RspConstantBo.SuccRspDesc);
        return rspBo;
    }
}
