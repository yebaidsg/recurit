package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.busi.recruit.GraduationSelectEvaluatedListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluatedListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluatedListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluatedListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:31
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectEvaluatedListBusiServiceImpl implements GraduationSelectEvaluatedListBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public GraduationSelectEvaluatedListBusiRspBO selectEvaluateList(GraduationSelectEvaluatedListBusiReqBO reqBO) {
        GraduationSelectEvaluatedListBusiRspBO rspBO = new GraduationSelectEvaluatedListBusiRspBO();
        PositionPO positionPO = new PositionPO();
        BeanUtils.copyProperties(reqBO,positionPO);
        positionPO.setUserId(reqBO.getUserId());
        Page page = new Page(-1,-1);
        if (null != reqBO.getProvinceName()){
            if (reqBO.getProvinceName().length() < 2){
                positionPO.setWorkPlace(reqBO.getProvinceName());
            } else{
                String province1 = String.valueOf(reqBO.getProvinceName().charAt(0));
                String province2 = String.valueOf(reqBO.getProvinceName().charAt(1));
                positionPO.setWorkPlace(province1+province2);
            }
        }
        if (null != reqBO.getCityName()){
            positionPO.setWorkPlace(reqBO.getCityName());
        }
        if (null != reqBO.getAreaName()){
            positionPO.setWorkPlace(reqBO.getAreaName());
        }
        List<PositionPO> positionPOList = positionMapper.selectEvaluateList(positionPO,page);
        List<PositionBO> positionBOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(positionPOList)){
            for (PositionPO po : positionPOList) {
                PositionBO positionBO = new PositionBO();
                BeanUtils.copyProperties(po,positionBO);
                positionBO.setSalary(po.getLowSalary()+"K"+"-"+po.getHighSalary()+"K");
                if (null != po.getPositionResource()){
                    if ("1".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("前程无忧网");
                    } else if ("2".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("汇博人才网");
                    } else if ("3".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("中华英才网");
                    }
                }
                positionBOList.add(positionBO);
            }
        }
        rspBO.setRows(positionBOList);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(positionMapper.selectCount(positionPO));
        rspBO.setRecordsTotal(page.getTotalCount());
        return rspBO;
    }
}
