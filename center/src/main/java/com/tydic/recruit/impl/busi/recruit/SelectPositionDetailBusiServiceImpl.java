package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.SelectPositionDetailBusiService;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionDetailBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionDetailBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huzb
 * @标题 SelectPositionDetailBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:53
 */
@HTServiceImpl
@Slf4j
public class SelectPositionDetailBusiServiceImpl implements SelectPositionDetailBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public SelectPositionDetailBusiRspBO selectDetail(SelectPositionDetailBusiReqBO reqBO) {
        SelectPositionDetailBusiRspBO rspBO = new SelectPositionDetailBusiRspBO();
        PositionPO positionPO = new PositionPO();
        positionPO.setPositionId(reqBO.getPositionId());
        PositionPO po = positionMapper.selectDetail(positionPO);
        if (null != po){
            BeanUtils.copyProperties(po,rspBO);
            if (null != po.getExperience()){
                if ("0".equals(po.getExperience())){
                    rspBO.setExperience("不限经验");
                } else {
                    rspBO.setExperience(po.getExperience() + "年以上");
                }
            }
            if (null != po.getHighSalary() && null != po.getLowSalary()){
                rspBO.setSalary(po.getLowSalary()+"-"+po.getHighSalary()+"K");
            }
            if (null != po.getPositionResource()){
                if ("1".equals(po.getPositionResource())){
                    rspBO.setPositionResourceStr("前程无忧网");
                } else if ("2".equals(po.getPositionResource())){
                    rspBO.setPositionResourceStr("汇博人才网");
                } else if ("3".equals(po.getPositionResource())){
                    rspBO.setPositionResourceStr("中华英才网");
                }
            }
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
