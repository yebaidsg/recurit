package com.tydic.recruit.impl.busi.user;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ozo.ability.RoleService;
import com.ozo.ability.bo.RoleBO;
import com.tydic.recruit.dao.po.Role;
import com.tydic.recruit.dao.user.RoleMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<RoleBO> findRoleByUserId(Long id) {
        return JSONObject.parseArray(JSONObject.toJSONString(roleMapper.findRoleByUserId(id), SerializerFeature.WriteMapNullValue),RoleBO.class);
    }

    @Override
    public List<RoleBO> findAllRole() {
        return JSONObject.parseArray(JSONObject.toJSONString(roleMapper.findAllRole(), SerializerFeature.WriteMapNullValue),RoleBO.class);
    }

    @Override
    public List<RoleBO> findRoleByUsername(String username) {
        return JSONObject.parseArray(JSONObject.toJSONString(roleMapper.findRoleByUsername(username), SerializerFeature.WriteMapNullValue),RoleBO.class);
    }

    @Override
    public void insertRole(RoleBO role) {
        Role role1 = new Role();
        BeanUtils.copyProperties(role,role1);
        roleMapper.insertRole(role1);
    }

    @Override
    public void deleteRole(Long id) {
        roleMapper.deleteRole(id);
    }

    @Override
    public void updateRole(RoleBO role) {
        Role role1 = new Role();
        BeanUtils.copyProperties(role,role1);
        roleMapper.updateRole(role1);
    }

    @Override
    public void updateRoleById(RoleBO role, Long id) {
        Role role1 = new Role();
        BeanUtils.copyProperties(role,role1);
        roleMapper.updateRoleById(role1,id);
    }
}
