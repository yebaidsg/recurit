package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.PersonalInformationMapper;
import com.tydic.recruit.dao.recurit.po.PersonalInformationPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitUpdateResumeBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:19
 */
@Service("GraduationRecruitUpdateResumeBusiService")
public class GraduationRecruitUpdateResumeBusiServiceImpl implements GraduationRecruitUpdateResumeBusiService {
    @Autowired
    private PersonalInformationMapper personalInformationMapper;
    @Override
    public GraduationRecruitUpdateResumeBusiRspBO updateResume(GraduationRecruitUpdateResumeBusiReqBO reqBO) {
        PersonalInformationPO personalInformationPO = new PersonalInformationPO();
        BeanUtils.copyProperties(reqBO,personalInformationPO);
        int insertCheck = personalInformationMapper.insert(personalInformationPO);
        if (insertCheck < 1){
            throw new BusinessException("8888","插入失败");
        }
        GraduationRecruitUpdateResumeBusiRspBO rspBO = new GraduationRecruitUpdateResumeBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
