package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProjectExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProjectExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectProjectExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectProjectExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:15
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectProjectExperienceAbilityServiceImpl implements GraduationSelectProjectExperienceAbilityService {
    @Autowired
    private GraduationSelectProjectExperienceBusiService graduationSelectProjectExperienceBusiService;
    @Override
    public GraduationSelectProjectExperienceAbilityRspBO selectProjectExperience(GraduationSelectProjectExperienceAbilityReqBO reqBO) {
        GraduationSelectProjectExperienceBusiReqBO busiReqBO = new GraduationSelectProjectExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectProjectExperienceBusiRspBO busiRspBO = graduationSelectProjectExperienceBusiService.selectProject(busiReqBO);
        GraduationSelectProjectExperienceAbilityRspBO rspBO = new GraduationSelectProjectExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
