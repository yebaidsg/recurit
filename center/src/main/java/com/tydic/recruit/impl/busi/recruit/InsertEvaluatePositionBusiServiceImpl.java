package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.InsertEvaluatePositionBusiService;
import com.tydic.recruit.api.busi.recruit.bo.InsertEvaluatePositionBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertEvaluatePositionBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.EvaluatePositionMapper;
import com.tydic.recruit.dao.recurit.po.EvaluatePositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huzb
 * @标题 InsertEvaluatePositionBusiServiceiMPL
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/19 16:10
 */
@HTServiceImpl
@Slf4j
public class InsertEvaluatePositionBusiServiceImpl implements InsertEvaluatePositionBusiService {
    @Autowired
    private EvaluatePositionMapper evaluatePositionMapper;
    @Override
    public InsertEvaluatePositionBusiRspBO insertEvaPo(InsertEvaluatePositionBusiReqBO reqBO) {
        EvaluatePositionPO evaluatePositionPO = new EvaluatePositionPO();
        BeanUtils.copyProperties(reqBO,evaluatePositionPO);
        /**
         * 查询
         */
        EvaluatePositionPO selectPO = evaluatePositionMapper.select(evaluatePositionPO);
        if (null != selectPO){
            int updateCheck = evaluatePositionMapper.update(evaluatePositionPO);
        } else {
            evaluatePositionPO.setEvaluateId(Sequence.getInstance().nextId());
            int insert = evaluatePositionMapper.insert(evaluatePositionPO);
        }
        InsertEvaluatePositionBusiRspBO rspBO = new InsertEvaluatePositionBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
