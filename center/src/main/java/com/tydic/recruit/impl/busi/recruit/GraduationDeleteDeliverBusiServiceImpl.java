package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationDeleteDeliverBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationDeleteDeliverBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationDeleteDeliverBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huzb
 * @标题 GraduationDeleteDeliverBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:04
 */
@HTServiceImpl
@Slf4j
public class GraduationDeleteDeliverBusiServiceImpl implements GraduationDeleteDeliverBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public GraduationDeleteDeliverBusiRspBO deleteDeliver(GraduationDeleteDeliverBusiReqBO reqBO) {
        positionMapper.deleteDeliver(reqBO.getDeliverId());
        GraduationDeleteDeliverBusiRspBO rspBO = new GraduationDeleteDeliverBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage("删除成功");
        return rspBO;
    }
}
