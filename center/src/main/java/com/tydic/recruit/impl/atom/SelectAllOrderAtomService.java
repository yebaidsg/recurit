package com.tydic.recruit.impl.atom;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.dao.po.AllOrderPO;

import java.util.List;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/22 0:19
 */
public interface SelectAllOrderAtomService {
    List<AllOrderPO> selectAllOrderByOrderId(Integer orderId, String state, String orderCreateTime, String userName, Page page);
}
