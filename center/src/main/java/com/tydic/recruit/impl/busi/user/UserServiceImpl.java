package com.tydic.recruit.impl.busi.user;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.base.bo.RspBaseBO;
import com.ozo.ability.UserService;
import com.ozo.ability.bo.UserBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.po.User;
import com.tydic.recruit.dao.user.RoleMapper;
import com.tydic.recruit.dao.user.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserBO> findAllUser() {
        return JSONObject.parseArray(JSONObject.toJSONString(userMapper.findAllUser(), SerializerFeature.WriteMapNullValue),UserBO.class);
    }

    @Override
    public UserBO findLoginUser(UserBO user) {
        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        return JSONObject.parseObject(JSONObject.toJSONString(userMapper.findLoginUser(user1), SerializerFeature.WriteMapNullValue),UserBO.class);
    }

    @Override
    public UserBO findUserByName(String username) {
        return JSONObject.parseObject(JSONObject.toJSONString(userMapper.findUserByName(username), SerializerFeature.WriteMapNullValue),UserBO.class);
    }

    @Override
    public List<UserBO> findUserByCondition(UserBO user) {
        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        return JSONObject.parseArray(JSONObject.toJSONString(userMapper.findUserByCondition(user1), SerializerFeature.WriteMapNullValue),UserBO.class);
    }

    @Override
    public List<UserBO> findUserByUsername(String username) {
        List<UserBO> userList= userMapper.findUserByUsername(username);
//        System.out.println("1111111111"+userList);
//        List<UserBO> userBOS = new ArrayList<>();
//        if (!CollectionUtils.isEmpty(userList)){
//
//            for (User user : userList) {
//                UserBO userBO = new UserBO();
//                BeanUtils.copyProperties(user,userBO);
//                userBOS.add(userBO);
//            }
//        }
//        System.out.println("1111111111"+userBOS);
        return userList;
    }

    @Override
    public RspBaseBO insertUser(UserBO user) {
        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        user1.setUserEnrollTime(new Date());
        user1.setUserStatus("0");
        userMapper.insertUser(user1);
        userMapper.insertUserRole(user1.getUserId(),301L);
        RspBaseBO rspBaseBO = new RspBaseBO();
        rspBaseBO.setCode(RspConstantBo.SuccRspCode);
        rspBaseBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBaseBO;
    }

    @Override
    public void insertUserRole(Long userId, Long roleId) {
        userMapper.insertUserRole(userId, roleId);
    }

    @Override
    public void deleteUser(Long id) {
        userMapper.deleteUser(id);
    }

    @Override
    public void updateUser(UserBO user) {
        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        userMapper.updateUser(user1);
    }


}
