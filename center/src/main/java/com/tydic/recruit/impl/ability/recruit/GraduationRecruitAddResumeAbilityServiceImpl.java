package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddResumeAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitAddResumeAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitAddResumeAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 22:43
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitAddResumeAbilityServiceImpl implements GraduationRecruitAddResumeAbilityService {

    @Autowired
    private GraduationRecruitAddResumeBusiService graduationRecruitAddResumeBusiService;
    @Override
    public GraduationRecruitAddResumeAbilityRspBO addResume(GraduationRecruitAddResumeAbilityReqBO reqBO) {
        GraduationRecruitAddResumeBusiReqBO busiReqBO = new GraduationRecruitAddResumeBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        if (null != reqBO.getAddress() && reqBO.getAddress().length >0){
                busiReqBO.setPersonAddressProvinceName(reqBO.getAddress()[0]);
            busiReqBO.setPersonAddressCityName(reqBO.getAddress()[1]);
            busiReqBO.setPersonAddressAreaName(reqBO.getAddress()[2]);
        }
        GraduationRecruitAddResumeBusiRspBO busiRspBO = graduationRecruitAddResumeBusiService.addResume(busiReqBO);
        GraduationRecruitAddResumeAbilityRspBO rspBO = new GraduationRecruitAddResumeAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
