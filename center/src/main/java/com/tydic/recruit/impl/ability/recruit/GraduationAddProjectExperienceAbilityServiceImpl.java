package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationProjectExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationProjectExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationAddProjectExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationAddProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationAddProjectExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:03
 */
@HTServiceImpl
@Slf4j
public class GraduationAddProjectExperienceAbilityServiceImpl implements GraduationAddProjectExperienceAbilityService {
    @Autowired
    private GraduationAddProjectExperienceBusiService graduationAddProjectExperienceBusiService;
    @Override
    public GraduationProjectExperienceAbilityRspBO addProjectExperience(GraduationProjectExperienceAbilityReqBO reqBO) {
        GraduationAddProjectExperienceBusiReqBO busiReqBO = new GraduationAddProjectExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationAddProjectExperienceBusiRspBO busiRspBO = graduationAddProjectExperienceBusiService.addProjectExperience(busiReqBO);
        GraduationProjectExperienceAbilityRspBO rspBO = new GraduationProjectExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
