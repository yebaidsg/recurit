package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @标题 GraduationRecruitAddJobIntentionalBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:56
 */
@Service("GraduationRecruitAddJobIntentionalBusiService")
public class GraduationRecruitAddJobIntentionalBusiServiceImpl implements GraduationRecruitAddJobIntentionalBusiService {
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Override
    public GraduationRecruitAddJobIntentionalBusiRspBO addJobInternational(GraduationRecruitAddJobIntentionalBusiReqBO reqBO) {
        PersonJobIntentionPO personJobIntentionPO = new PersonJobIntentionPO();
        BeanUtils.copyProperties(reqBO,personJobIntentionPO);

        personJobIntentionPO.setCreateId(reqBO.getUserId());
        personJobIntentionPO.setCreateTime(new Date());
        if (null != reqBO.getExpectationCity() && reqBO.getExpectationCity().length >0){
            personJobIntentionPO.setExpectProvinceCode(reqBO.getExpectationCity()[0]);
            personJobIntentionPO.setExpectCityCode(reqBO.getExpectationCity()[1]);
            personJobIntentionPO.setExpectAreaCode(reqBO.getExpectationCity()[2]);
        }
        if (null != reqBO.getSecondPositionCodeList() && reqBO.getSecondPositionCodeList().length >0){
            personJobIntentionPO.setFirstPositionCode(reqBO.getSecondPositionCodeList()[0]);
            personJobIntentionPO.setSecondPositionCode(reqBO.getSecondPositionCodeList()[1]);
        }
        if (null != reqBO.getIntentionId()){
            personJobIntentionMapper.update(personJobIntentionPO);
        } else {
            personJobIntentionPO.setIntentionId(Sequence.getInstance().nextId());
            int insertCheck = personJobIntentionMapper.insert(personJobIntentionPO);
            if (insertCheck <1 ){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationRecruitAddJobIntentionalBusiRspBO rspBO = new GraduationRecruitAddJobIntentionalBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
