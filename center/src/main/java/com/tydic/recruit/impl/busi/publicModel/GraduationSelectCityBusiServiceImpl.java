package com.tydic.recruit.impl.busi.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.ability.publicModel.bo.CityBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectCityBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.publicmodel.ProvinceMapper;
import com.tydic.recruit.dao.publicmodel.po.CityPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @标题 GraduationSelectCityBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:39
 */
@Service("GraduationSelectCityBusiService")
public class GraduationSelectCityBusiServiceImpl implements GraduationSelectCityBusiService {

    @Autowired
    private ProvinceMapper provinceMapper;
    @Override
    public GraduationSelectCityBusiRspBO selectCityList(GraduationSelectCityBusiReqBO reqBO) {
        GraduationSelectCityBusiRspBO rspBO = new GraduationSelectCityBusiRspBO();
        CityPO cityPO = new CityPO();
        BeanUtils.copyProperties(reqBO,cityPO);
        Page<CityPO> page = new Page<>(reqBO.getPageNo(),reqBO.getPageSize());
        List<CityPO> lists = provinceMapper.selectCityList(cityPO,page);
        if (!CollectionUtils.isEmpty(lists)){
            List<CityBO> cityBOS = JSONObject.parseArray(JSONObject.toJSONString(lists, SerializerFeature.WriteMapNullValue),CityBO.class);
            rspBO.setRows(cityBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(page.getTotalCount());
        return rspBO;
    }
}
