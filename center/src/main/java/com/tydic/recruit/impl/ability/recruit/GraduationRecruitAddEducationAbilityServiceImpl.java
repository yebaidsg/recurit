package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddEducationAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitAddEducationAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitAddEducationAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:00
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitAddEducationAbilityServiceImpl implements GraduationRecruitAddEducationAbilityService {
    @Autowired
    private GraduationRecruitAddEducationBusiService graduationRecruitAddEducationBusiService;
    @Override
    public GraduationRecruitAddEducationAbilityRspBO addEducation(GraduationRecruitAddEducationAbilityReqBO reqBO) {
        GraduationRecruitAddEducationBusiReqBO busiReqBO = new GraduationRecruitAddEducationBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitAddEducationBusiRspBO busiRspBO = graduationRecruitAddEducationBusiService.addEducation(busiReqBO);
        GraduationRecruitAddEducationAbilityRspBO rspBO = new GraduationRecruitAddEducationAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
