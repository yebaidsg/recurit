package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateResumeAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitUpdateResumeAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitUpdateResumeAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:20
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitUpdateResumeAbilityServiceImpl implements GraduationRecruitUpdateResumeAbilityService {
    @Autowired
    private GraduationRecruitUpdateResumeBusiService graduationRecruitUpdateResumeBusiService;
    @Override
    public GraduationRecruitUpdateResumeAbilityRspBO updateResume(GraduationRecruitUpdateResumeAbilityReqBO reqBO) {
        GraduationRecruitUpdateResumeBusiReqBO busiReqBO = new GraduationRecruitUpdateResumeBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitUpdateResumeBusiRspBO busiRspBO = graduationRecruitUpdateResumeBusiService.updateResume(busiReqBO);
        GraduationRecruitUpdateResumeAbilityRspBO rspBO = new GraduationRecruitUpdateResumeAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
