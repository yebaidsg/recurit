package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddLanguageAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationAddLanguageAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationAddLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationAddLanguageAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:15
 */
@HTServiceImpl
@Slf4j
public class GraduationAddLanguageAbilityServiceImpl implements GraduationAddLanguageAbilityService {
    @Autowired
    private GraduationAddLanguageBusiService graduationAddLanguageBusiService;
    @Override
    public GraduationAddLanguageAbilityRspBO addLanguage(GraduationAddLanguageAbilityReqBO reqBO) {
        GraduationAddLanguageBusiReqBO busiReqBO = new GraduationAddLanguageBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationAddLanguageBusiRspBO busiRspBO = graduationAddLanguageBusiService.addLanguage(busiReqBO);
        GraduationAddLanguageAbilityRspBO rspBO = new GraduationAddLanguageAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
