package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationAddWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.WorkExperienceMapper;
import com.tydic.recruit.dao.recurit.po.WorkExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationAddWorkExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 14:35
 */
@Service("GraduationAddWorkExperienceBusiService")
public class GraduationAddWorkExperienceBusiServiceImpl implements GraduationAddWorkExperienceBusiService {

    @Autowired
    private WorkExperienceMapper workExperienceMapper;
    @Override
    public GraduationAddWorkExperienceBusiRspBO addWorkExperience(GraduationAddWorkExperienceBusiReqBO reqBO) {
        WorkExperiencePO workExperiencePO = new WorkExperiencePO();
        BeanUtils.copyProperties(reqBO,workExperiencePO);
        if (null != reqBO.getWorkId()){
            workExperienceMapper.update(workExperiencePO);
        }else {
            workExperiencePO.setWorkId(Sequence.getInstance().nextId());
            int insertCheck = workExperienceMapper.insert(workExperiencePO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationAddWorkExperienceBusiRspBO rspBO = new GraduationAddWorkExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
