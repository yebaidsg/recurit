package com.tydic.recruit.impl.ability.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectTownAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectTownAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectTownAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectTownBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectTownAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:02
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectTownAbilityServiceImpl implements GraduationSelectTownAbilityService {

    @Autowired
    private GraduationSelectTownBusiService graduationSelectTownBusiService;
    @Override
    public GraduationSelectTownAbilityRspBO selectTownList(GraduationSelectTownAbilityReqBO reqBO) {
        GraduationSelectTownBusiReqBO busiReqBO = new GraduationSelectTownBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectTownBusiRspBO busiRspBO = graduationSelectTownBusiService.selectTownList(busiReqBO);
        return JSONObject.parseObject(JSONObject.toJSONString(busiRspBO, SerializerFeature.WriteMapNullValue),GraduationSelectTownAbilityRspBO.class);
    }
}
