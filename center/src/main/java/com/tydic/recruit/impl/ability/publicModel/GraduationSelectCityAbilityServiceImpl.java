package com.tydic.recruit.impl.ability.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectCityAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectCityAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectCityAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectCityBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectCityAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:37
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectCityAbilityServiceImpl implements GraduationSelectCityAbilityService {
    @Autowired
    private GraduationSelectCityBusiService graduationSelectCityBusiService;
    @Override
    public GraduationSelectCityAbilityRspBO selectCityList(GraduationSelectCityAbilityReqBO reqBO) {
        GraduationSelectCityBusiReqBO busiReqBO = new GraduationSelectCityBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectCityBusiRspBO BusirspBO = graduationSelectCityBusiService.selectCityList(busiReqBO);
        return JSONObject.parseObject(JSONObject.toJSONString(BusirspBO, SerializerFeature.WriteMapNullValue),GraduationSelectCityAbilityRspBO.class);
    }
}
