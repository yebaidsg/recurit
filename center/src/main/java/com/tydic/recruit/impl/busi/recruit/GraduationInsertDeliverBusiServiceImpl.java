package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationInsertDeliverBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationInsertDeliverBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationInsertDeliverBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.DeliverPositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author huzb
 * @标题 GraduationInsertDeliverBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 23:56
 */
@HTServiceImpl
@Slf4j
public class GraduationInsertDeliverBusiServiceImpl implements GraduationInsertDeliverBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public GraduationInsertDeliverBusiRspBO insertDeliver(GraduationInsertDeliverBusiReqBO reqBO) {
        GraduationInsertDeliverBusiRspBO rspBO = new GraduationInsertDeliverBusiRspBO();
        DeliverPositionPO deliverPositionPO = new DeliverPositionPO();
        deliverPositionPO.setPositionId(reqBO.getPositionId());
        deliverPositionPO.setUserId(reqBO.getUserId());
        int i = positionMapper.deliverCount(deliverPositionPO);
        if (i > 0){
            rspBO.setCode(RspConstantBo.SuccRspCode);
            rspBO.setMessage("您已申请过该职位");
            return rspBO;
        }
        deliverPositionPO.setDeliverTime(new Date());
        deliverPositionPO.setDeliverStatus("1");
        positionMapper.insertDeliver(deliverPositionPO);

        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
