package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.busi.recruit.GraduationSelectPositionCommendBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectPosionCommendBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectPosionCommendBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.PersonalInformationMapper;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import com.tydic.recruit.dao.recurit.po.PersonalInformationPO;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectPositionCommendBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 21:44
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectPositionCommendBusiServiceImpl implements GraduationSelectPositionCommendBusiService {
    private static final String POSITION = "1";
    private static final String AREA = "2";
    private static final String ZUIJIA = "3";
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Autowired
    private PersonalInformationMapper personalInformationMapper;
    @Autowired
    private PositionMapper positionMapper; 
    @Override
    public GraduationSelectPosionCommendBusiRspBO selectPositionCommend(GraduationSelectPosionCommendBusiReqBO reqBO) {
        GraduationSelectPosionCommendBusiRspBO rspBO = new GraduationSelectPosionCommendBusiRspBO();
        /**
         * 根据用户ID查询求职意向与期望地址
         */
        PersonalInformationPO personalInformationPO = new PersonalInformationPO();
        personalInformationPO.setUserId(reqBO.getUserId());
        PersonalInformationPO informationPO = personalInformationMapper.select(personalInformationPO);
        if (null == informationPO){
            rspBO.setCode(RspConstantBo.FailRspCode);
            rspBO.setMessage("未查询到简历信息，请完善简历信息");
            return rspBO;
        }
        /**
         * 根据简历ID查询意向信息
         */
        PersonJobIntentionPO personJobIntentionPO = new PersonJobIntentionPO();
        personJobIntentionPO.setPersonId(informationPO.getPersonId());
        List<PersonJobIntentionPO> jobIntentionPOS = personJobIntentionMapper.selectList(personJobIntentionPO);
        if (CollectionUtils.isEmpty(jobIntentionPOS)){
            rspBO.setCode(RspConstantBo.FailRspCode);
            rspBO.setMessage("意向职位信息查询为空，请添加职位信息");
            return rspBO;
        }
        /**
         * 职位信息
         */
        List<PositionPO> commendPosition = new ArrayList<>();
        List<String> positionIntentions = new ArrayList<>();
        List<String> intentionArea = new ArrayList<>();
        Page page = new Page(-1,-1);
        List<PositionPO> list = new ArrayList<>();
        PositionPO positionPO = new PositionPO();
        int count = 0;
        for (PersonJobIntentionPO jobIntentionPO : jobIntentionPOS) {
            positionIntentions.add(jobIntentionPO.getSecondPositionCode());
            intentionArea.add(jobIntentionPO.getExpectProvinceName());
        }
        if (POSITION.equals(reqBO.getStatus())){
            for (String positionIntention : positionIntentions) {
                positionPO.setJobTitle(positionIntention);
                list = positionMapper.selectPositonCommend(positionPO);
                if (!CollectionUtils.isEmpty(list)){
                    for (PositionPO po : list) {
                        commendPosition.add(po);
                    }
                }
            }
        } else if (AREA.equals(reqBO.getStatus())){
            for (String s : intentionArea) {
                positionPO.setWorkPlace(s);
                list = positionMapper.selectPositonCommend(positionPO);
                if (!CollectionUtils.isEmpty(list)){
                    for (PositionPO po : list) {
                        commendPosition.add(po);
                    }
                }
            }
        } else if (ZUIJIA.equals(reqBO.getStatus())){
            for (PersonJobIntentionPO jobIntentionPO : jobIntentionPOS) {
                positionPO.setJobTitle(jobIntentionPO.getSecondPositionCode());
                positionPO.setWorkPlace(jobIntentionPO.getExpectProvinceName());
                list = positionMapper.selectPositonCommend(positionPO);
                if (!CollectionUtils.isEmpty(list)){
                    for (PositionPO po : list) {
                        commendPosition.add(po);
                    }
                }
            }
        }
        List<PositionBO> bolist = new ArrayList<>();
        if (!CollectionUtils.isEmpty(commendPosition)){
            for (PositionPO po : commendPosition) {
                PositionBO positionBO = new PositionBO();
                BeanUtils.copyProperties(po,positionBO);
                positionBO.setSalary(po.getLowSalary()+"K"+"-"+po.getHighSalary()+"K");
                if (null != po.getPositionResource()){
                    if ("1".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("前程无忧网");
                    } else if ("2".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("汇博人才网");
                    } else if ("3".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("中华英才网");
                    }
                }
                bolist.add(positionBO);
            }
        }
        rspBO.setRows(bolist);
        rspBO.setTotal(positionMapper.selectCount(positionPO));
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        return rspBO;
    }
}
