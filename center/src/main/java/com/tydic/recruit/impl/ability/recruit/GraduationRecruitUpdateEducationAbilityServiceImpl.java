package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateEducationAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitUpdateEducationAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitUpdateEducationAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:07
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitUpdateEducationAbilityServiceImpl implements GraduationRecruitUpdateEducationAbilityService {
    @Autowired
    private GraduationRecruitUpdateEducationBusiService graduationRecruitUpdateEducationBusiService;
    @Override
    public GraduationRecruitUpdateEducationAbilityRspBO updateEducation(GraduationRecruitUpdateEducationAbilityReqBO reqBO) {
        GraduationRecruitUpdateEducationBusiReqBO busiReqBO = new GraduationRecruitUpdateEducationBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitUpdateEducationBusiRspBO busiRspBO = graduationRecruitUpdateEducationBusiService.updateEducation(busiReqBO);
        GraduationRecruitUpdateEducationAbilityRspBO rspBO = new GraduationRecruitUpdateEducationAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
