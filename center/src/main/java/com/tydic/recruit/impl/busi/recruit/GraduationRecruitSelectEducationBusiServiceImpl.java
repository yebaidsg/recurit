package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.EducationalExperienceMapper;
import com.tydic.recruit.dao.recurit.po.EducationalExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitSelectEducationBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:16
 */
@Service("GraduationRecruitSelectEducationBusiService")
public class GraduationRecruitSelectEducationBusiServiceImpl implements GraduationRecruitSelectEducationBusiService {
    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;
    @Override
    public GraduationRecruitSelectEducationBusiRspBO selectEducation(GraduationRecruitSelectEducationBusiReqBO reqBO) {
        GraduationRecruitSelectEducationBusiRspBO rspBO = new GraduationRecruitSelectEducationBusiRspBO();
        EducationalExperiencePO educationalExperiencePO = new EducationalExperiencePO();
        BeanUtils.copyProperties(reqBO,educationalExperiencePO);
        EducationalExperiencePO experiencePO = educationalExperienceMapper.select(educationalExperiencePO);
        if (null != experiencePO){
            BeanUtils.copyProperties(experiencePO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
