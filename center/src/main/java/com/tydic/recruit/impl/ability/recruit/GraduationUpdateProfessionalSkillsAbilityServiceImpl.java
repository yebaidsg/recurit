package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProfessionalSkillsAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateProfessionalSkillsAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateProfessionalSkillsAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:52
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateProfessionalSkillsAbilityServiceImpl implements GraduationUpdateProfessionalSkillsAbilityService {
    @Autowired
    private GraduationUpdateProfessionalSkillsBusiService graduationUpdateProfessionalSkillsBusiService;
    @Override
    public GraduationUpdateProfessionalSkillsAbilityRspBO updateProfession(GraduationUpdateProfessionalSkillsAbilityReqBO reqBO) {
        GraduationUpdateProfessionalSkillsBusiReqBO busiReqBO = new GraduationUpdateProfessionalSkillsBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateProfessionalSkillsBusiRspBO busiRspBO = graduationUpdateProfessionalSkillsBusiService.updateProfession(busiReqBO);
        GraduationUpdateProfessionalSkillsAbilityRspBO rspBO = new GraduationUpdateProfessionalSkillsAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
