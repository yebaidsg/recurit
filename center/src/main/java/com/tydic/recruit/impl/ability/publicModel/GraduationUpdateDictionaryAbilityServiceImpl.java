package com.tydic.recruit.impl.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.GraduationUpdateDictionaryAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationUpdateDictionaryAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationUpdateDictionaryAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationUpdateDictionaryBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateDictionaryAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:33
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateDictionaryAbilityServiceImpl implements GraduationUpdateDictionaryAbilityService {
    @Autowired
    private GraduationUpdateDictionaryBusiService graduationUpdateDictionaryBusiService;
    @Override
    public GraduationUpdateDictionaryAbilityRspBO updateDictionary(GraduationUpdateDictionaryAbilityReqBO reqBO) {
        if (null == reqBO.getOperType()){
            throw new BusinessException(RspConstantBo.FailRspCode,"操作类型为空");
        }
        GraduationUpdateDictionaryBusiReqBO busiReqBO = new GraduationUpdateDictionaryBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateDictionaryBusiRspBO busiRspBO = graduationUpdateDictionaryBusiService.updateDictionary(busiReqBO);
        GraduationUpdateDictionaryAbilityRspBO rspBO = new GraduationUpdateDictionaryAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
