package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectProfessionalSkillsListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.ProfessionalSkillsBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.ProfessionalSkillsMapper;
import com.tydic.recruit.dao.recurit.po.ProfessionalSkillsPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectProfessionalSkillsListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:32
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectProfessionalSkillsListBusiServiceImpl implements GraduationSelectProfessionalSkillsListBusiService {
    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;
    @Override
    public GraduationSelectProfessionalSkillsListBusiRspBO selectSkillsList(GraduationSelectProfessionalSkillsListBusiReqBO reqBO) {
        GraduationSelectProfessionalSkillsListBusiRspBO rspBO = new GraduationSelectProfessionalSkillsListBusiRspBO();
        ProfessionalSkillsPO professionalSkillsPO = new ProfessionalSkillsPO();
        professionalSkillsPO.setPersonId(reqBO.getPersonId());
        List<ProfessionalSkillsPO> list = professionalSkillsMapper.selectList(professionalSkillsPO);
        List<ProfessionalSkillsBO> professionalSkillsBOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            for (ProfessionalSkillsPO skillsPO : list) {
                ProfessionalSkillsBO professionalSkillsBO = new ProfessionalSkillsBO();
                BeanUtils.copyProperties(skillsPO,professionalSkillsBO);
                professionalSkillsBO.setSkillsId(skillsPO.getSkillsId().toString());
                if (null != skillsPO.getMastery()){
                    switch (skillsPO.getMastery()){
                        case "1":
                            professionalSkillsBO.setMastery("一般");
                            break;
                        case "2":
                            professionalSkillsBO.setMastery("良好");
                            break;
                        case "3":
                            professionalSkillsBO.setMastery("熟练");
                            break;
                        case "4":
                            professionalSkillsBO.setMastery("精通");
                            break;
                        default:
                            professionalSkillsBO.setMastery("无");
                            break;
                    }
                }
                professionalSkillsBOS.add(professionalSkillsBO);
            }
            rspBO.setProfessionalSkillsBOS(professionalSkillsBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
