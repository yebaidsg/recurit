package com.tydic.recruit.impl.busi.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.ability.publicModel.bo.ProvinceBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectProvinceBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.publicmodel.ProvinceMapper;
import com.tydic.recruit.dao.publicmodel.po.ProvincePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @标题 GraduationSelectProvinceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:54
 */
@Service("GraduationSelectProvinceBusiService")
public class GraduationSelectProvinceBusiServiceImpl implements GraduationSelectProvinceBusiService {

    @Autowired
    private ProvinceMapper provinceMapper;
    @Override
    public GraduationSelectProvinceBusiRspBO selectProvince(GraduationSelectProvinceBusiReqBO reqBO) {
        GraduationSelectProvinceBusiRspBO rspBO = new GraduationSelectProvinceBusiRspBO();
        ProvincePO provincePO = new ProvincePO();
        if (null != reqBO){
            BeanUtils.copyProperties(reqBO,provincePO);
        }
        Page<ProvincePO> page = new Page<>(reqBO.getPageNo(),reqBO.getPageSize());
        List<ProvincePO> provincePOS = provinceMapper.selectProvinceList(provincePO,page);
        if (!CollectionUtils.isEmpty(provincePOS)){
            String jsonString = JSONObject.toJSONString(provincePOS, SerializerFeature.WriteMapNullValue);
            List<ProvinceBO> list = JSONObject.parseArray(jsonString,ProvinceBO.class);
            rspBO.setRows(list);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(page.getTotalCount());
        return rspBO;
    }
}
