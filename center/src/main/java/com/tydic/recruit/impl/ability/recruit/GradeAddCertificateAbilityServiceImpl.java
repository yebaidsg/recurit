package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GradeAddCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GradeAddCertificateAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GradeAddCertificateAbilityService;
import com.tydic.recruit.api.busi.recruit.GradeAddCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GradeAddCertificateAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:20
 */
@HTServiceImpl
@Slf4j
public class GradeAddCertificateAbilityServiceImpl implements GradeAddCertificateAbilityService {
    @Autowired
    private GradeAddCertificateBusiService gradeAddCertificateBusiService;
    @Override
    public GradeAddCertificateAbilityRspBO addCertificate(GradeAddCertificateAbilityReqBO reqBO) {
        GradeAddCertificateBusiReqBO busiReqBO = new GradeAddCertificateBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GradeAddCertificateBusiRspBO busiRspBO = gradeAddCertificateBusiService.addCertificate(busiReqBO);
        GradeAddCertificateAbilityRspBO rspBO = new GradeAddCertificateAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
