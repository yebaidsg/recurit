package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddWorkExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationAddWorkExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationAddWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationAddWorkExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 14:32
 */
@HTServiceImpl
@Slf4j
public class GraduationAddWorkExperienceAbilityServiceImpl implements GraduationAddWorkExperienceAbilityService {
    @Autowired
    private GraduationAddWorkExperienceBusiService graduationAddWorkExperienceBusiService;
    @Override
    public GraduationAddWorkExperienceAbilityRspBO addWorkExperience(GraduationAddWorkExperienceAbilityReqBO reqBO) {
        GraduationAddWorkExperienceBusiReqBO busiReqBO = new GraduationAddWorkExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationAddWorkExperienceBusiRspBO busiRspBO = graduationAddWorkExperienceBusiService.addWorkExperience(busiReqBO);
        GraduationAddWorkExperienceAbilityRspBO rspBO = new GraduationAddWorkExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
