package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectEducationAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitSelectEducationAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitSelectEducationAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:14
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitSelectEducationAbilityServiceImpl implements GraduationRecruitSelectEducationAbilityService {
    @Autowired
    private GraduationRecruitSelectEducationBusiService graduationRecruitSelectEducationBusiService;
    @Override
    public GraduationRecruitSelectEducationAbilityRspBO selectEducation(GraduationRecruitSelectEducationAbilityReqBO reqBO) {
        GraduationRecruitSelectEducationBusiReqBO busiReqBO = new GraduationRecruitSelectEducationBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitSelectEducationBusiRspBO busiRspBO = graduationRecruitSelectEducationBusiService.selectEducation(busiReqBO);
        GraduationRecruitSelectEducationAbilityRspBO rspBO = new GraduationRecruitSelectEducationAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
