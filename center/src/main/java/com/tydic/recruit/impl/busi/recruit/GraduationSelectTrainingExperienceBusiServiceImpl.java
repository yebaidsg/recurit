package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.TrainingExperienceMapper;
import com.tydic.recruit.dao.recurit.po.TrainingExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectTrainingExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:04
 */
@Service("GraduationSelectTrainingExperienceBusiService")
public class GraduationSelectTrainingExperienceBusiServiceImpl implements GraduationSelectTrainingExperienceBusiService {
    @Autowired
    private TrainingExperienceMapper trainingExperienceMapper;
    @Override
    public GraduationSelectTrainingExperienceBusiRspBO selectTrainingList(GraduationSelectTrainingExperienceBusiReqBO reqBO) {
        TrainingExperiencePO trainingExperiencePO = new TrainingExperiencePO();
        BeanUtils.copyProperties(reqBO,trainingExperiencePO);
        TrainingExperiencePO experiencePO = trainingExperienceMapper.select(trainingExperiencePO);
        GraduationSelectTrainingExperienceBusiRspBO rspBO = new GraduationSelectTrainingExperienceBusiRspBO();
        if (null != experiencePO){
            BeanUtils.copyProperties(experiencePO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
