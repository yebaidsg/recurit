package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectTrainingExperienceListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.TrainingExperienceBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.TrainingExperienceMapper;
import com.tydic.recruit.dao.recurit.po.TrainingExperiencePO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectTrainingExperienceListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:56
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectTrainingExperienceListBusiServiceImpl implements GraduationSelectTrainingExperienceListBusiService {
    @Autowired
    private TrainingExperienceMapper trainingExperienceMapper;
    @Override
    public GraduationSelectTrainingExperienceListBusiRspBO selectList(GraduationSelectTrainingExperienceListBusiReqBO reqBO) {
        GraduationSelectTrainingExperienceListBusiRspBO rspBO = new GraduationSelectTrainingExperienceListBusiRspBO();
        TrainingExperiencePO trainingExperiencePO = new TrainingExperiencePO();
        trainingExperiencePO.setPersonId(reqBO.getPersonId());
        List<TrainingExperiencePO> list = trainingExperienceMapper.selectList(trainingExperiencePO);
        List<TrainingExperienceBO> trainingExperienceBOS = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-DD");
        if (!CollectionUtils.isEmpty(list)){
            for (TrainingExperiencePO experiencePO : list) {
                TrainingExperienceBO trainingExperienceBO = new TrainingExperienceBO();
                BeanUtils.copyProperties(experiencePO,trainingExperienceBO);
                trainingExperienceBO.setTrainingId(experiencePO.getTrainingId().toString());
                trainingExperienceBO.setTrainingStartTime(simpleDateFormat.format(experiencePO.getTrainingStartTime()));
                trainingExperienceBO.setTrainingEndTime(simpleDateFormat.format(experiencePO.getTrainingEndTime()));
                trainingExperienceBOS.add(trainingExperienceBO);
            }
            rspBO.setTrainingExperienceBOS(trainingExperienceBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
