package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectCertificateAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectCertificateAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectCertificateAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:41
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectCertificateAbilityServiceImpl implements GraduationSelectCertificateAbilityService {
    @Autowired
    private GraduationSelectCertificateBusiService graduationSelectCertificateBusiService;
    @Override
    public GraduationSelectCertificateAbilityRspBO selectCertificate(GraduationSelectCertificateAbilityReqBO reqBO) {
        GraduationSelectCertificateBusiReqBO busiReqBO = new GraduationSelectCertificateBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectCertificateBusiRspBO busiRspBO = graduationSelectCertificateBusiService.selectCertificate(busiReqBO);
        GraduationSelectCertificateAbilityRspBO rspBO = new GraduationSelectCertificateAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
