//package com.tydic.recruit.impl.atom.impl;
//
//import com.tydic.recruit.dao.NextOrderDAO;
//import com.tydic.recruit.dao.OrderDAO;
//import com.tydic.recruit.dao.po.MainOrderPo;
//import com.tydic.recruit.dao.po.NextOrder;
//import com.tydic.recruit.impl.atom.CreateOrderAtomService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @author 胡中宝
// * @Description TODO
// * @copyright 2020 www.tydic.com Inc. All rights reserved.
// * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
// * @since 2020/10/21 11:22
// */
//@Service
//public class CreateOrderAtomServiceImpl implements CreateOrderAtomService {
//
//    @Autowired
//    private OrderDAO orderDAO;
//    @Autowired
//    private NextOrderDAO nextOrderDAO;
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void insertMainOrder(MainOrderPo mainOrderPo) {
//        orderDAO.insertMain(mainOrderPo);
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void insertNextOrder(List<NextOrder> nextOrders) {
//        for (NextOrder nextOrder : nextOrders) {
//            nextOrderDAO.insert(nextOrder);
//        }
//    }
//}
