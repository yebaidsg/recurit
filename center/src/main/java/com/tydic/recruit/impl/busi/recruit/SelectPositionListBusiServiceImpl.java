package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.busi.recruit.SelectPositionListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.PositionBO;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionListBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 SelectPositionListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:45
 */
@HTServiceImpl
@Slf4j
public class SelectPositionListBusiServiceImpl implements SelectPositionListBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Override
    public SelectPositionListBusiRspBO selectPosiList(SelectPositionListBusiReqBO reqBO) {
        /**
         * 根据userId查询
         */
        SelectPositionListBusiRspBO rspBO = new SelectPositionListBusiRspBO();
        PositionPO positionPO = new PositionPO();
        BeanUtils.copyProperties(reqBO,positionPO);
        Page page = new Page(reqBO.getPageNo(),-1);
        if (null != reqBO.getProvinceName()){
            if (reqBO.getProvinceName().length() < 2){
                positionPO.setWorkPlace(reqBO.getProvinceName());
            } else{
                String province1 = String.valueOf(reqBO.getProvinceName().charAt(0));
                String province2 = String.valueOf(reqBO.getProvinceName().charAt(1));
                positionPO.setWorkPlace(province1+province2);
            }
        }
        List<PositionPO> positionPOS = positionMapper.selectList(positionPO,page);
        if (null != reqBO.getCityName()){
            positionPO.setWorkPlace(reqBO.getCityName());
        }
        if (null != reqBO.getAreaName()){
            positionPO.setWorkPlace(reqBO.getAreaName());
        }
        List<PositionBO> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(positionPOS)){
            for (PositionPO po : positionPOS) {
                PositionBO positionBO = new PositionBO();
                BeanUtils.copyProperties(po,positionBO);
                positionBO.setSalary(po.getLowSalary()+"K"+"-"+po.getHighSalary()+"K");
                if (null != po.getPositionResource()){
                    if ("1".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("前程无忧网");
                    } else if ("2".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("汇博人才网");
                    } else if ("3".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("中华英才网");
                    }
                }
                list.add(positionBO);
            }
        }
        rspBO.setRows(list);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(positionMapper.selectCount(positionPO));
        rspBO.setRecordsTotal(page.getTotalCount());
        return rspBO;
    }
}
