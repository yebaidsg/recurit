package com.tydic.recruit.impl.busi.recruit;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:35
 */
@Service("GraduationRecruitUpdateJobIntentionalBusiService")
public class GraduationRecruitUpdateJobIntentionalBusiServiceImpl implements GraduationRecruitUpdateJobIntentionalBusiService {
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Override
    public GraduationRecruitUpdateJobIntentionalBusiRspBO updateJon(GraduationRecruitUpdateJobIntentionalBusiReqBO reqBO) {
        PersonJobIntentionPO personJobIntentionPO = new PersonJobIntentionPO();
        BeanUtils.copyProperties(reqBO,personJobIntentionPO);
        int updateCheck = personJobIntentionMapper.update(personJobIntentionPO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationRecruitUpdateJobIntentionalBusiRspBO rspBO = new GraduationRecruitUpdateJobIntentionalBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
