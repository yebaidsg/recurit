package com.tydic.recruit.impl.atom;

import com.tydic.recruit.dao.po.CloseOrderPO;

import java.util.List;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/22 14:23
 */
public interface CloseOrderAtomService {
    /**
     * 查看订单状态
     * @param mainOrderId
     * @param userId
     * @return
     */
    List<CloseOrderPO> orderState(Integer mainOrderId,Integer userId);
    /**
     * 修改订单状态
     */
    void updateNextOrderState(Integer nid,Integer sid);
}
