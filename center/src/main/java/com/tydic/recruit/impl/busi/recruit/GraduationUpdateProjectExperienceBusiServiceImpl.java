package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.ProjectExperienceMapper;
import com.tydic.recruit.dao.recurit.po.ProjectExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateProjectExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:09
 */
@Service("GraduationUpdateProjectExperienceBusiService")
public class GraduationUpdateProjectExperienceBusiServiceImpl implements GraduationUpdateProjectExperienceBusiService {
    @Autowired
    private ProjectExperienceMapper projectExperienceMapper;
    @Override
    public GraduationUpdateProjectExperienceBusiRspBO updateProject(GraduationUpdateProjectExperienceBusiReqBO reqBO) {
        ProjectExperiencePO projectExperiencePO = new ProjectExperiencePO();
        BeanUtils.copyProperties(reqBO,projectExperiencePO);
        int updateCheck = projectExperienceMapper.update(projectExperiencePO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateProjectExperienceBusiRspBO rspBO = new GraduationUpdateProjectExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
