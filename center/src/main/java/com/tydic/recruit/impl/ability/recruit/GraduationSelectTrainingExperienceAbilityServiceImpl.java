package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectTrainingExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectTrainingExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectTrainingExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 15:59
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectTrainingExperienceAbilityServiceImpl implements GraduationSelectTrainingExperienceAbilityService {
    @Autowired
    private GraduationSelectTrainingExperienceBusiService graduationSelectTrainingExperienceBusiService;
    @Override
    public GraduationSelectTrainingExperienceAbilityRspBO selectTraining(GraduationSelectTrainingExperienceAbilityReqBO reqBO) {
        GraduationSelectTrainingExperienceBusiReqBO busiReqBO = new GraduationSelectTrainingExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectTrainingExperienceBusiRspBO busiRspBO = graduationSelectTrainingExperienceBusiService.selectTrainingList(busiReqBO);
        GraduationSelectTrainingExperienceAbilityRspBO rspBO = new GraduationSelectTrainingExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
