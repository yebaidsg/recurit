package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectResumeAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitSelectResumeAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitSelectResumeAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:25
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitSelectResumeAbilityServiceImpl implements GraduationRecruitSelectResumeAbilityService {
    @Autowired
    private GraduationRecruitSelectResumeBusiService graduationRecruitSelectResumeBusiService;
    @Override
    public GraduationRecruitSelectResumeAbilityRspBO selectResume(GraduationRecruitSelectResumeAbilityReqBO reqBO) {
        GraduationRecruitSelectResumeBusiReqBO busiReqBO = new GraduationRecruitSelectResumeBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitSelectResumeBusiRspBO busiRspBO = graduationRecruitSelectResumeBusiService.selectResume(busiReqBO);
        GraduationRecruitSelectResumeAbilityRspBO rspBO = new GraduationRecruitSelectResumeAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
