package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectRecommendListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectRecommendListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectRecommendListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectRecommendListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/22 14:15
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectRecommendListBusiServiceImpl implements GraduationSelectRecommendListBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public GraduationSelectRecommendListBusiRspBO selectRecommedList(GraduationSelectRecommendListBusiReqBO reqBO) {
        GraduationSelectRecommendListBusiRspBO rspBO = new GraduationSelectRecommendListBusiRspBO();
        PositionPO positionPO = new PositionPO();
        List<PositionPO> positionPOS = positionMapper.selectPositionList(positionPO);
        List<PositionBO> positionBOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(positionPOS)){
            for (PositionPO po : positionPOS) {
                PositionBO positionBO = new PositionBO();
                BeanUtils.copyProperties(po,positionBO);
                if (null != po.getExperience()){
                    if ("0".equals(po.getExperience())){
                        positionBO.setExperience("经验不限");
                    } else {
                        positionBO.setExperience(po.getExperience() + "年以上");
                    }
                }
                if (null != po.getPositionResource()){
                    if ("1".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("前程无忧网");
                    } else if ("2".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("汇博人才网");
                    } else if ("3".equals(po.getPositionResource())){
                        positionBO.setPositionResourceStr("中华英才网");
                    }
                }
                positionBOList.add(positionBO);
            }
        }
        rspBO.setProjectExperienceBOS(positionBOList);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
