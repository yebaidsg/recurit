package com.tydic.recruit.impl.busi.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.ability.publicModel.bo.AreaBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectAreaBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.publicmodel.ProvinceMapper;
import com.tydic.recruit.dao.publicmodel.po.AreaPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @标题 GraduationSelectAreaBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:50
 */
@Service("GraduationSelectAreaBusiService")
public class GraduationSelectAreaBusiServiceImpl implements GraduationSelectAreaBusiService {

    @Autowired
    private ProvinceMapper provinceMapper;
    @Override
    public GraduationSelectAreaBusiRspBO selectAreaList(GraduationSelectAreaBusiReqBO reqBO) {
        GraduationSelectAreaBusiRspBO rspBO = new GraduationSelectAreaBusiRspBO();
        AreaPO areaPO = new AreaPO();
        BeanUtils.copyProperties(reqBO,areaPO);
        Page<AreaPO> page = new Page<>(reqBO.getPageNo(),reqBO.getPageSize());
        List<AreaPO> areaPOS = provinceMapper.selectAreaList(areaPO,page);
        if (!CollectionUtils.isEmpty(areaPOS)){
            List<AreaBO> list = JSONObject.parseArray(JSONObject.toJSONString(areaPOS, SerializerFeature.WriteMapNullValue),AreaBO.class);
            rspBO.setRows(list);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(page.getTotalCount());
        return rspBO;
    }
}
