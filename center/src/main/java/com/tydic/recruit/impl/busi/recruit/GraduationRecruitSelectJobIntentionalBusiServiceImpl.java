package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitSelectJobIntentionalBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:42
 */
@Service("GraduationRecruitSelectJobIntentionalBusiService")
public class GraduationRecruitSelectJobIntentionalBusiServiceImpl implements GraduationRecruitSelectJobIntentionalBusiService {
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Override
    public GraduationRecruitSelectJobIntentionalBusiRspBO selectJob(GraduationRecruitSelectJobIntentionalBusiReqBO reqBO) {
        PersonJobIntentionPO personJobIntentionPO = new PersonJobIntentionPO();
        BeanUtils.copyProperties(reqBO,personJobIntentionPO);
        PersonJobIntentionPO intentionPO = personJobIntentionMapper.select(personJobIntentionPO);
        GraduationRecruitSelectJobIntentionalBusiRspBO rspBO = new GraduationRecruitSelectJobIntentionalBusiRspBO();
        BeanUtils.copyProperties(intentionPO,rspBO);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
