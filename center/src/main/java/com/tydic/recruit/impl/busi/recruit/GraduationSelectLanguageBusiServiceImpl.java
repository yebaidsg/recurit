package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.LanguageAbilityMapper;
import com.tydic.recruit.dao.recurit.po.LanguageAbilityPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectLanguageBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:14
 */
@Service("GraduationSelectLanguageBusiService")
public class GraduationSelectLanguageBusiServiceImpl implements GraduationSelectLanguageBusiService {
    @Autowired
    private LanguageAbilityMapper languageAbilityMapper;
    @Override
    public GraduationSelectLanguageBusiRspBO selectLanguage(GraduationSelectLanguageBusiReqBO reqBO) {
        GraduationSelectLanguageBusiRspBO rspBO = new GraduationSelectLanguageBusiRspBO();
        LanguageAbilityPO languageAbilityPO = new LanguageAbilityPO();
        BeanUtils.copyProperties(reqBO,languageAbilityPO);
        LanguageAbilityPO abilityPO = languageAbilityMapper.select(languageAbilityPO);
        if (null != abilityPO){
            BeanUtils.copyProperties(abilityPO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
