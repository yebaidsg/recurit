package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectEvaluateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluateBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.EvaluatePositionMapper;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluateBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:11
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectEvaluateBusiServiceImpl implements GraduationSelectEvaluateBusiService {
    @Autowired
    private EvaluatePositionMapper evaluatePositionMapper;
    @Override
    public GraduationSelectEvaluateBusiRspBO selectEvaluate(GraduationSelectEvaluateBusiReqBO reqBO) {
        int count = evaluatePositionMapper.countEvaluate(reqBO.getUserId());
        GraduationSelectEvaluateBusiRspBO rspBO = new GraduationSelectEvaluateBusiRspBO();
        rspBO.setCount(count);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
