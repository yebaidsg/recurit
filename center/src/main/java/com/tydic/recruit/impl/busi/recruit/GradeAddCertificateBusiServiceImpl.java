package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GradeAddCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.CertificateMapper;
import com.tydic.recruit.dao.recurit.po.CertificatePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GradeAddCertificateBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:23
 */
@Service("GradeAddCertificateBusiService")
public class GradeAddCertificateBusiServiceImpl implements GradeAddCertificateBusiService {
    @Autowired
    private CertificateMapper certificateMapper;
    @Override
    public GradeAddCertificateBusiRspBO addCertificate(GradeAddCertificateBusiReqBO reqBO) {
        CertificatePO certificatePO = new CertificatePO();
        BeanUtils.copyProperties(reqBO,certificatePO);
        if (null != reqBO.getCertificateId()){
            certificateMapper.update(certificatePO);
        } else {
            certificatePO.setCertificateId(Sequence.getInstance().nextId());
            int insertCheck = certificateMapper.insert(certificatePO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"新增失败");
            }
        }
        GradeAddCertificateBusiRspBO rspBO = new GradeAddCertificateBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
