package com.tydic.recruit.impl.atom;

import com.tydic.recruit.dao.po.MainOrderPo;
import com.tydic.recruit.dao.po.NextOrder;

import java.util.List;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/21 11:15
 */
public interface CreateOrderAtomService {
    void insertMainOrder(MainOrderPo mainOrderPo);
    void insertNextOrder(List<NextOrder> nextOrders);
}
