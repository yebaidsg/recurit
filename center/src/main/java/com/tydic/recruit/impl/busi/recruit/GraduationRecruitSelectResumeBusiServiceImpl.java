package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitSelectResumeBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiRspBO;
import com.tydic.recruit.common.base.bo.RspBaseBo;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PersonalInformationMapper;
import com.tydic.recruit.dao.recurit.po.PersonalInformationPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitSelectResumeBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:28
 */
@Service("GraduationRecruitSelectResumeBusiService")
public class GraduationRecruitSelectResumeBusiServiceImpl implements GraduationRecruitSelectResumeBusiService {
    @Autowired
    private PersonalInformationMapper personalInformationMapper;
    @Override
    public GraduationRecruitSelectResumeBusiRspBO selectResume(GraduationRecruitSelectResumeBusiReqBO reqBO) {
        PersonalInformationPO personalInformationPO = new PersonalInformationPO();
        BeanUtils.copyProperties(reqBO,personalInformationPO);
        PersonalInformationPO informationPO = personalInformationMapper.select(personalInformationPO);
        String jsonString = JSONObject.toJSONString(informationPO, SerializerFeature.WriteMapNullValue);
        GraduationRecruitSelectResumeBusiRspBO rspBO = JSONObject.parseObject(jsonString,GraduationRecruitSelectResumeBusiRspBO.class);
        String[] address = new String[]{rspBO.getPersonProvinceCode(),rspBO.getPersonCityCode(),rspBO.getPersonAreaCode()};
        String[] juzhu = new String[]{rspBO.getPersonAddressProvinceCode(),rspBO.getPersonAddressCityCode(),rspBO.getPersonAddressAreaCode()};
        rspBO.setAddress(address);
        rspBO.setJuzhu(juzhu);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
