package com.tydic.recruit.impl.busi.python;

import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiRspBO;
import com.tydic.recruit.api.busi.python.DateCrawlingHandleBusiService;
import com.tydic.recruit.api.busi.recruit.InsertPositionBusiService;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author huzb
 * @标题 DateCrawlingHandleBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 17:24
 */
@HTServiceImpl
@Slf4j
public class DateCrawlingHandleBusiServiceImpl implements DateCrawlingHandleBusiService {
    private static final Integer FIVE = 1;
    private static final Integer HUIBO = 2;
    private static final Integer ZHONGHUA = 3;
    @Autowired
    private InsertPositionBusiService insertPositionBusiService;
    @Override
    public DateCrawlingHandleBusiRspBO dataHandle(DateCrawlingHandleBusiReqBO reqBO) {
        DateCrawlingHandleBusiRspBO rspBO = new DateCrawlingHandleBusiRspBO();
        String net = "";
        InsertPositionBusiServiceReqBO serviceReqBO = new InsertPositionBusiServiceReqBO();
        try{
            if (FIVE.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\job51_4.py";
                serviceReqBO.setPath("D:\\Gitworkplace\\recurit\\51job15.csv");
            } else if (HUIBO.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\huibo1.py";
                serviceReqBO.setPath("D:\\Gitworkplace\\recurit\\Hb12.csv");
            } else if (ZHONGHUA.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\ycw_1.py";
                serviceReqBO.setPath("D:\\Gitworkplace\\recurit\\ycw4.csv");
            }
            serviceReqBO.setPositionResource(reqBO.getNetState().toString());
            String[] args1 = new String[]{"C:\\ProgramData\\Anaconda3\\envs\\recurit\\python.exe",net};
            Process pr=Runtime.getRuntime().exec(args1);
            BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream(),"GBK"));
            System.out.println(in);
            System.out.println(pr.getInputStream());
            String line = null;
            System.out.println(in.readLine());
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
            in.close();
            pr.waitFor();
            /**
             * 将数据导入到数据库
             */
            InsertPositionBusiServiceRspBO serviceRspBO =  insertPositionBusiService.insert(serviceReqBO);
            System.out.println("end");
        } catch (Exception e){
            e.printStackTrace();
        }
//        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
