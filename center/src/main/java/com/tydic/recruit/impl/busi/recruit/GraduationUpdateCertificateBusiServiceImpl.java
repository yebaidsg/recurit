package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.CertificateMapper;
import com.tydic.recruit.dao.recurit.po.CertificatePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateCertificateBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:36
 */
@Service("GraduationUpdateCertificateBusiService")
public class GraduationUpdateCertificateBusiServiceImpl implements GraduationUpdateCertificateBusiService {
    @Autowired
    private CertificateMapper certificateMapper;
    @Override
    public GraduationUpdateCertificateBusiRspBO updateCertificate(GraduationUpdateCertificateBusiReqBO reqBO) {
        CertificatePO certificatePO = new CertificatePO();
        BeanUtils.copyProperties(reqBO,certificatePO);
        int updateCheck = certificateMapper.update(certificatePO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateCertificateBusiRspBO rspBO =  new GraduationUpdateCertificateBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
