package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddEducationBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.EducationalExperienceMapper;
import com.tydic.recruit.dao.recurit.po.EducationalExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationRecruitAddEducationBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:02
 */
@Service("GraduationRecruitAddEducationBusiService")
public class GraduationRecruitAddEducationBusiServiceImpl implements GraduationRecruitAddEducationBusiService {
    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;
    @Override
    public GraduationRecruitAddEducationBusiRspBO addEducation(GraduationRecruitAddEducationBusiReqBO reqBO) {
        EducationalExperiencePO educationalExperiencePO = new EducationalExperiencePO();
        BeanUtils.copyProperties(reqBO,educationalExperiencePO);
        if (null != reqBO.getEduId()){
            educationalExperienceMapper.update(educationalExperiencePO);
        } else if (null == reqBO.getEduId()){
            educationalExperiencePO.setEduId(Sequence.getInstance().nextId());
            int insert = educationalExperienceMapper.insert(educationalExperiencePO);
            if (insert < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationRecruitAddEducationBusiRspBO rspBO = new GraduationRecruitAddEducationBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
