package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.tydic.recruit.api.busi.recruit.GraduationSelectJonIntentionalListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalListBusiReq;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalListBusiRsp;
import com.tydic.recruit.api.busi.recruit.bo.JobIntentionalBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.PersonJobIntentionMapper;
import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectJonIntentionalListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 15:50
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectJonIntentionalListBusiServiceImpl implements GraduationSelectJonIntentionalListBusiService {
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;
    @Override
    public GraduationRecruitSelectJobIntentionalListBusiRsp selectJonIntentionalList(GraduationRecruitSelectJobIntentionalListBusiReq req) {
        GraduationRecruitSelectJobIntentionalListBusiRsp rsp = new GraduationRecruitSelectJobIntentionalListBusiRsp();
        PersonJobIntentionPO personJobIntentionPO = new PersonJobIntentionPO();
        personJobIntentionPO.setPersonId(req.getPersonId());
        List<PersonJobIntentionPO> lists = personJobIntentionMapper.selectList(personJobIntentionPO);
        List<JobIntentionalBO> jobIntentionalBOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(lists)){
            for (PersonJobIntentionPO list : lists) {
                JobIntentionalBO jobIntentionalBO = new JobIntentionalBO();
                BeanUtils.copyProperties(list,jobIntentionalBO);
                if (null != list.getSalaryRequirements()){
                    if ("1".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("1000元/月以下");
                    } else if ("2".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("1000-2000元/月");
                    } else if ("3".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("2001-4000元/月");
                    } else if ("4".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("4001-6000元/月");
                    } else if ("5".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("6001-8000元/月");
                    } else if ("6".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("8001-10000元/月");
                    } else if ("7".equals(list.getSalaryRequirements())){
                        jobIntentionalBO.setSalaryRequirements("1000元-15000/月");
                    }
                }
                if (null != list.getNatureOfWork()){
                    if ("1".equals(list.getNatureOfWork())){
                        jobIntentionalBO.setNatureOfWork("全职");
                    } else if ("2".equals(list.getNatureOfWork())){
                        jobIntentionalBO.setNatureOfWork("兼职");
                    } else if ("3".equals(list.getNatureOfWork())){
                        jobIntentionalBO.setNatureOfWork("实习生");
                    }
                }
                if (null != list.getFirstPositionCode() && null != list.getSecondPositionCode()){
                    jobIntentionalBO.setSecondPositionCode(list.getFirstPositionCode()+"/"+ list.getSecondPositionCode());
                }
                jobIntentionalBO.setIntentionId(list.getIntentionId().toString());
                jobIntentionalBOS.add(jobIntentionalBO);
            }
            if (!CollectionUtils.isEmpty(jobIntentionalBOS)){
                rsp.setJobIntentionalBOS(jobIntentionalBOS);
            }
        }
        rsp.setCode(RspConstantBo.SuccRspCode);
        rsp.setMessage(RspConstantBo.SuccRspDesc);
        return rsp;
    }
}
