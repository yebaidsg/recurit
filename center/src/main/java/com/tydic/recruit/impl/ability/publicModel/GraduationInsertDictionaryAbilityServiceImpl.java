package com.tydic.recruit.impl.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.GraduationInsertDictionaryAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationInsertDictionaryAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationInsertDictionaryAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationInsertDictionaryBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationInsertDictionaryAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:03
 */
@HTServiceImpl
@Slf4j
public class GraduationInsertDictionaryAbilityServiceImpl implements GraduationInsertDictionaryAbilityService {

    @Autowired
    private GraduationInsertDictionaryBusiService graduationInsertDictionaryBusiService;
    @Override
    public GraduationInsertDictionaryAbilityRspBO insertDictionary(GraduationInsertDictionaryAbilityReqBO reqBO) {
        initCheck(reqBO);
        GraduationInsertDictionaryBusiReqBO busiReqBO = new GraduationInsertDictionaryBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationInsertDictionaryBusiRspBO busiRspBO = graduationInsertDictionaryBusiService.insertDictionary(busiReqBO);
        GraduationInsertDictionaryAbilityRspBO rspBO = new GraduationInsertDictionaryAbilityRspBO();
        rspBO.setCode(busiRspBO.getCode());
        rspBO.setMessage(busiRspBO.getMessage());
        return rspBO;
    }

    public void initCheck(GraduationInsertDictionaryAbilityReqBO reqBO){
        if (StringUtils.isBlank(reqBO.getCode())){
            throw new BusinessException(RspConstantBo.FailRspCode,"字典编码不能为空");
        }
        if (StringUtils.isBlank(reqBO.getPCode())){
            throw new BusinessException(RspConstantBo.FailRspCode,"字典pCode不能为空");
        }
        if (StringUtils.isBlank(reqBO.getTitle())){
            throw new BusinessException(RspConstantBo.FailRspCode,"字典类型不能为空");
        }
    }
}
