package com.tydic.recruit.impl.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.GraduationUpdateDictionaryBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.publicmodel.RecruitDictionaryMapper;
import com.tydic.recruit.dao.publicmodel.po.RecruitDictionaryPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateDictionaryBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:34
 */
@Service("GraduationUpdateDictionaryBusiService")
public class GraduationUpdateDictionaryBusiServiceImpl implements GraduationUpdateDictionaryBusiService {
    @Autowired
    private RecruitDictionaryMapper recruitDictionaryMapper;
    private static final String DELETE = "1";
    private static final String UPDATE = "0";
    private static final String DELETE_STATUS = "1";
    @Override
    public GraduationUpdateDictionaryBusiRspBO updateDictionary(GraduationUpdateDictionaryBusiReqBO reqBO) {
        RecruitDictionaryPO recruitDictionaryPO = new RecruitDictionaryPO();
        if (DELETE.equals(reqBO.getOperType())){
            recruitDictionaryPO.setStatus(DELETE_STATUS);
            recruitDictionaryPO.setDicId(reqBO.getDicId());
        } else if (UPDATE.equals(reqBO.getOperType())){
            BeanUtils.copyProperties(reqBO,recruitDictionaryPO);
        }
        int updateCheck = recruitDictionaryMapper.updateDictionary(recruitDictionaryPO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改字典信息失败");
        }
        GraduationUpdateDictionaryBusiRspBO rspBO = new GraduationUpdateDictionaryBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
