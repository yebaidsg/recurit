package com.tydic.recruit.impl.ability.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectProvinceAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectProvinceAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectProvinceAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectProvinceBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectProvinceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:24
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectProvinceAbilityServiceImpl implements GraduationSelectProvinceAbilityService {
    @Autowired
    private GraduationSelectProvinceBusiService graduationSelectProvinceBusiService;
    @Override
    public GraduationSelectProvinceAbilityRspBO SelectProvince(GraduationSelectProvinceAbilityReqBO reqBO) {
        GraduationSelectProvinceBusiReqBO busiReqBO = new GraduationSelectProvinceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectProvinceBusiRspBO busiRspBO = graduationSelectProvinceBusiService.selectProvince(busiReqBO);
        return JSONObject.parseObject(JSONObject.toJSONString(busiRspBO, SerializerFeature.WriteMapNullValue),GraduationSelectProvinceAbilityRspBO.class);
    }
}
