package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddTrainingExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationAddTrainingExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationAddTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationAddTrainingExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:31
 */
@HTServiceImpl
@Slf4j
public class GraduationAddTrainingExperienceAbilityServiceImpl implements GraduationAddTrainingExperienceAbilityService {
    @Autowired
    private GraduationAddTrainingExperienceBusiService graduationAddTrainingExperienceBusiService;
    @Override
    public GraduationAddTrainingExperienceAbilityRspBO addTraining(GraduationAddTrainingExperienceAbilityReqBO reqBO) {
        GraduationAddTrainingExperienceBusiReqBO busiReqBO = new GraduationAddTrainingExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationAddTrainingExperienceBusiRspBO busiRspBO = graduationAddTrainingExperienceBusiService.addTraining(busiReqBO);
        GraduationAddTrainingExperienceAbilityRspBO rspBO = new GraduationAddTrainingExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
