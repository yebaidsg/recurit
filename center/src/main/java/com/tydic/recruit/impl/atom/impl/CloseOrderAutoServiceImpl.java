//package com.tydic.recruit.impl.atom.impl;
//
//import com.tydic.recruit.dao.CloseOrderDAO;
//import com.tydic.recruit.dao.po.CloseOrderPO;
//import com.tydic.recruit.impl.atom.CloseOrderAtomService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @author 胡中宝
// * @Description TODO
// * @copyright 2020 www.tydic.com Inc. All rights reserved.
// * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
// * @since 2020/10/22 14:26
// */
//@Service
//public class CloseOrderAutoServiceImpl implements CloseOrderAtomService {
//
//    @Autowired
//    private CloseOrderDAO closeOrderDAO;
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public List<CloseOrderPO> orderState(Integer mainOrderId, Integer userId) {
//        List<CloseOrderPO> lists = closeOrderDAO.closeNextOrder(mainOrderId, userId);
//        return closeOrderDAO.closeNextOrder(mainOrderId, userId);
//    }
//
//    @Override
//    public void updateNextOrderState(Integer nid, Integer sid) {
//        closeOrderDAO.updateOrderState(nid, sid);
//    }
//}
