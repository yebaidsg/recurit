package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.ProfessionalSkillsMapper;
import com.tydic.recruit.dao.recurit.po.ProfessionalSkillsPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectProfessionalSkillsBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:08
 */
@Service("GraduationSelectProfessionalSkillsBusiService")
public class GraduationSelectProfessionalSkillsBusiServiceImpl implements GraduationSelectProfessionalSkillsBusiService {
    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;
    @Override
    public GraduationSelectProfessionalSkillsBusiRspBO selectProfession(GraduationSelectProfessionalSkillsBusiReqBO reqBO) {
        GraduationSelectProfessionalSkillsBusiRspBO rspBO = new GraduationSelectProfessionalSkillsBusiRspBO();
        ProfessionalSkillsPO professionalSkillsPO = new ProfessionalSkillsPO();
        BeanUtils.copyProperties(reqBO,professionalSkillsPO);
        ProfessionalSkillsPO skillsPO = professionalSkillsMapper.select(professionalSkillsPO);
        if (null != skillsPO){
            BeanUtils.copyProperties(skillsPO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
