package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationAddProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.ProjectExperienceMapper;
import com.tydic.recruit.dao.recurit.po.ProjectExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationAddProjectExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:07
 */
@Service("GraduationAddProjectExperienceBusiService")
public class GraduationAddProjectExperienceBusiServiceImpl implements GraduationAddProjectExperienceBusiService {
    @Autowired
    private ProjectExperienceMapper projectExperienceMapper;
    @Override
    public GraduationAddProjectExperienceBusiRspBO addProjectExperience(GraduationAddProjectExperienceBusiReqBO reqBO) {
        ProjectExperiencePO projectExperiencePO = new ProjectExperiencePO();
        BeanUtils.copyProperties(reqBO,projectExperiencePO);
        if (null != reqBO.getProjectId()){
            projectExperienceMapper.update(projectExperiencePO);
        } else {
            projectExperiencePO.setProjectId(Sequence.getInstance().nextId());
            int insertCheck = projectExperienceMapper.insert(projectExperiencePO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationAddProjectExperienceBusiRspBO rspBO = new GraduationAddProjectExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
