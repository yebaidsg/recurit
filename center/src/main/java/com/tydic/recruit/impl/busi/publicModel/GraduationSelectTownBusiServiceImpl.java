package com.tydic.recruit.impl.busi.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.ability.publicModel.bo.TownBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectTownBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.publicmodel.ProvinceMapper;
import com.tydic.recruit.dao.publicmodel.po.TownPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @标题 GraduationSelectTownBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:05
 */
@Service("GraduationSelectTownBusiService")
public class GraduationSelectTownBusiServiceImpl implements GraduationSelectTownBusiService {

    @Autowired
    private ProvinceMapper provinceMapper;
    @Override
    public GraduationSelectTownBusiRspBO selectTownList(GraduationSelectTownBusiReqBO reqBO) {
        GraduationSelectTownBusiRspBO rspBO = new GraduationSelectTownBusiRspBO();
        TownPO townPO = new TownPO();
        BeanUtils.copyProperties(reqBO,townPO);
        Page<TownPO> page = new Page<>(reqBO.getPageNo(),reqBO.getPageSize());
        List<TownPO> townPOS = provinceMapper.selectTownList(townPO,page);
        if (!CollectionUtils.isEmpty(townPOS)){
            rspBO.setRows(JSONObject.parseArray(JSONObject.toJSONString(townPOS, SerializerFeature.WriteMapNullValue), TownBO.class));
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(page.getTotalCount());
        return rspBO;
    }
}
