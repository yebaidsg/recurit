package com.tydic.recruit.impl.busi.user;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ozo.ability.PermsService;
import com.ozo.ability.bo.PermsBO;
import com.tydic.recruit.dao.user.PermsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermsServiceImpl implements PermsService {

    @Autowired
    PermsMapper permsMapper;

    @Override
    public List<PermsBO> findPermsByRoleId(Long id) {
        return JSONObject.parseArray(JSONObject.toJSONString(permsMapper.findPermsByRoleId(id), SerializerFeature.WriteMapNullValue),PermsBO.class);
    }

    @Override
    public List<PermsBO> findAllPerms() {
        return JSONObject.parseArray(JSONObject.toJSONString(permsMapper.findAllPerms(), SerializerFeature.WriteMapNullValue),PermsBO.class);
    }
}
