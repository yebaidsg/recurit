package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.WorkExperienceMapper;
import com.tydic.recruit.dao.recurit.po.WorkExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectWorkExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:14
 */
@Service("GraduationSelectWorkExperienceBusiService")
public class GraduationSelectWorkExperienceBusiServiceImpl implements GraduationSelectWorkExperienceBusiService {
    @Autowired
    private WorkExperienceMapper workExperienceMapper;
    @Override
    public GraduationSelectWorkExperienceBusiRspBO select(GraduationSelectWorkExperienceBusiReqBO reqBO) {
        WorkExperiencePO workExperiencePO = new WorkExperiencePO();
        BeanUtils.copyProperties(reqBO,workExperiencePO);
        WorkExperiencePO experiencePO = workExperienceMapper.select(workExperiencePO);
        GraduationSelectWorkExperienceBusiRspBO rspBO = new GraduationSelectWorkExperienceBusiRspBO();
        BeanUtils.copyProperties(experiencePO,rspBO);
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
