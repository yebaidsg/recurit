package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProfessionalSkillsAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectProfessionalSkillsAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectProfessionalSkillsAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:05
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectProfessionalSkillsAbilityServiceImpl implements GraduationSelectProfessionalSkillsAbilityService {
    @Autowired
    private GraduationSelectProfessionalSkillsBusiService graduationSelectProfessionalSkillsBusiService;
    @Override
    public GraduationSelectProfessionalSkillsAbilityRspBO selectProfession(GraduationSelectProfessionalSkillsAbilityReqBO reqBO) {
        GraduationSelectProfessionalSkillsBusiReqBO busiReqBO = new GraduationSelectProfessionalSkillsBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectProfessionalSkillsBusiRspBO busiRspBO = graduationSelectProfessionalSkillsBusiService.selectProfession(busiReqBO);
        GraduationSelectProfessionalSkillsAbilityRspBO rspBO = new GraduationSelectProfessionalSkillsAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
