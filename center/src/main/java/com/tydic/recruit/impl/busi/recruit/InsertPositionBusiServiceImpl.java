package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.tydic.recruit.api.busi.recruit.InsertPositionBusiService;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceRspBO;
import com.tydic.recruit.dao.recurit.PositionMapper;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import com.tydic.recruit.utils.CsvUtils;
import com.tydic.recruit.utils.bean.CsvBean;
import com.tydic.recruit.utils.csv.CsvUtil;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @标题 InsertPositionBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 19:10
 */
@Service("InsertPositionBusiService")
public class InsertPositionBusiServiceImpl implements InsertPositionBusiService {
    @Autowired
    private PositionMapper positionMapper;
    @Override
    public InsertPositionBusiServiceRspBO insert(InsertPositionBusiServiceReqBO reqBO) {
        /**
         * 修改所有职位类型
         */
        PositionPO positionPO = new PositionPO();
        positionPO.setPositionType("2");
        positionMapper.updatePositionType(positionPO);
        try {
//            CsvUtils csvUtils = new CsvUtils();
//            File file = new File(reqBO.getPath());
//            FileInputStream fileInputStream = new FileInputStream(file);
//            MultipartFile multipartFile = new MockMultipartFile(file.getName(), file.getName(),
//                    ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
            List<CsvBean> csvBeans = CsvUtil.readCsv(reqBO.getPath());
            List<CsvBean> newCsv = new ArrayList<>();
            for (CsvBean csvBean : csvBeans) {
                csvBean.setPositionResource(reqBO.getPositionResource());
            }
            for (CsvBean csvBean : csvBeans) {
                if (!csvBean.getJobTitle().equals("JobTitle")){
                    newCsv.add(csvBean);
                }
            }
            positionMapper.insert(newCsv);
        } catch (Exception e){
            e.printStackTrace();
        }
        InsertPositionBusiServiceRspBO rspBO = new InsertPositionBusiServiceRspBO();
        rspBO.setCode("0000");
        rspBO.setMessage("成功");
        return rspBO;
    }
}
