package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GradeSelectCertificateListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.CertificateBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeSelectCertificateListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeSelectCertificateListBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.CertificateMapper;
import com.tydic.recruit.dao.recurit.po.CertificatePO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GradeSelectCertificateListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:47
 */
@HTServiceImpl
@Slf4j
public class GradeSelectCertificateListBusiServiceImpl implements GradeSelectCertificateListBusiService {
    @Autowired
    private CertificateMapper certificateMapper;
    @Override
    public GradeSelectCertificateListBusiRspBO selectCertificateList(GradeSelectCertificateListBusiReqBO reqBO) {
        GradeSelectCertificateListBusiRspBO rspBO = new GradeSelectCertificateListBusiRspBO();
        CertificatePO certificatePO = new CertificatePO();
        certificatePO.setPersonId(reqBO.getPersonId());
        List<CertificatePO> list = certificateMapper.selectList(certificatePO);
        List<CertificateBO> certificateBOS = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (!CollectionUtils.isEmpty(list)){
            for (CertificatePO po : list) {
                CertificateBO certificateBO = new CertificateBO();
                BeanUtils.copyProperties(po,certificateBO);
                certificateBO.setCertificateGetTime(simpleDateFormat.format(po.getCertificateGetTime()));
                certificateBO.setCertificateId(po.getCertificateId().toString());
                certificateBOS.add(certificateBO);
            }
            rspBO.setCertificateBOS(certificateBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
