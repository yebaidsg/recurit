package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.ProfessionalSkillsMapper;
import com.tydic.recruit.dao.recurit.po.ProfessionalSkillsPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateProfessionalSkillsBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:56
 */
@Service("GraduationUpdateProfessionalSkillsBusiService")
public class GraduationUpdateProfessionalSkillsBusiServiceImpl implements GraduationUpdateProfessionalSkillsBusiService {
    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;
    @Override
    public GraduationUpdateProfessionalSkillsBusiRspBO updateProfession(GraduationUpdateProfessionalSkillsBusiReqBO reqBO) {
        ProfessionalSkillsPO professionalSkillsPO = new ProfessionalSkillsPO();
        BeanUtils.copyProperties(reqBO,professionalSkillsPO);
        int updateCheck = professionalSkillsMapper.update(professionalSkillsPO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateProfessionalSkillsBusiRspBO rspBO = new GraduationUpdateProfessionalSkillsBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
