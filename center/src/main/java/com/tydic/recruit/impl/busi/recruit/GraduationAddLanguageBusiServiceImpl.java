package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationAddLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.LanguageAbilityMapper;
import com.tydic.recruit.dao.recurit.po.LanguageAbilityPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationAddLanguageBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:19
 */
@Service("GraduationAddLanguageBusiService")
public class GraduationAddLanguageBusiServiceImpl implements GraduationAddLanguageBusiService {
    @Autowired
    private LanguageAbilityMapper languageAbilityMapper;
    @Override
    public GraduationAddLanguageBusiRspBO addLanguage(GraduationAddLanguageBusiReqBO reqBO) {
        LanguageAbilityPO languageAbilityPO = new LanguageAbilityPO();
        BeanUtils.copyProperties(reqBO,languageAbilityPO);
        if (null != reqBO.getLanguageId()){
            languageAbilityMapper.update(languageAbilityPO);
        } else {
            languageAbilityPO.setLanguageId(Sequence.getInstance().nextId());
            int insertCheck = languageAbilityMapper.insert(languageAbilityPO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationAddLanguageBusiRspBO rspBO = new GraduationAddLanguageBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
