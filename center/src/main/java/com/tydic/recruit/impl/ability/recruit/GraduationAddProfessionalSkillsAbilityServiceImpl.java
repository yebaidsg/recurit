package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationProfessionalSkillsAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationAddProfessionalSkillsAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationAddProfessionalSkillsBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationAddProfessionalSkillsAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:34
 */
@HTServiceImpl
@Slf4j
public class GraduationAddProfessionalSkillsAbilityServiceImpl implements GraduationAddProfessionalSkillsAbilityService {
    @Autowired
    private GraduationAddProfessionalSkillsBusiService graduationAddProfessionalSkillsBusiService;
    @Override
    public GraduationProfessionalSkillsAbilityRspBO addProfession(GraduationProfessionalSkillsAbilityReqBO reqBO) {
        GraduationAddProfessionalSkillsBusiReqBO busiReqBO = new GraduationAddProfessionalSkillsBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationAddProfessionalSkillsBusiRspBO busiRspBO = graduationAddProfessionalSkillsBusiService.addProfession(busiReqBO);
        GraduationProfessionalSkillsAbilityRspBO rspBO = new GraduationProfessionalSkillsAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
