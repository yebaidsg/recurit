package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateJobIntentionalAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitUpdateJobIntentionalAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitUpdateJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:32
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitUpdateJobIntentionalAbilityServiceImpl implements GraduationRecruitUpdateJobIntentionalAbilityService {
    @Autowired
    private GraduationRecruitUpdateJobIntentionalBusiService graduationRecruitUpdateJobIntentionalBusiService;
    @Override
    public GraduationRecruitUpdateJobIntentionalAbilityRspBO updateJob(GraduationRecruitUpdateJobIntentionalAbilityReqBO reqBO) {
        GraduationRecruitUpdateJobIntentionalBusiReqBO busiReqBO = new GraduationRecruitUpdateJobIntentionalBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationRecruitUpdateJobIntentionalBusiRspBO busiRspBO = graduationRecruitUpdateJobIntentionalBusiService.updateJon(busiReqBO);
        GraduationRecruitUpdateJobIntentionalAbilityRspBO rspBO = new GraduationRecruitUpdateJobIntentionalAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
