package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationSelectCertificateBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.CertificateMapper;
import com.tydic.recruit.dao.recurit.po.CertificatePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationSelectCertificateBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:44
 */
@Service("GraduationSelectCertificateBusiService")
public class GraduationSelectCertificateBusiServiceImpl implements GraduationSelectCertificateBusiService {
    @Autowired
    private CertificateMapper certificateMapper;
    @Override
    public GraduationSelectCertificateBusiRspBO selectCertificate(GraduationSelectCertificateBusiReqBO reqBO) {
        GraduationSelectCertificateBusiRspBO rspBO = new GraduationSelectCertificateBusiRspBO();
        CertificatePO certificatePO = new CertificatePO();
        BeanUtils.copyProperties(reqBO,certificatePO);
        CertificatePO certificatePO1 = certificateMapper.select(certificatePO);
        if (null != certificatePO1){
            BeanUtils.copyProperties(certificatePO,rspBO);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
