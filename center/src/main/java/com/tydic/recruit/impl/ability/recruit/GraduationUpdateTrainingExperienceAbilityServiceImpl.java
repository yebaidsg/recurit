package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateTrainingExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateTrainingExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateTrainingExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:51
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateTrainingExperienceAbilityServiceImpl implements GraduationUpdateTrainingExperienceAbilityService {
    @Autowired
    private GraduationUpdateTrainingExperienceBusiService graduationUpdateTrainingExperienceBusiService;
    @Override
    public GraduationUpdateTrainingExperienceAbilityRspBO updateTraining(GraduationUpdateTrainingExperienceAbilityReqBO reqBO) {
        GraduationUpdateTrainingExperienceBusiReqBO busiReqBO = new GraduationUpdateTrainingExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateTrainingExperienceBusiRspBO busiRspBO = graduationUpdateTrainingExperienceBusiService.updateTraining(busiReqBO);
        GraduationUpdateTrainingExperienceAbilityRspBO rspBO = new GraduationUpdateTrainingExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
