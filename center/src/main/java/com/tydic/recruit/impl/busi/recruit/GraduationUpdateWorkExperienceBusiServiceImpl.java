package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.WorkExperienceMapper;
import com.tydic.recruit.dao.recurit.po.WorkExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateWorkExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:06
 */
@Service("GraduationUpdateWorkExperienceBusiService")
public class GraduationUpdateWorkExperienceBusiServiceImpl implements GraduationUpdateWorkExperienceBusiService {
    @Autowired
    private WorkExperienceMapper workExperienceMapper;
    @Override
    public GraduationUpdateWorkExperienceBusiRspBO updateWork(GraduationUpdateWorkExperienceBusiReqBO reqBO) {
        WorkExperiencePO workExperiencePO = new WorkExperiencePO();
        BeanUtils.copyProperties(reqBO,workExperiencePO);
        int updateCheck = workExperienceMapper.update(workExperiencePO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateWorkExperienceBusiRspBO rspBO = new GraduationUpdateWorkExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
