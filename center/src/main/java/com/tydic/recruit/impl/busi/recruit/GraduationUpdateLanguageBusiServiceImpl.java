package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.LanguageAbilityMapper;
import com.tydic.recruit.dao.recurit.po.LanguageAbilityPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateLanguageBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:47
 */
@Service("GraduationUpdateLanguageBusiService")
public class GraduationUpdateLanguageBusiServiceImpl implements GraduationUpdateLanguageBusiService {
    @Autowired
    private LanguageAbilityMapper languageAbilityMapper;
    @Override
    public GraduationUpdateLanguageBusiRspBO updateLanguage(GraduationUpdateLanguageBusiReqBO reqBO) {
        LanguageAbilityPO languageAbilityPO = new LanguageAbilityPO();
        BeanUtils.copyProperties(reqBO,languageAbilityPO);
        int updateCheck = languageAbilityMapper.update(languageAbilityPO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateLanguageBusiRspBO rspBO = new GraduationUpdateLanguageBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
