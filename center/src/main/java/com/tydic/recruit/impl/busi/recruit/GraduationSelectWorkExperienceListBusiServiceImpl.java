package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectWorkExperienceListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.WorkExperienceListBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.WorkExperienceMapper;
import com.tydic.recruit.dao.recurit.po.WorkExperiencePO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectWorkExperienceListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:24
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectWorkExperienceListBusiServiceImpl implements GraduationSelectWorkExperienceListBusiService {
    @Autowired
    private WorkExperienceMapper workExperienceMapper;
    @Override
    public GraduationSelectWorkExperienceListBusiRspBO selectWorList(GraduationSelectWorkExperienceListBusiReqBO reqBO) {
        GraduationSelectWorkExperienceListBusiRspBO rspBO = new GraduationSelectWorkExperienceListBusiRspBO();
        WorkExperiencePO workExperiencePO = new WorkExperiencePO();
        workExperiencePO.setPersonId(reqBO.getPersonId());
        List<WorkExperiencePO> list = workExperienceMapper.selectList(workExperiencePO);
        List<WorkExperienceListBO> workExperienceListBOS = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("YYYY-mm-DD");
        if (!CollectionUtils.isEmpty(list)){
            for (WorkExperiencePO experiencePO : list) {
                WorkExperienceListBO workExperienceListBO = new WorkExperienceListBO();
                BeanUtils.copyProperties(experiencePO,workExperienceListBO);
                workExperienceListBO.setWorkId(experiencePO.getWorkId().toString());
                workExperienceListBO.setEntryTime(format.format(experiencePO.getEntryTime()));
                workExperienceListBO.setDepartureTime(format.format(experiencePO.getDepartureTime()));
                if (null != workExperienceListBO.getDepartureTime()){
                    workExperienceListBO.setDepartureTime(format.format(experiencePO.getDepartureTime()));
                }
                workExperienceListBOS.add(workExperienceListBO);
            }
            rspBO.setWorkExperienceListBOS(workExperienceListBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
