package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateLanguageAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateLanguageAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateLanguageBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateLanguageAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:36
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateLanguageAbilityServiceImpl implements GraduationUpdateLanguageAbilityService {
    @Autowired
    private GraduationUpdateLanguageBusiService graduationUpdateLanguageBusiService;
    @Override
    public GraduationUpdateLanguageAbilityRspBO updateLanguage(GraduationUpdateLanguageAbilityReqBO reqBO) {
        GraduationUpdateLanguageBusiReqBO busiReqBO = new GraduationUpdateLanguageBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateLanguageBusiRspBO busiRspBO = graduationUpdateLanguageBusiService.updateLanguage(busiReqBO);
        GraduationUpdateLanguageAbilityRspBO rspBO = new GraduationUpdateLanguageAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
