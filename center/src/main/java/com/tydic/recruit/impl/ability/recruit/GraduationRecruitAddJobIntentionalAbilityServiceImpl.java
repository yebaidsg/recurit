package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddJobIntentionalAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationRecruitAddJobIntentionalAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationRecruitAddJobIntentionalBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationRecruitAddJobIntentionalAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:48
 */
@HTServiceImpl
@Slf4j
public class GraduationRecruitAddJobIntentionalAbilityServiceImpl implements GraduationRecruitAddJobIntentionalAbilityService {

    @Autowired
    private GraduationRecruitAddJobIntentionalBusiService graduationRecruitAddJobIntentionalBusiService;
    @Override
    public GraduationRecruitAddJobIntentionalAbilityRspBO addJobInternational(GraduationRecruitAddJobIntentionalAbilityReqBO reqBO) {
        GraduationRecruitAddJobIntentionalBusiReqBO busiReqBO = new GraduationRecruitAddJobIntentionalBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        if (reqBO.getSecondPositionCodeList() != null && reqBO.getSecondPositionCodeList().length >0){
            busiReqBO.setFirstPositionCode(reqBO.getSecondPositionCodeList()[0]);
            busiReqBO.setSecondPositionCode(reqBO.getSecondPositionCodeList()[1]);
        }
        GraduationRecruitAddJobIntentionalBusiRspBO busiRspBO = graduationRecruitAddJobIntentionalBusiService.addJobInternational(busiReqBO);
        GraduationRecruitAddJobIntentionalAbilityRspBO rspBO = new GraduationRecruitAddJobIntentionalAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
