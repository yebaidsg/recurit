package com.tydic.recruit.impl.busi.recruit;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.recruit.GraduationAddTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.TrainingExperienceMapper;
import com.tydic.recruit.dao.recurit.po.TrainingExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationAddTrainingExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:34
 */
@Service("GraduationAddTrainingExperienceBusiService")
public class GraduationAddTrainingExperienceBusiServiceImpl implements GraduationAddTrainingExperienceBusiService {
    @Autowired
    private TrainingExperienceMapper trainingExperienceMapper;
    @Override
    public GraduationAddTrainingExperienceBusiRspBO addTraining(GraduationAddTrainingExperienceBusiReqBO reqBO) {
        TrainingExperiencePO trainingExperiencePO = new TrainingExperiencePO();
        BeanUtils.copyProperties(reqBO,trainingExperiencePO);
        if (null != reqBO.getTrainingId()){
            trainingExperienceMapper.update(trainingExperiencePO);
        } else {
            trainingExperiencePO.setTrainingId(Sequence.getInstance().nextId());
            int insertCheck = trainingExperienceMapper.insert(trainingExperiencePO);
            if (insertCheck < 1){
                throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
            }
        }
        GraduationAddTrainingExperienceBusiRspBO rspBO = new GraduationAddTrainingExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
