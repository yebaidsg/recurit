package com.tydic.recruit.impl.busi.python;

import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiRspBO;
import com.tydic.recruit.api.busi.python.DateCrawlingBusiService;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author huzb
 * @标题 DateCrawlingBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 16:58
 */
@HTServiceImpl
@Slf4j
public class DateCrawlingBusiServiceImpl implements DateCrawlingBusiService {
    private static final String FIVE = "1";
    private static final String HUIBO = "2";
    private static final String ZHONGHUA = "3";
    @Override
    public DateCrawlingBusiRspBO getCrawling(DateCrawlingBusiReqBO reqBO) {
        DateCrawlingBusiRspBO rspBO = new DateCrawlingBusiRspBO();
        String net = "";
        try{
            if (FIVE.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\test9.py";
            } else if (HUIBO.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\huibo_scrapy.py";
            } else if (ZHONGHUA.equals(reqBO.getNetState())){
                net = "E:\\毕业设计\\python\\recurit\\test7.py";
            }
        String[] args1 = new String[]{"C:\\ProgramData\\Anaconda3\\envs\\recurit\\python.exe",net,reqBO.getEnd()};
        Process pr=Runtime.getRuntime().exec(args1);
        BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream(),"GBK"));
        System.out.println(in);
        System.out.println(pr.getInputStream());
        String line = null;
        System.out.println(in.readLine());
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
        pr.waitFor();
        System.out.println("end");
    }catch (Exception e){
        e.printStackTrace();
    }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
