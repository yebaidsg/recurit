package com.tydic.recruit.impl.ability.publicModel;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectAreaAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectAreaAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectAreaAbilityRspBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectAreaBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectAreaAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:46
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectAreaAbilityServiceImpl implements GraduationSelectAreaAbilityService {
    @Autowired
    private GraduationSelectAreaBusiService graduationSelectAreaBusiService;
    @Override
    public GraduationSelectAreaAbilityRspBO selectAreaList(GraduationSelectAreaAbilityReqBO reqBO) {
        GraduationSelectAreaBusiReqBO busiReqBO = new GraduationSelectAreaBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectAreaBusiRspBO busiRspBO = graduationSelectAreaBusiService.selectAreaList(busiReqBO);
        return JSONObject.parseObject(JSONObject.toJSONString(busiRspBO, SerializerFeature.WriteMapNullValue),GraduationSelectAreaAbilityRspBO.class);
    }
}
