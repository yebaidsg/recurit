package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.tydic.recruit.api.busi.recruit.GraduationSelectProjectExperienceListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.ProjectExperienceBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.ProjectExperienceMapper;
import com.tydic.recruit.dao.recurit.po.ProjectExperiencePO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectProjectExperienceListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:46
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectProjectExperienceListBusiServiceImpl implements GraduationSelectProjectExperienceListBusiService {
    @Autowired
    private ProjectExperienceMapper projectExperienceMapper;
    @Override
    public GraduationSelectProjectExperienceListBusiRspBO selectProjectList(GraduationSelectProjectExperienceListBusiReqBO reqBO) {
        GraduationSelectProjectExperienceListBusiRspBO rspBO = new GraduationSelectProjectExperienceListBusiRspBO();
        ProjectExperiencePO projectExperiencePO = new ProjectExperiencePO();
        projectExperiencePO.setPersonId(reqBO.getPersonId());
        List<ProjectExperiencePO> projectExperiencePOS = projectExperienceMapper.selectList(projectExperiencePO);
        List<ProjectExperienceBO> list = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (!CollectionUtils.isEmpty(projectExperiencePOS)){
            for (ProjectExperiencePO experiencePO : projectExperiencePOS) {
                ProjectExperienceBO experienceBO = new ProjectExperienceBO();
                BeanUtils.copyProperties(experiencePO,experienceBO);
                experienceBO.setProjectId(experiencePO.getProjectId().toString());
                experienceBO.setProjectStartTime(simpleDateFormat.format(experiencePO.getProjectStartTime()));
                experienceBO.setProjectEndTime(simpleDateFormat.format(experiencePO.getProjectEndTime()));
                list.add(experienceBO);
            }
            rspBO.setProjectExperienceBOS(list);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
