package com.tydic.recruit.impl.busi.publicModel;

import com.ohaotian.plugin.db.Sequence;
import com.tydic.recruit.api.busi.publicModel.GraduationInsertDictionaryBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.publicmodel.RecruitDictionaryMapper;
import com.tydic.recruit.dao.publicmodel.po.RecruitDictionaryPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationInsertDictionaryBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:57
 */
@Service("GraduationInsertDictionaryBusiService")
public class GraduationInsertDictionaryBusiServiceImpl implements GraduationInsertDictionaryBusiService {
    @Autowired
    private RecruitDictionaryMapper recruitDictionaryMapper;
    private static final String EFFECTIVE = "0";
    @Override
    public GraduationInsertDictionaryBusiRspBO insertDictionary(GraduationInsertDictionaryBusiReqBO reqBO) {
        GraduationInsertDictionaryBusiRspBO rspBO = new GraduationInsertDictionaryBusiRspBO();
        RecruitDictionaryPO recruitDictionaryPO = new RecruitDictionaryPO();
        BeanUtils.copyProperties(reqBO,recruitDictionaryPO);
        recruitDictionaryPO.setStatus(EFFECTIVE);
        recruitDictionaryPO.setDicId(Sequence.getInstance().nextId());
        int insertCheck = recruitDictionaryMapper.insertDictionary(recruitDictionaryPO);
        if (0 < insertCheck){
            throw new BusinessException(RspConstantBo.FailRspCode,"插入失败");
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
