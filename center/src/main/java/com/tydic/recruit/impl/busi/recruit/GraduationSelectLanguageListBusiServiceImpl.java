package com.tydic.recruit.impl.busi.recruit;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ohaotian.plugin.base.bo.RspBaseBO;
import com.tydic.recruit.api.busi.recruit.GraduationSelectLanguageListBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageListBusiRspBO;
import com.tydic.recruit.api.busi.recruit.bo.LanguageBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.recurit.LanguageAbilityMapper;
import com.tydic.recruit.dao.recurit.po.LanguageAbilityPO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectLanguageListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:18
 */
@HTServiceImpl
@Slf4j
public class GraduationSelectLanguageListBusiServiceImpl implements GraduationSelectLanguageListBusiService {
    @Autowired
    private LanguageAbilityMapper languageAbilityMapper;
    @Override
    public GraduationSelectLanguageListBusiRspBO selectLanList(GraduationSelectLanguageListBusiReqBO reqBO) {
        GraduationSelectLanguageListBusiRspBO rspBO = new GraduationSelectLanguageListBusiRspBO();
        LanguageAbilityPO languageAbilityPO = new LanguageAbilityPO();
        languageAbilityPO.setPersonId(reqBO.getPersonId());
        List<LanguageAbilityPO> list = languageAbilityMapper.selectList(languageAbilityPO);
        List<LanguageBO> languageBOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)){
            for (LanguageAbilityPO abilityPO : list) {
                LanguageBO languageBO = new LanguageBO();
                BeanUtils.copyProperties(abilityPO,languageBO);
                languageBO.setLanguageId(abilityPO.getLanguageId().toString());
                if (null != abilityPO.getLanguages()){
                    switch (abilityPO.getLanguages()){
                        case "1":
                            languageBO.setLanguages("英语");
                            break;
                        case "2":
                            languageBO.setLanguages("汉语");
                            break;
                        case "3":
                            languageBO.setLanguages("日语");
                            break;
                        case "4":
                            languageBO.setLanguages("法语");
                            break;
                        case "5":
                            languageBO.setLanguages("德语");
                            break;
                        case "6":
                            languageBO.setLanguages("西班牙语");
                            break;
                        default:
                            languageBO.setLanguages("无");
                            break;
                    }
                }
                if (null != abilityPO.getLisSpeAbility()){
                    switch (abilityPO.getLisSpeAbility()){
                        case "1":
                            languageBO.setLisSpeAbility("一般");
                            break;
                        case "2":
                            languageBO.setLisSpeAbility("良好");
                            break;
                        case "3":
                            languageBO.setLisSpeAbility("熟练");
                            break;
                        case "4":
                            languageBO.setLisSpeAbility("精通");
                            break;
                        default:
                            languageBO.setLisSpeAbility("无");
                            break;
                    }
                }
                if(null != abilityPO.getReadWriteAbility()){
                    switch (abilityPO.getReadWriteAbility()){
                        case "1":
                            languageBO.setReadWriteAbility("一般");
                            break;
                        case "2":
                            languageBO.setReadWriteAbility("良好");
                            break;
                        case "3":
                            languageBO.setReadWriteAbility("熟练");
                            break;
                        case "4":
                            languageBO.setReadWriteAbility("精通");
                            break;
                        default:
                            languageBO.setReadWriteAbility("无");
                            break;
                    }
                }
                languageBOS.add(languageBO);
            }
            rspBO.setLanguageBOS(languageBOS);
        }
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
