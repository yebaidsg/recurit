package com.tydic.recruit.impl.busi.recruit;

import com.tydic.recruit.api.busi.recruit.GraduationUpdateTrainingExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.common.exception.BusinessException;
import com.tydic.recruit.dao.recurit.TrainingExperienceMapper;
import com.tydic.recruit.dao.recurit.po.TrainingExperiencePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @标题 GraduationUpdateTrainingExperienceBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 15:48
 */
@Service("GraduationUpdateTrainingExperienceBusiService")
public class GraduationUpdateTrainingExperienceBusiServiceImpl implements GraduationUpdateTrainingExperienceBusiService {
    @Autowired
    private TrainingExperienceMapper trainingExperienceMapper;
    @Override
    public GraduationUpdateTrainingExperienceBusiRspBO updateTraining(GraduationUpdateTrainingExperienceBusiReqBO reqBO) {
        TrainingExperiencePO trainingExperiencePO = new TrainingExperiencePO();
        BeanUtils.copyProperties(reqBO,trainingExperiencePO);
        int updateCheck = trainingExperienceMapper.update(trainingExperiencePO);
        if (updateCheck < 1){
            throw new BusinessException(RspConstantBo.FailRspCode,"修改失败");
        }
        GraduationUpdateTrainingExperienceBusiRspBO rspBO = new GraduationUpdateTrainingExperienceBusiRspBO();
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
