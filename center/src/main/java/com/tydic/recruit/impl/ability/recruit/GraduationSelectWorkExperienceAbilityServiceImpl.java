package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectWorkExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationSelectWorkExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationSelectWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationSelectWorkExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:10
 */
@HTServiceImpl
public class GraduationSelectWorkExperienceAbilityServiceImpl implements GraduationSelectWorkExperienceAbilityService {
    @Autowired
    private GraduationSelectWorkExperienceBusiService graduationSelectWorkExperienceBusiService;
    @Override
    public GraduationSelectWorkExperienceAbilityRspBO select(GraduationSelectWorkExperienceAbilityReqBO reqBO) {
        GraduationSelectWorkExperienceBusiReqBO busiReqBO = new GraduationSelectWorkExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationSelectWorkExperienceBusiRspBO busiRspBO = graduationSelectWorkExperienceBusiService.select(busiReqBO);
        GraduationSelectWorkExperienceAbilityRspBO rspBO = new GraduationSelectWorkExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
