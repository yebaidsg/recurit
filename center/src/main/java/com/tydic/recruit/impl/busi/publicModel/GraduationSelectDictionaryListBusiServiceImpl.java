package com.tydic.recruit.impl.busi.publicModel;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryBO;
import com.tydic.recruit.api.busi.publicModel.GraduationSelectDictionaryListBusiService;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiRspBO;
import com.tydic.recruit.common.base.bo.RspConstantBo;
import com.tydic.recruit.dao.publicmodel.RecruitDictionaryMapper;
import com.tydic.recruit.dao.publicmodel.po.RecruitDictionaryPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @标题 GraduationSelectDictionaryListBusiServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:35
 */
@Service("GraduationSelectDictionaryListBusiService")
public class GraduationSelectDictionaryListBusiServiceImpl implements GraduationSelectDictionaryListBusiService {
    @Autowired
    private RecruitDictionaryMapper recruitDictionaryMapper;
    private static final String EFFECTIVE = "0";
    @Override
    public GraduationSelectDictionaryListBusiRspBO selectDictionaryList(GraduationSelectDictionaryListBusiReqBO reqBO) {
        GraduationSelectDictionaryListBusiRspBO rspBO = new GraduationSelectDictionaryListBusiRspBO();
        RecruitDictionaryPO recruitDictionaryPO = new RecruitDictionaryPO();
        BeanUtils.copyProperties(reqBO,recruitDictionaryPO);
        Page<RecruitDictionaryPO> page = new Page<>(reqBO.getPageNo(),reqBO.getPageSize());
        recruitDictionaryPO.setStatus(EFFECTIVE);
        List<RecruitDictionaryPO> lists = recruitDictionaryMapper.selectList(recruitDictionaryPO,page);
        if (!CollectionUtils.isEmpty(lists)){
            List<GraduationSelectDictionaryBO> dictionaryBOS = new ArrayList<>();
            for (RecruitDictionaryPO list : lists) {
                GraduationSelectDictionaryBO graduationSelectDictionaryBO = new GraduationSelectDictionaryBO();
                BeanUtils.copyProperties(list,graduationSelectDictionaryBO);
                dictionaryBOS.add(graduationSelectDictionaryBO);
            }
            if (!CollectionUtils.isEmpty(dictionaryBOS)){
                rspBO.setRows(dictionaryBOS);
            }
        }
        rspBO.setPageNo(page.getPageNo());
        rspBO.setTotal(page.getTotalPages());
        rspBO.setRecordsTotal(page.getTotalCount());
        rspBO.setCode(RspConstantBo.SuccRspCode);
        rspBO.setMessage(RspConstantBo.SuccRspDesc);
        return rspBO;
    }
}
