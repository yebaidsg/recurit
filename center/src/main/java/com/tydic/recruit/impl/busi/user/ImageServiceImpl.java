package com.tydic.recruit.impl.busi.user;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ozo.ability.ImageService;
import com.ozo.ability.bo.ImageBO;
import com.tydic.recruit.dao.po.Image;
import com.tydic.recruit.dao.user.ImageMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageMapper imageMapper;

    @Override
    public List<ImageBO> findAllImage() {
        return JSONObject.parseArray(JSONObject.toJSONString(imageMapper.findAllImage(), SerializerFeature.WriteMapNullValue),ImageBO.class);
    }

    @Override
    public List<ImageBO> findImageByCondition(ImageBO image) {
        Image image1 = new Image();
        BeanUtils.copyProperties(image,image1);
        return JSONObject.parseArray(JSONObject.toJSONString(imageMapper.findImageByCondition(image1), SerializerFeature.WriteMapNullValue),ImageBO.class);
    }

    @Override
    public ImageBO findImageByconnectId(Long connectId) {
        return JSONObject.parseObject(JSONObject.toJSONString(imageMapper.findImageByconnectId(connectId), SerializerFeature.WriteMapNullValue),ImageBO.class);

    }

    @Override
    public ImageBO findImageById(Long imageId) {
        return JSONObject.parseObject(JSONObject.toJSONString(imageMapper.findImageById(imageId), SerializerFeature.WriteMapNullValue),ImageBO.class);
    }

    @Override
    public ImageBO findImageByName(String imageName) {
        return JSONObject.parseObject(JSONObject.toJSONString(imageMapper.findImageByName(imageName), SerializerFeature.WriteMapNullValue),ImageBO.class);
    }

    @Override
    public void insertImage(ImageBO image) {
        Image image1 = new Image();
        BeanUtils.copyProperties(image,image1);
        imageMapper.insertImage(image1);
    }

    @Override
    public void deleteImage(Long id) {
        imageMapper.deleteImage(id);
    }

    @Override
    public void updateImage(ImageBO image) {
        Image image1 = new Image();
        BeanUtils.copyProperties(image,image1);
        imageMapper.updateImage(image1);
    }

    @Override
    public void updateImageByConnectId(ImageBO image) {
        Image image1 = new Image();
        BeanUtils.copyProperties(image,image1);
        imageMapper.updateImageByConnectId(image1);
    }


}
