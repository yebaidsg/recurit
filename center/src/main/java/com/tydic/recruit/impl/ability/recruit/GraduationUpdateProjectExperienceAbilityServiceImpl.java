package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProjectExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProjectExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateProjectExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateProjectExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateProjectExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:19
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateProjectExperienceAbilityServiceImpl implements GraduationUpdateProjectExperienceAbilityService {
    @Autowired
    private GraduationUpdateProjectExperienceBusiService graduationUpdateProjectExperienceBusiService;
    @Override
    public GraduationUpdateProjectExperienceAbilityRspBO updateProjectExperience(GraduationUpdateProjectExperienceAbilityReqBO reqBO) {
        GraduationUpdateProjectExperienceBusiReqBO busiReqBO = new GraduationUpdateProjectExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateProjectExperienceBusiRspBO busiRspBO = graduationUpdateProjectExperienceBusiService.updateProject(busiReqBO);
        GraduationUpdateProjectExperienceAbilityRspBO rspBO = new GraduationUpdateProjectExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
