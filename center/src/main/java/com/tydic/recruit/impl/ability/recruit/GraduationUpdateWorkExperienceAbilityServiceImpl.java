package com.tydic.recruit.impl.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateWorkExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.GraduationUpdateWorkExperienceAbilityService;
import com.tydic.recruit.api.busi.recruit.GraduationUpdateWorkExperienceBusiService;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @标题 GraduationUpdateWorkExperienceAbilityServiceImpl
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:04
 */
@HTServiceImpl
@Slf4j
public class GraduationUpdateWorkExperienceAbilityServiceImpl implements GraduationUpdateWorkExperienceAbilityService {
    @Autowired
    private GraduationUpdateWorkExperienceBusiService graduationUpdateWorkExperienceBusiService;
    @Override
    public GraduationUpdateWorkExperienceAbilityRspBO updateWork(GraduationUpdateWorkExperienceAbilityReqBO reqBO) {
        GraduationUpdateWorkExperienceBusiReqBO busiReqBO = new GraduationUpdateWorkExperienceBusiReqBO();
        BeanUtils.copyProperties(reqBO,busiReqBO);
        GraduationUpdateWorkExperienceBusiRspBO busiRspBO = graduationUpdateWorkExperienceBusiService.updateWork(busiReqBO);
        GraduationUpdateWorkExperienceAbilityRspBO rspBO = new GraduationUpdateWorkExperienceAbilityRspBO();
        BeanUtils.copyProperties(busiRspBO,rspBO);
        return rspBO;
    }
}
