package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

/**
 * @author huzb
 * @标题 EvaluatePositionPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/19 14:49
 */
@Data
public class EvaluatePositionPO {
    private Long evaluateId;
    private int evaluateStart;
    private Long userId;
    private String userName;
    private Integer positionId;
    private String jobTitle;
}
