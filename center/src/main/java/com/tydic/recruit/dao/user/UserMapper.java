package com.tydic.recruit.dao.user;

import com.ozo.ability.bo.UserBO;
import com.tydic.recruit.dao.po.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public
interface UserMapper {

    List<User> findAllUser();

    User findLoginUser(User user);

    User findUserByName(@Param("username") String username);

    List<User> findUserByCondition(User user);

    List<UserBO> findUserByUsername(@Param("username") String username);

    User findUserById(@Param("userId") Long userId);

    void insertUser(User user);

    void insertUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    void deleteUser(Long id);

    void updateUser(User user);

}
