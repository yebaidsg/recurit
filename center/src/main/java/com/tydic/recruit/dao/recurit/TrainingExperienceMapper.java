package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.TrainingExperiencePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 TrainingExperienceMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:35
 */
@Repository
public interface TrainingExperienceMapper {
    int insert(TrainingExperiencePO trainingExperiencePO);
    int update(TrainingExperiencePO trainingExperiencePO);
    TrainingExperiencePO select(TrainingExperiencePO trainingExperiencePO);
    List<TrainingExperiencePO> selectList(TrainingExperiencePO trainingExperiencePO);
    int delete(@Param("id") Long id);

    void deleteByPersonId(Long personId);
}
