package com.tydic.recruit.dao.user;

import com.tydic.recruit.dao.po.Perms;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermsMapper {

    List<Perms> findPermsByRoleId(Long id);

    List<Perms> findAllPerms();
}
