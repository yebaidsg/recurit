package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

/**
 * @author huzb
 * @标题 EvaluateCommedPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:27
 */
@Data
public class EvaluateCommendPO {
    private Long userId;
    private String evaluateStart;
    private Integer positionId;
}
