package com.tydic.recruit.dao.publicmodel.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 AreaPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:04
 */
@Data
public class AreaPO implements Serializable {
    private String areaCode;
    private String areaName;
    private String cityCode;
}
