package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.ProjectExperiencePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 ProjectExperienceMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:08
 */
@Repository
public interface ProjectExperienceMapper {
    int insert(ProjectExperiencePO projectExperiencePO);
    int update(ProjectExperiencePO projectExperiencePO);
    ProjectExperiencePO select(ProjectExperiencePO projectExperiencePO);
    List<ProjectExperiencePO> selectList(ProjectExperiencePO projectExperiencePO);
    int delete(@Param("id") Long id);

    void deleteByPersonId(Long personId);
}
