package com.tydic.recruit.dao.publicmodel.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 TownPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:12
 */
@Data
public class TownPO implements Serializable {
    private String townCode;
    private String townName;
    private String areaCode;
}
