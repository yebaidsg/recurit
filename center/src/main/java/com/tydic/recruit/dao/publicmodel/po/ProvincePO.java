package com.tydic.recruit.dao.publicmodel.po;

import lombok.Data;

/**
 * @标题 ProvincePO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:54
 */
@Data
public class ProvincePO {
    private String provinceCode;
    private String provinceName;
}
