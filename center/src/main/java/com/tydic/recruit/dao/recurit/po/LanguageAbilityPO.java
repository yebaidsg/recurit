package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 LanguageAbilityPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:20
 */
@Data
public class LanguageAbilityPO implements Serializable {
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
    private Long languageId;
}
