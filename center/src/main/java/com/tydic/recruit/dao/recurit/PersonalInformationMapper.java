package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.PersonalInformationPO;
import com.tydic.recruit.dao.recurit.po.StatisPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 PersonalInformationMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 22:48
 */
@Repository
public interface PersonalInformationMapper {
    /**
     * 新增个人信息
     */
    int insert(PersonalInformationPO personalInformationPO);

    int update(PersonalInformationPO personalInformationPO);

    PersonalInformationPO select(PersonalInformationPO personalInformationPO);

    List<PersonalInformationPO> findByPersonName(@Param("personName") String personName);

    List<StatisPO> findStatis1();

    List<StatisPO> findStatis2();

    void deletePersonInfo(Long personId);
}
