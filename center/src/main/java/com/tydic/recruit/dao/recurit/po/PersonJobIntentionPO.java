package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.util.Date;

/**
 * @标题 PersonJobIntentionPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:57
 */
@Data
public class PersonJobIntentionPO {
    private Long intentionId;;
    private Long userId;
    /**
     * 简历id
     */
    private Long personId;
    private String personName;
    /**
     * 一级职位编码
     */
    private String firstPositionCode;
    /**
     * 二级职位编码
     */
    private String secondPositionCode;
    /**
     * 期望城市编码
     */
    private String expectProvinceCode;
    /**
     * 期望城市名称
     */
    private String expectProvinceName;
    /**
     * 期望城市
     */
    private String expectCityCode;
    private String expectCityName;
    /**
     * 期望区县
     */
    private String expectAreaCode;
    private String expectAreaName;
    /**
     * 城镇
     */
    private String expectTownCode;
    private String expectTownName;
    /**
     * 薪资要求
     */
    private String salaryRequirements;
    /**
     * 工作性质
     */
    private String natureOfWork;
    private Long createId;
    private Date createTime;
    private Long updateId;
    private Date updateTime;
}
