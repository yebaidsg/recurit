package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.WorkExperiencePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 WorkExperienceMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 14:36
 */
@Repository
public interface WorkExperienceMapper {
    int insert(WorkExperiencePO workExperiencePO);
    int update(WorkExperiencePO workExperiencePO);
    WorkExperiencePO select(WorkExperiencePO workExperiencePO);
    List<WorkExperiencePO> selectList(WorkExperiencePO workExperiencePO);
    int deleteByWorkId(@Param("id") Long id);

    void deleteByPersonId(Long personId);
}
