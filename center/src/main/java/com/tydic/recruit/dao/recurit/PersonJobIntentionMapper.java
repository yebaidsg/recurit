package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.PersonJobIntentionPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 PersonJobIntentionMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:58
 */
@Repository
public interface PersonJobIntentionMapper {
    int insert(PersonJobIntentionPO personJobIntentionPO);
    int update(PersonJobIntentionPO personJobIntentionPO);
    PersonJobIntentionPO select(PersonJobIntentionPO personJobIntentionPO);
    List<PersonJobIntentionPO> selectList(PersonJobIntentionPO personJobIntentionPO);
    int delectById(@Param("id") Long id);

    List<PersonJobIntentionPO> findByPersonName(@Param("personName") String personName);
    void deleteByPersonId(Long personId);
}
