package com.tydic.recruit.dao.recurit;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.dao.recurit.po.DeliverPositionPO;
import com.tydic.recruit.dao.recurit.po.EvaluateCommendPO;
import com.tydic.recruit.dao.recurit.po.PositionPO;
import com.tydic.recruit.utils.bean.CsvBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author huzb
 * @标题 PositionMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 18:59
 */
@Repository
public interface PositionMapper {
    int insert(@Param("csvBeans") List<CsvBean> csvBeans);
    /**
     * 列表查询
     */
    List<PositionPO> selectList(PositionPO positionPO, Page page);
    PositionPO selectDetail(PositionPO positionPO);
    int updatePositionType(PositionPO positionPO);
    int selectCount(PositionPO positionPO);
    /**
     * 查看评分最多最高职位
     */
    List<PositionPO> selectPositionList(PositionPO positionPO);
    /**
     * 根据薪资要求匹配
     */
    List<PositionPO> selectSalaryList(PositionPO positionPO);

    /**
     * 根据userId查看已评价列表
     */
    List<PositionPO> selectEvaluateList(PositionPO positionPO,Page<PositionPO> page);
    /**
     * 查询所有评分记录
     */
    List<EvaluateCommendPO> selectAll(PositionPO positionPO);
    /**
     * 新增投递简历
     */
    int insertDeliver(DeliverPositionPO deliverPositionPO);

    /**
     * 删除投递
     */
    int deleteDeliver(@Param("deliverId") Integer deliverId);

    /**
     * 列表查询投递
     */
    List<PositionPO> selectDeliverList(PositionPO positionPO);

    /**
     * 详情查询
     */
    int deliverCount(DeliverPositionPO positionPO);

    int selectCountDeliver(@Param("userId") Long userId);

    List<PositionPO> findByJobTitle(@Param("jobTitle") String jobTitle,@Param("positionType") String positionType);
    void deletePosition(Integer positionId);

    /**
     * 根据职位推荐
     */
    List<PositionPO> selectPositonCommend(PositionPO positionPO);
}
