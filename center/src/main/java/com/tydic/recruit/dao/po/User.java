package com.tydic.recruit.dao.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {
    private Long userId;
    private String username;
    private String password;
    private String userPhone;
    private String userStatus;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date userEnrollTime;

}
