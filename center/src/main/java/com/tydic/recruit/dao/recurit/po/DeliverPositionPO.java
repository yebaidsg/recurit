package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.util.Date;

/**
 * @author huzb
 * @标题 DeliverPositionPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 23:34
 */
@Data
public class DeliverPositionPO {
    private int deliverId;
    private Integer positionId;
    private Long userId;
    private Date deliverTime;
    private String deliverStatus;
}
