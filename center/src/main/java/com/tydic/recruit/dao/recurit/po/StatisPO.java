package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

@Data
public class StatisPO {
    private Integer month;
    private Integer count;
}
