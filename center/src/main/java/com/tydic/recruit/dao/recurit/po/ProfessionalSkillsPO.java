package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 ProfessionalSkillsPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:38
 */
@Data
public class ProfessionalSkillsPO implements Serializable {
    private Long skillsId;
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
}
