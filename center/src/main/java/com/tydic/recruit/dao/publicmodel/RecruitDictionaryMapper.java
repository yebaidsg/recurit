package com.tydic.recruit.dao.publicmodel;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.dao.publicmodel.po.RecruitDictionaryPO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 RecruitDectionaryMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:21
 */
@Repository
public interface RecruitDictionaryMapper {
    /**
     * 字典查询
     */
    List<RecruitDictionaryPO> selectList(RecruitDictionaryPO recruitDictionaryPO, Page<RecruitDictionaryPO> page);
    /**
     * 新增
     */
    int insertDictionary(RecruitDictionaryPO recruitDictionaryPO);
    /**
     * 修改
     */
    int updateDictionary(RecruitDictionaryPO recruitDictionaryPO);
}
