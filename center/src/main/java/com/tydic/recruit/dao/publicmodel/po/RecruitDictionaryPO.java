package com.tydic.recruit.dao.publicmodel.po;

import lombok.Data;

/**
 * @标题 recruitDictionaryPO
 * @说明 字典PP
 * @作者 胡中宝
 * @时间 2021/4/4 15:17
 */
@Data
public class RecruitDictionaryPO {
    private Long dicId;
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
