package com.tydic.recruit.dao.publicmodel.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 CityPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:59
 */
@Data
public class CityPO implements Serializable {
    private String cityCode;
    private String cityName;
    private String provinceCode;
}
