package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author huzb
 * @标题 PositionPO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:35
 */
@Data
public class PositionPO implements Serializable {
    private Long positionId;
    private String jobTitle;
    private String companyName;
    private String workPlace;
    private String companyType;
    private String experience;
    private String quantity;
    private String education;
    private String lowSalary;
    private String highSalary;
    private String jobInfo;
    private Integer evaluateStart;
    private Long userId;
    private Integer deliverId;
    private String deliverStatus;
    private List<Integer> positionIds;
    private String positionType;
    private List<String> positionIntentions;
    private List<String> intentionArea;
    private String positionResource;
}
