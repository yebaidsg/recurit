package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.util.Date;

/**
 * @标题 EducationalExperiencePO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:44
 */
@Data
public class EducationalExperiencePO {
    private Long eduId;
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
}
