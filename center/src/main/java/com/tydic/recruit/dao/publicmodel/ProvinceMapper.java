package com.tydic.recruit.dao.publicmodel;

import com.ohaotian.plugin.db.Page;
import com.tydic.recruit.dao.publicmodel.po.AreaPO;
import com.tydic.recruit.dao.publicmodel.po.CityPO;
import com.tydic.recruit.dao.publicmodel.po.ProvincePO;
import com.tydic.recruit.dao.publicmodel.po.TownPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 ProvinceMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:54
 */
@Repository
public interface ProvinceMapper {
    /**
     * 省查询
     */
    List<ProvincePO> selectProvinceList(ProvincePO provincePO, Page<ProvincePO> page);
    /**
     * 根据省code查询市
     */
    List<CityPO> selectCityList(CityPO cityPO,Page<CityPO> page);

    /**
     * 根据市查区县
     */
    List<AreaPO> selectAreaList(AreaPO areaPO,Page<AreaPO> page);

    /**
     * 根据区县查诚征
     */
    List<TownPO> selectTownList(TownPO townPO,Page<TownPO> page);
}
