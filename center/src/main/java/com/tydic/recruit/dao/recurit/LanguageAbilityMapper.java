package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.LanguageAbilityPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 LanguageAbilityMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:21
 */
@Repository
public interface LanguageAbilityMapper {
    int insert(LanguageAbilityPO languageAbilityPO);
    int update(LanguageAbilityPO languageAbilityPO);
    LanguageAbilityPO select(LanguageAbilityPO languageAbilityPO);
    List<LanguageAbilityPO> selectList(LanguageAbilityPO languageAbilityPO);
    int delete(@Param("id") Long id);

    void deleteByPersonId(Long personId);
}
