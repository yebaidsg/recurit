package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @标题 ProjectExperiencePO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:07
 */
@Data
public class ProjectExperiencePO implements Serializable {
    private Long projectId;
    private Long personId;
    private String projectName;
    private Date projectStartTime;
    private Date projectEndTime;
    private String projectDescribe;
}
