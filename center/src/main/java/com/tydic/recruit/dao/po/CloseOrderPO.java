package com.tydic.recruit.dao.po;

import lombok.Data;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/22 14:12
 */
@Data
public class CloseOrderPO {
    private Integer nid;
    private Integer sid;
}
