package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.EducationalExperiencePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 EducationalExperienceMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:45
 */
@Repository
public interface EducationalExperienceMapper {
    int insert(EducationalExperiencePO educationalExperiencePO);
    int update(EducationalExperiencePO educationalExperiencePO);
    EducationalExperiencePO select(EducationalExperiencePO educationalExperiencePO);
    List<EducationalExperiencePO> selectList(EducationalExperiencePO educationalExperiencePO);
    int deleteById(@Param("id") Long id);

    List<EducationalExperiencePO> findEduByUserId(@Param("userId") Long userId);
    void deleteByPersonId(Long personId);
}
