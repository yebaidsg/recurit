package com.tydic.recruit.dao.po;

public class Image {

    private Long imageId;
    private String imageName;
    private Long connectId;
    private String imageUrl;

    public Image(Long imageId, String imageName, Long connectId, String imageUrl) {
        this.imageId = imageId;
        this.imageName = imageName;
        this.connectId = connectId;
        this.imageUrl = imageUrl;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Long getConnectId() {
        return connectId;
    }

    public void setConnectId(Long connectId) {
        this.connectId = connectId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Image() {
    }
}
