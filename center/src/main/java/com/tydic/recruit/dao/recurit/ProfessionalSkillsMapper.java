package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.ProfessionalSkillsPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 ProfessionalSkillsMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:39
 */
@Repository
public interface ProfessionalSkillsMapper {
    int insert(ProfessionalSkillsPO professionalSkillsPO);
    int update(ProfessionalSkillsPO professionalSkillsPO);
    ProfessionalSkillsPO select(ProfessionalSkillsPO professionalSkillsPO);
    List<ProfessionalSkillsPO> selectList(ProfessionalSkillsPO professionalSkillsPO);
    int delete(@Param("id") Long id);

    List<ProfessionalSkillsPO> findSkillByUserId(@Param("userId") Long userId);
    void deleteByPersonId(Long personId);
}
