package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @标题 TrainingExperiencePO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:35
 */
@Data
public class TrainingExperiencePO implements Serializable {
    private Long personId;
    private String trainingName;
    private Date trainingStartTime;
    private Date trainingEndTime;
    private String trainingClass;
    private Long trainingId;
}
