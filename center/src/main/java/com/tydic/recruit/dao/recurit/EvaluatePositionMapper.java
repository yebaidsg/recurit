package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.EvaluatePositionPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author huzb
 * @标题 EvaluatePositionMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/19 14:50
 */
@Repository
public interface EvaluatePositionMapper {
    int insert(EvaluatePositionPO evaluatePositionPO);
    int update(EvaluatePositionPO evaluatePositionPO);
    EvaluatePositionPO select(EvaluatePositionPO evaluatePositionPO);
    int countEvaluate(@Param("userId") Long userId);

    List<EvaluatePositionPO> findEvaluateByCondition(@Param("userName") String userName, @Param("jobTitle") String jobTitle);
    void deleteEvaluate(Long evaluateId);
}
