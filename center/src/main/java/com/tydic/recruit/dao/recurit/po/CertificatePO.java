package com.tydic.recruit.dao.recurit.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @标题 CertificatePO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:23
 */
@Data
public class CertificatePO implements Serializable {
    private Long certificateId;
    private Long personId;
    private String certificateName;
    private Date certificateGetTime;
}
