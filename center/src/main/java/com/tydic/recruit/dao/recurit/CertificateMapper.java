package com.tydic.recruit.dao.recurit;

import com.tydic.recruit.dao.recurit.po.CertificatePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @标题 CertificateMapper
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:24
 */
@Repository
public interface CertificateMapper {
    int insert(CertificatePO certificatePO);
    int update(CertificatePO certificatePO);
    CertificatePO select(CertificatePO certificatePO);
    List<CertificatePO> selectList(CertificatePO certificatePO);
    int delete(@Param("id") Long id);

    void deleteByPersonId(Long personId);
}
