# 新功能发布记录

# 云服务器 ECS

 [查看产品>](https://www.aliyun.com/product/ecs)

云服务器ECS（Elastic Compute Service）是一种简单高效、处理能力可弹性伸缩的计算服务。帮助您构建更稳定、安全的应用，提升运维效率，降低IT成本，使您更专注于核心业务创新。