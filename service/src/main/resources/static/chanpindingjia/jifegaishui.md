# 计费概述

更新时间：2020-04-28 17:23:22

本文介绍云服务器ECS的计费资源、资源的计费方式和购买资源时的支付方式。

同一ECS资源的价格在不同地域中可能存在差别。ECS资源的价格信息，请参见[云产品定价页](https://www.aliyun.com/price/product)。

## 计费资源

一台云服务器ECS包括ECS实例、镜像、块存储等资源。更多信息，请参见[云服务器ECS产品架构](https://help.aliyun.com/document_detail/25367.html#section-vwb-qk2-ghb)。

涉及计费的ECS资源如下：

| 资源类型 | 说明                                                         |
| :------- | :----------------------------------------------------------- |
| ECS实例  | 以实例规格的形式提供 ，包括vCPU和内存，收取实例规格费用。**说明** 如果是带本地盘的实例规格，则该价格已包含本地盘价格。 |
| 镜像     | 镜像包括四种类型：公共镜像Windows Server：计费与实例规格大小有关，以售卖页显示为准。Red Hat Enterprise Linux：收取镜像费用，以售卖页显示为准。其他：免费。镜像市场的镜像，以镜像供应商提供的信息为准。自定义镜像来源于免费公共镜像/镜像市场镜像：收取快照容量费用。来源于付费公共镜像/镜像市场镜像：收取快照容量费用。如果创建ECS实例时使用了自定义镜像，还会收取镜像费用。共享镜像来源于免费公共镜像/镜像市场镜像：免费。来源于付费公共镜像/镜像市场镜像：如果创建ECS实例时使用了该共享镜像，则收取费用。若从未使用，则不收费。 |
| 块存储   | 块存储包括三种类型：云盘：收取云盘容量费用，可以用作系统盘和数据盘。本地盘：收取本地盘容量费用，只能用作数据盘。此处本地盘指与特定实例规格绑定的本地盘，不支持单独购买，且费用计入实例规格。配备本地盘的实例规格族包括但不限于：d1ne、d1、i2、i2g、i1、gn5和ga1，更多实例规格族信息请参见[实例规格族](https://help.aliyun.com/document_detail/25378.html#concept-sx4-lxv-tdb)。 |
| 公网带宽 | ECS实例可以通过以下方式访问公网：通过固定公网IP访问：不收取固定公网IP保有费用，仅收取公网出网带宽费用。详情请参见[公网带宽计费方式](https://help.aliyun.com/document_detail/25411.html#publicIP-china) 。通过弹性公网IP（EIP）访问：EIP为独立产品，详情请参见[EIP计费概述](https://help.aliyun.com/document_detail/122035.html#concept-645525)。通过NAT网关访问：NAT网关为独立产品，详情请参见[NAT网关计费说明](https://help.aliyun.com/document_detail/48126.html#concept-z13-hty-ydb)。 |
| 快照     | 收取快照容量费用。                                           |

## 计费方式

ECS资源的主要计费方式为包年包月和按量付费。

- 包年包月：一种预付费模式，即先付费再使用。一般适用于固定的7*24服务，例如Web服务。更多信息，请参见[包年包月](https://help.aliyun.com/document_detail/56220.html#subs-china)。
- 按量付费：一种后付费模式，即先使用再付费。一般适用于有爆发业务量的应用或服务，例如临时扩展、临时测试、科学计算。更多信息，请参见[按量付费](https://help.aliyun.com/document_detail/40653.html#Pay-As-You-Go)。

包年包月和按量付费的区别，请参见[计费方式对比](https://help.aliyun.com/document_detail/25370.html#billingMethod-china)。

| 资源类型 | 支持的计费方式                                               |
| :------- | :----------------------------------------------------------- |
| ECS实例  | 包年包月按量付费如果您需要长时间使用按量付费ECS实例，可以搭配预留实例券抵扣ECS实例的账单，兼顾灵活性和成本。更多信息，请参见[预留实例券概述](https://help.aliyun.com/document_detail/100370.html#concept-tc4-zhq-dgb)。预留实例券单独计费，详情请参见[预留实例券计费方式](https://help.aliyun.com/document_detail/100371.html#concept-t2m-n4q-dgb)。抢占式实例抢占式实例可以节省成本，但是存在回收机制，您可以使用弹性供应组缓解回收机制带来的不稳定因素。更多信息，请参见[弹性供应概述](https://help.aliyun.com/document_detail/120020.html#concept-287169)。 |
| 镜像     | 包年包月按量付费镜像只能和ECS实例搭配使用，Windows类型的预留实例券可以抵扣镜像的账单。 |
| 云盘     | 包年包月按量付费通过不同方式创建云盘时，支持的计费方式不同：随ECS实例创建云盘，计费方式和ECS实例相同。为已有包年包月ECS实例创建包年包月云盘，计费方式为包年包月。在云盘页面创建云盘，计费方式仅支持按量付费。使用快照创建云盘，计费方式仅支持按量付费。云盘支持转换计费方式，方便您灵活管理云盘。更多信息，请参见[转换云盘计费方式](https://help.aliyun.com/document_detail/58666.html#concept-zpt-5g3-ydb)。 |
| 公网带宽 | 通过固定公网IP访问公网时，支持以下方式：按固定带宽按照您指定的带宽收费。根据ECS实例计费方式不同，又可以分为：按固定带宽（包年包月）按固定带宽（按量付费）按使用流量按照您实际使用的流量收费，每小时整点结算。您可以在创建ECS实例时开启公网带宽，也可以在创建ECS实例后通过升降配功能开启公网带宽。更多信息，请参见[公网带宽计费方式](https://help.aliyun.com/document_detail/25411.html#publicIP-china)。 |
| 快照     | 预付费存储包按量付费更多计费说明，请参见[快照计费方式](https://help.aliyun.com/document_detail/56159.html#concept-rq2-pcx-ydb)。 |

## 支付方式

购买ECS资源时，您可以选择以下支付方式：

- 阿里云账户余额

- 在线支付

  通过API [CreateInstance](https://help.aliyun.com/document_detail/25499.html#CreateInstance)创建包年包月ECS实例时，不能使用信用卡支付。

- 代金券

  请确保代金券的状态为**可用**且订单类型为**通用**。 您可以登录[用户中心](https://expense.console.aliyun.com/)，选择***\*全部菜单项\** > \**卡券管理\** > \**代金券管理\****查看代金券。

**说明** 优惠券用于在出账前抵扣消费金额，因此并不涉及实际支付动作。