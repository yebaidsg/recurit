# 什么是云服务器ECS

更新时间：2020-04-20 11:42:44

云服务器（Elastic Compute Service，简称ECS）是阿里云提供的性能卓越、稳定可靠、弹性扩展的IaaS（Infrastructure as a Service）级别云计算服务。云服务器ECS免去了您采购IT硬件的前期准备，让您像使用水、电、天然气等公共资源一样便捷、高效地使用服务器，实现计算资源的即开即用和弹性伸缩。阿里云ECS持续提供创新型服务器，解决多种业务需求，助力您的业务发展。



## 为什么选择云服务器ECS

选择云服务器ECS，您可以轻松构建具有以下优势的计算资源：

- 无需自建机房，无需采购以及配置硬件设施。
- 分钟级交付，快速部署，缩短应用上线周期。
- 快速接入部署在全球范围内的数据中心和BGP机房。
- 成本透明，按需使用，支持根据业务波动随时扩展和释放资源。
- 提供GPU和FPGA等异构计算服务器、弹性裸金属服务器以及通用的x86架构服务器。
- 支持通过内网访问其他阿里云服务，形成丰富的行业解决方案，降低公网流量成本。
- 提供虚拟防火墙、角色权限控制、内网隔离、防病毒攻击及流量监控等多重安全方案。
- 提供性能监控框架和主动运维体系。
- 提供行业通用标准API，提高易用性和适用性。

更多选择理由，请参见[云服务器ECS的优势](https://help.aliyun.com/document_detail/51704.html#concept-c1y-bp2-vdb)和[应用场景](https://help.aliyun.com/document_detail/25371.html#concept-j5s-nwq-ydb)。

## 产品架构

云服务器ECS主要包含以下功能组件：

- [实例](https://help.aliyun.com/document_detail/25374.html#concept-i1k-fv2-5db)：等同于一台虚拟服务器，内含CPU、内存、操作系统、网络配置、磁盘等基础的计算组件。实例的计算性能、内存性能和适用业务场景由实例规格决定，其具体性能指标包括实例vCPU核数、内存大小、网络性能等。

- [镜像](https://help.aliyun.com/document_detail/25389.html#concept-qql-3zb-wdb)：提供实例的操作系统、初始化应用数据及预装的软件。操作系统支持多种Linux发行版和多种Windows Server版本。

- [块存储](https://help.aliyun.com/document_detail/63136.html#concept-pl4-tzb-wdb)：块设备类型产品，具备高性能和低时延的特性。提供基于分布式存储架构的云盘以及基于物理机本地存储的本地盘。

- [快照](https://help.aliyun.com/document_detail/25391.html#concept-qft-2zw-ydb)：某一时间点一块云盘的数据状态文件。常用于数据备份、数据恢复和制作自定义镜像等。

- [安全组](https://help.aliyun.com/document_detail/25387.html#concept-o2y-mqw-ydb)：由同一地域内具有相同保护需求并相互信任的实例组成，是一种虚拟防火墙，用于设置实例的网络访问控制。

- 网络

  ：

  - [专有网络（Virtual Private Cloud）](https://help.aliyun.com/document_detail/34217.html#concept-kbk-cpz-ndb)：逻辑上彻底隔离的云上私有网络。您可以自行分配私网IP地址范围、配置路由表和网关等。
  - 经典网络：所有经典网络类型实例都建立在一个共用的基础网络上。由阿里云统一规划和管理网络配置。

更多功能组件详情，请参见[云服务器ECS产品详情页](https://www.aliyun.com/product/ecs)。

以下为云服务器ECS的产品组件架构图，图中涉及的功能组件的详细介绍请参见相应的帮助文档。[![WhatIsEcs-Orange-Renminbi](https://static-aliyun-doc.oss-cn-hangzhou.aliyuncs.com/assets/img/zh-CN/7514537851/p85840.png)](https://static-aliyun-doc.oss-cn-hangzhou.aliyuncs.com/assets/img/zh-CN/7514537851/p85840.png)

## 产品定价

云服务器ECS支持包年包月、按量付费、预留实例券、抢占式实例等多种账单计算模式。更多详情，请参见[计费概述](https://help.aliyun.com/document_detail/25398.html#concept-isb-scd-5db)和[云产品定价页](https://www.aliyun.com/price/product#/ecs/detail)。

## 管理工具

通过注册阿里云账号，您可以在任何地域下，通过阿里云提供的以下途径创建、使用或者释放云服务器ECS：

- ECS管理控制台：具有交互式操作的Web服务页面。关于管理控制台的操作，请参见[常用操作导航](https://help.aliyun.com/document_detail/25429.html#concept-q3w-45w-wdb)。

- ECS API：支持GET和POST请求的RPC风格API。关于API说明，请参见

  API参考

  。以下为调用云服务器ECS API的常用开发者工具：

  - [命令行工具CLI](https://help.aliyun.com/document_detail/110244.html#concept-rc3-qrc-bhb)：基于阿里云API建立的灵活且易于扩展的管理工具。您可基于命令行工具封装阿里云的原生API，扩展出您需要的功能。
  - [OpenAPI Explorer](https://api.aliyun.com/)：提供快速检索接口、在线调用API和动态生成SDK示例代码等服务。
  - [阿里云SDK](https://develop.aliyun.com/tools/sdk?#/java)：提供Java、Python、PHP等多种编程语言的SDK。

- [资源编排（Resource Orchestration Service）](https://help.aliyun.com/document_detail/28852.html#concept-28852-zh)：通过创建一个描述您所需的所有阿里云资源的模板，然后资源编排将根据模板，自动创建和配置资源。

- [运维编排服务（Operation Orchestration Service）](https://help.aliyun.com/document_detail/125604.html#concept-1181174)：自动化管理和执行运维任务。您可以在执行模板中定义执行任务、执行顺序、执行输入和输出等，通过执行模板达到自动化完成运维任务的目的。

- [Terraform](https://help.aliyun.com/document_detail/91285.html#concept-twc-3dz-dfb)：能够通过配置文件在阿里云以及其他支持Terraform的云商平台调用计算资源，并对其进行版本控制的开源工具。

- 阿里云App：移动端类型的管理工具。

- [Alibaba Cloud Toolkit](https://help.aliyun.com/product/29966.html)：阿里云针对IDE平台为开发者提供的一款插件，用于帮助您高效开发并部署适合在云端运行的应用。

## 部署建议

您可以从以下维度考虑如何启动并使用云服务器ECS：

- 地域和可用区

  地域指阿里云的数据中心，地域和可用区决定了ECS实例所在的物理位置。一旦成功创建实例后，其元数据（仅专有网络VPC类型ECS实例支持获取元数据）将确定下来，并无法更换地域。您可以从用户地理位置、阿里云产品发布情况、应用可用性、以及是否需要内网通信等因素选择地域和可用区。例如，如果您同时需要通过阿里云内网使用云数据库RDS，RDS实例和ECS实例必须处于同一地域中。更多详情，请参见[地域和可用区](https://help.aliyun.com/document_detail/40654.html#concept-h4v-j5k-xdb)。

- 高可用性

  为保证业务处理的正确性和服务不中断，建议您通过快照实现数据备份，通过跨可用区、部署集、负载均衡（Server Load Balancer）等实现应用容灾。

- 网络规划

  阿里云推荐您使用专有网络VPC，可自行规划私网IP，全面支持新功能和新型实例规格。此外，专有网络VPC支持多业务系统隔离和多地域部署系统的使用场景。更多详情，请参见[专有网络（Virtual Private Cloud）](https://help.aliyun.com/document_detail/34217.html#concept-kbk-cpz-ndb)。

- 安全方案

  您可以使用云服务器ECS的安全组，控制ECS实例的出入网访问策略以及端口监听状态。对于部署在云服务器ECS上的应用，阿里云为您提供了免费的DDoS基础防护和基础安全服务，此外您还可以使用阿里云云盾，例如：

  - 通过DDoS高防IP保障源站的稳定可靠。更多详情，请参见[DDoS高防IP文档](https://help.aliyun.com/document_detail/28464.html#concept-28464-zh)。
  - 通过云安全中心保障云服务器ECS的安全。更多详情，请参见[云安全中心文档](https://help.aliyun.com/document_detail/42302.html#concept-bjv-y5w-ydb)。

## 相关服务

使用云服务器ECS的同时，您还可以选择以下阿里云服务：

- 根据业务需求和策略的变化，使用弹性伸缩（Auto Scaling）自动调整云服务器ECS的数量。更多详情，请参见[弹性伸缩](https://help.aliyun.com/document_detail/25857.html#concept-25857-zh)。
- 使用专有宿主机（Dedicated Host）部署ECS实例，可让您独享物理服务器资源、降低上云和业务部署调整的成本、满足严格的合规和监管要求。更多详情，请参见[专有宿主机DDH](https://help.aliyun.com/document_detail/68563.html#concept-yqx-czm-tdb)。
- 使用容器服务Kubernetes版在一组云服务器ECS上通过Docker容器管理应用生命周期。更多详情，请参见[容器服务Kubernetes版](https://help.aliyun.com/document_detail/86737.html#concept-cbm-1zc-l2b)。
- 通过负载均衡（Server Load Balancer）对多台云服务器ECS实现流量分发的负载均衡目的。更多详情，请参见[负载均衡](https://help.aliyun.com/document_detail/27539.html#concept-whs-lp4-tdb)。
- 通过云监控（CloudMonitor）制定实例、系统盘和公网带宽等的监控方案。更多详情，请参见[云监控](https://help.aliyun.com/document_detail/35170.html#concept-awy-bh4-tdb)。
- 在同一阿里云地域下，采用关系型云数据库（Relational Database Service）作为云服务器ECS的数据库应用是典型的业务访问架构，可极大降低网络延时和公网访问费用，并实现云数据库RDS的最佳性能。云数据库RDS支持多种数据库引擎，包括MySQL、SQL Server、PostgreSQL、PPAS和MariaDB。更多详情，请参见[关系型云数据库](https://help.aliyun.com/document_detail/26092.html#concept-pc2-lv5-tdb)。
- 在[云市场](https://market.aliyun.com/)获取由第三方服务商提供的基础软件、企业软件、网站建设、代运维、云安全、数据及API、解决方案等相关的各类软件和服务。您也可以成为云市场服务供应商，提供软件应用及服务。更多详情，请参见[云市场文档](https://help.aliyun.com/document_detail/51671.html#concept-2125940)。

更多方案，请参见[阿里云解决方案](https://www.aliyun.com/solution/all)。