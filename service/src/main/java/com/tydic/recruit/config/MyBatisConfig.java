package com.tydic.recruit.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/20 14:08
 */
@Configuration
public class MyBatisConfig {
//    @Value("${dialectClass}")
//    private String dialectClass;
//
//    /**
//     * 分页插件
//     */
//    @Bean
//    public PaginationStatementHandlerInterceptor paginationStatementHandlerInterceptor() {
//        PaginationStatementHandlerInterceptor paginationStatementHandlerInterceptor = new PaginationStatementHandlerInterceptor();
//        Properties properties = new Properties();
//        properties.setProperty("dialectClass", dialectClass);
//        paginationStatementHandlerInterceptor.setProperties(properties);
//
//        return paginationStatementHandlerInterceptor;
//    }
}
