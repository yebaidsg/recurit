//package com.tydic.order.service;
//
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.netflix.hystrix.EnableHystrix;
//
///**
// * 描述 service启动类 HSF
// *
// * @author tgy
// * @date 2019/7/23 10:02
// * @Copyright 2019 www.tydic.com Inc. All rights reserved.
// * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
// */
//@SpringBootApplication(scanBasePackages = {"com.ohaotian.*.*", "com.tydic.recruit.*"})
//@EnableDiscoveryClient
//@EnableCircuitBreaker
//@EnableHystrix
//@MapperScan(basePackages = "com.tydic.orderdemo.dao")
////dubbo启动需放开,非dubbo需关闭
////@EnableDubbo(scanBasePackages = "com.tydic.orderdemo")
//public class OrderServiceApplication {
//    public static void main(String[] args) {
//        // 启动Pandora Boot用于加载Pandora容器（hsf启动需要放开）
////        PandoraBootstrap.run(args);
//
//        SpringApplication.run(OrderServiceApplication.class, args);
//
//        // 标记服务启动完成,并设置线程wait 防止业务代码运行完毕退出后，导致容器退出（hsf启动需要放开）
////        PandoraBootstrap.markStartupAndWait();
//    }
//}
