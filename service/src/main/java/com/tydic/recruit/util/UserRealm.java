package com.tydic.recruit.util;

import com.ozo.ability.PermsService;
import com.ozo.ability.RoleService;
import com.ozo.ability.UserService;
import com.ozo.ability.bo.PermsBO;
import com.ozo.ability.bo.RoleBO;
import com.ozo.ability.bo.ShiroUser;
import com.ozo.ability.bo.UserBO;
import com.tydic.recruit.api.order.bo.User;
import com.tydic.recruit.dao.po.Role;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermsService permsService;

    /**
     * 授权
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户信息
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        try{
            List<RoleBO> roles = roleService.findRoleByUserId(Long.parseLong(user.getUid().toString()));
            List<Long> roleIds = new ArrayList<Long>();
            for (RoleBO role:roles){
                info.addRole(role.getRoleName());
                roleIds.add(role.getRoleId());//角色存储
            }
            for(Long roleId:roleIds){
                List<PermsBO> permss = permsService.findPermsByRoleId(roleId);
                for(PermsBO perms:permss) {
                    info.addStringPermission(perms.getPermsName());//权限储存
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return info;
        /*//设置角色
        Set<String> roles = new HashSet<>();
        //roles.add(user.getRole());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        //设置权限
        //info.addStringPermission(user.getPerms());
        return info;*/
//        return null;
    }

    /**
     * 认证
     * @param authenticationToken
     * @return
     * @throws AuthenticationException */

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        UserBO user = userService.findUserByName(token.getUsername());
        if(user != null){
            ShiroUser shiroUser = new ShiroUser();
            shiroUser.setUserId(user.getUserId());
            shiroUser.setName(user.getUsername());
            shiroUser.setPassword(user.getPassword());
            shiroUser.setAuthCacheKey(user.getUsername()+user.getUserId());
            //System.out.println(shiroUser);
            return new SimpleAuthenticationInfo(shiroUser,shiroUser.getPassword(),this.getName());
        }
        return null;

    }
}
