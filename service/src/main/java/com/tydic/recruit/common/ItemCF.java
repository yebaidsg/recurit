package com.tydic.recruit.common;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

/**
 * @author huzb
 * @标题 ItemCF
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 21:20
 */
public class ItemCF {
    //推荐物品的最大个数
    final static int RECOMMENDER_NUM = 3;

    public static void main(String[] args) throws IOException, TasteException {
        String file = "E:\\evaluate_position.csv";
        //数据模型
        DataModel model = new FileDataModel(new File(file));
        //用户相识度算法
        ItemSimilarity item=new EuclideanDistanceSimilarity(model);
        //物品推荐算法
        Recommender r=new GenericItemBasedRecommender(model,item);
        LongPrimitiveIterator iter =model.getUserIDs();
        while(iter.hasNext()){
            long uid=iter.nextLong();
            List<RecommendedItem> list = r.recommend(uid, 1);
            System.out.printf("uid:%s",uid);
            for (RecommendedItem ritem : list) {
                System.out.println("itemID:" + ritem.getItemID());
                System.out.println("value:" + ritem.getValue());
//                System.out.printf("(%s,%f)", ritem.getItemID(), ritem.getValue());
            }
            System.out.println();
        }
    }
}
