package com.tydic.recruit.common;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huzb
 * @标题 UserCF
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 21:20
 */
public class UserCF {
    //临近的用户个数
    final static int NEIGHBORHOOD_NUM = 2;
    //推荐物品的最大个数
    final static int RECOMMENDER_NUM = 3;


    List<String> userCF(String file) throws IOException, TasteException {
        List<String> resultList = new ArrayList<>();
//        String file = "E:\\evaluate_position.csv";
        //数据模型
        DataModel model = new FileDataModel(new File(file));
        //用户相识度算法
        UserSimilarity user = new EuclideanDistanceSimilarity(model);
        NearestNUserNeighborhood neighbor = new NearestNUserNeighborhood(NEIGHBORHOOD_NUM, user, model);
        //用户近邻算法
        Recommender r = new GenericUserBasedRecommender(model, neighbor, user);//用户推荐算法
        LongPrimitiveIterator iter = model.getUserIDs();///得到用户ID

        while (iter.hasNext()) {
            long uid = iter.nextLong();
            List<RecommendedItem> list = r.recommend(uid, RECOMMENDER_NUM);
            System.out.printf("uid:%s", uid);
            for (RecommendedItem ritem : list) {
                resultList.add(String.valueOf(ritem.getItemID()));
                System.out.println("itemID:" + ritem.getItemID());
                System.out.println("value:" + ritem.getValue());
//                System.out.printf("(%s,%f)", ritem.getItemID(), ritem.getValue());
            }
            System.out.println();
        }
        return resultList;
    }


    public static void main(String[] args) throws IOException, TasteException {
        List<String> resultList = new ArrayList<>();
        String file = "E:\\evaluate_position.csv";
        //数据模型
        DataModel model = new FileDataModel(new File(file));
        UserSimilarity user = new EuclideanDistanceSimilarity(model);//用户相识度算法
        NearestNUserNeighborhood neighbor = new NearestNUserNeighborhood(NEIGHBORHOOD_NUM, user, model);
        //用户近邻算法
        Recommender r = new GenericUserBasedRecommender(model, neighbor, user);//用户推荐算法
        LongPrimitiveIterator iter = model.getUserIDs();///得到用户ID

        while (iter.hasNext()) {
            long uid = iter.nextLong();
            List<RecommendedItem> list = r.recommend(uid, RECOMMENDER_NUM);
            System.out.printf("uid:%s", uid);
            for (RecommendedItem ritem : list) {
                resultList.add(String.valueOf(ritem.getItemID()));
                System.out.println("itemID:" + ritem.getItemID());
                System.out.println("value:" + ritem.getValue());
//                System.out.printf("(%s,%f)", ritem.getItemID(), ritem.getValue());
            }
            System.out.println();
        }
    }

}
