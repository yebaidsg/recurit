package com.tydic.recruit.controller;

import com.tydic.recruit.api.busi.recruit.InsertPositionBusiService;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceRspBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huzb
 * @标题 CsvController
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 19:14
 */
@RestController
@RequestMapping("/csv")
public class CsvController {

    @Autowired
    private InsertPositionBusiService insertPositionBusiService;
    @RequestMapping("/insert")
    public InsertPositionBusiServiceRspBO insert(@RequestBody InsertPositionBusiServiceReqBO reqBO){
        return insertPositionBusiService.insert(reqBO);
    }
}
