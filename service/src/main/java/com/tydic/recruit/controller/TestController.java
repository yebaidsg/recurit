//package com.tydic.recruit.controller;
//
//import com.ohaotian.plugin.base.annotation.BusiResponseBody;
//import com.ohaotian.plugin.base.bo.RspPage;
//import com.ohaotian.plugin.base.exception.ZTBusinessException;
//import com.tydic.recruit.api.busi.BO.*;
//import com.tydic.recruit.api.busi.CloseOrderBusiService;
//import com.tydic.recruit.api.busi.CreateOrderBusiServise;
//import com.tydic.recruit.api.busi.SelectAllOrderBusiService;
//import lombok.extern.ohaotian.HTServiceRef;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.*;
//
///**
// * @author 胡中宝
// * @Description TODO
// * @copyright 2020 www.tydic.com Inc. All rights reserved.
// * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
// * @since 2020/10/20 10:58
// */
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//
//    /**
//     * 创建订单
//     */
//    @HTServiceRef
//    private CreateOrderBusiServise createOrderBusiServise;
//    /**
//     * 查找所有订单
//     */
//    @HTServiceRef
//    private SelectAllOrderBusiService selectAllOrderBusiService;
//
//    /**
//     * 关闭订单
//     * @return
//     */
//    @HTServiceRef
//    private CloseOrderBusiService closeOrderBusiService;
////    @HTServiceRef
////    private CreateNextOrderBusiService createNextOrderBusiService;
//    @GetMapping("/test")
//    public String test(){
//        System.out.println("s======");
//        return "test";
//    }
//
//    @RequestMapping(value = "/insert",method = RequestMethod.POST,consumes = "application/json")
//    @BusiResponseBody
//    public void insertMain(@RequestBody CreateOrderBusiReqBO createOrderBusiReqBO, BindingResult result){
//        int i = 0;
//        if (result.hasErrors()){
//            throw new ZTBusinessException(result.getAllErrors().get(0).getDefaultMessage());
//        }else {
//            createOrderBusiServise.createOrder(createOrderBusiReqBO);
//        }
//    }
//
//    @GetMapping("/findOrder")
//    @BusiResponseBody
//    public RspPage<AllOrderRspBO> selectOrder(AllOrderReqBO allOrderReqBO){
//        RspPage<AllOrderRspBO> rspPage = selectAllOrderBusiService.selectAllOrderBusiByOrderId(allOrderReqBO);
//        return rspPage;
//    }
//
//    /**
//     * 关闭订单
//     */
//
//    @PostMapping("/closeOrder")
//    @BusiResponseBody
//    public CloseOrderRspBO closeOrder(CloseOrderReqBO closeOrderReqBO){
//        return closeOrderBusiService.closeOrderBusi(closeOrderReqBO);
//    }
//}
