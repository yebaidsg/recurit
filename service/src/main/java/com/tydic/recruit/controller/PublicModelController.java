package com.tydic.recruit.controller;

import com.tydic.recruit.api.ability.publicModel.GraduationSelectAreaAbilityService;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectCityAbilityService;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectDictionaryListAbilityService;
import com.tydic.recruit.api.ability.publicModel.GraduationSelectProvinceAbilityService;
import com.tydic.recruit.api.ability.publicModel.bo.*;
import lombok.extern.ohaotian.HTServiceRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huzb
 * @标题 PublicModelController
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/3 15:45
 */
@RestController
@RequestMapping("/public/model")
public class PublicModelController {

    @Autowired
    private GraduationSelectProvinceAbilityService graduationSelectProvinceAbilityService;

    @HTServiceRef
    private GraduationSelectCityAbilityService graduationSelectCityAbilityService;

    @HTServiceRef
    private GraduationSelectAreaAbilityService graduationSelectAreaAbilityService;

    @HTServiceRef
    private GraduationSelectDictionaryListAbilityService graduationSelectDictionaryListAbilityService;
    /**
     * 查询省份
     */
    @RequestMapping("/selectProvince")
    @ResponseBody
    GraduationSelectProvinceAbilityRspBO SelectProvince(@RequestBody GraduationSelectProvinceAbilityReqBO reqBO){
        return graduationSelectProvinceAbilityService.SelectProvince(reqBO);
    }
    /**
     * 查询市
     */
    @RequestMapping("/selectCityList")
    @ResponseBody
    GraduationSelectCityAbilityRspBO selectCityList(@RequestBody GraduationSelectCityAbilityReqBO reqBO){
        return graduationSelectCityAbilityService.selectCityList(reqBO);
    }

    /**
     * 查询区县
     */
    @RequestMapping("/selectAreaList")
    @ResponseBody
    GraduationSelectAreaAbilityRspBO selectAreaList(@RequestBody GraduationSelectAreaAbilityReqBO reqBO){
        return graduationSelectAreaAbilityService.selectAreaList(reqBO);
    }

    /**
     * 查询字典
     */
    @RequestMapping("/selectDictionaryList")
    @ResponseBody
    GraduationSelectDictionaryListAbilityRspBO selectDictionaryList(@RequestBody GraduationSelectDictionaryListAbilityReqBO reqBO){
        return graduationSelectDictionaryListAbilityService.selectDictionaryList(reqBO);
    }
}
