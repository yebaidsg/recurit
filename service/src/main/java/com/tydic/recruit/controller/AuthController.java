package com.tydic.recruit.controller;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import com.ozo.ability.ImageService;
import com.ozo.ability.PermsService;
import com.ozo.ability.RoleService;
import com.ozo.ability.UserService;
import com.ozo.ability.bo.ImageBO;
import com.ozo.ability.bo.RoleBO;
import com.ozo.ability.bo.ShiroUser;
import com.ozo.ability.bo.UserBO;
import com.tydic.recruit.dao.po.Image;
import com.tydic.recruit.dao.po.Role;
import com.tydic.recruit.dao.po.User;
import com.tydic.recruit.dao.user.UserMapper;
import com.tydic.recruit.util.AjaxResult;
import com.tydic.recruit.util.GuuidUtil;
import com.tydic.recruit.util.JwtUtil;
import com.tydic.recruit.util.RedisUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermsService permsService;

    @Autowired
    private ImageService imageService;
    @Autowired
    private UserMapper userMapper;

    @PostMapping("/login")
    public AjaxResult login(@RequestBody User user, HttpSession session) {
        //从shiro中，获取一个Subject对象
        Subject subject = SecurityUtils.getSubject();
        UserBO user1 = userService.findUserByName(user.getUsername());
        ShiroUser shiroUser = new ShiroUser();
        shiroUser.setUserId(user1.getUserId());
        shiroUser.setName(user.getUsername());
        AuthenticationToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
        if(token != null){
            try {
                subject.login(token);
                String resToken = JwtUtil.sign1(user.getUsername(), user.getPassword());
                System.out.println(resToken);
                if(resToken != null){
                    redisUtil.set(resToken, user.getUsername(), 60 * 60);
                    shiroUser.setToken(resToken);
                    System.out.println(shiroUser);
                    User userpo = userMapper.findUserByName(user.getUsername());
                    session.setAttribute("user",userpo);
                    shiroUser.setUserId(userpo.getUserId());
                    shiroUser.setName(userpo.getUsername());
                    return AjaxResult.success("成功",shiroUser);
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @GetMapping("/info")
    public AjaxResult findUserInfo(@RequestHeader("X-Token") String token, HttpSession session){
        Boolean flag = JwtUtil.verify(token);
        String username = JwtUtil.getUserName(token);
        if(!flag){
            throw new RuntimeException("没有token，请重新登录");
        }else {
            ShiroUser shiroUser = new ShiroUser();
            shiroUser.setName(username);
            List<RoleBO> roles = roleService.findRoleByUsername(username);
            System.out.println(roles);
            List<String> roless = new ArrayList<String>();
            for (RoleBO role:roles){
                roless.add(role.getRoleName());
            }
            shiroUser.setRoles(roless);
            UserBO user = userService.findUserByName(username);
            ImageBO image = imageService.findImageByconnectId(user.getUserId());
            String imageUrl;
            String baseUrl = "http://localhost:8001/";
            if(image != null){
                imageUrl = baseUrl + image.getImageUrl();
            }else{
                imageUrl = baseUrl + "image/defaultAvatar.png";
            }
            shiroUser.setAvatar(imageUrl);
            if(shiroUser == null){
                throw new RuntimeException("没有用户，请重新登录1");
            }else {
                System.out.println(shiroUser);
                return AjaxResult.success("成功",shiroUser);
            }
        }
    }

    @PostMapping("/logout")
    public AjaxResult logout(@RequestHeader("X-Token") String token){
        SecurityUtils.getSubject().logout();
        redisUtil.del(token);
        return AjaxResult.success("成功");
    }
    @PostMapping("/insertUser")
    @ResponseBody
    RspBaseBO insertUser(@RequestBody UserBO user){
        user.setUserId(GuuidUtil.getUUID());
        return userService.insertUser(user);
    }
}
