package com.tydic.recruit.controller;

import com.alibaba.fastjson.JSONObject;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiRspBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiRspBO;
import com.tydic.recruit.api.busi.python.DateCrawlingBusiService;
import com.tydic.recruit.api.busi.python.DateCrawlingHandleBusiService;
import com.tydic.recruit.util.AjaxResult;
import lombok.extern.ohaotian.HTServiceRef;
import netscape.javascript.JSObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huzb
 * @标题 PythonController
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 18:10
 */
@RestController
@RequestMapping("/python")
public class PythonController {
    @HTServiceRef
    private DateCrawlingBusiService dateCrawlingBusiService;
    @HTServiceRef
    private DateCrawlingHandleBusiService dateCrawlingHandleBusiService;
    /**
     * 获取数据
     * @param
     * @return
     */
    @RequestMapping("/getCrawling/{end}/{netState}")
    AjaxResult getCrawling(@PathVariable("end") String end, @PathVariable("netState") String netState){
        DateCrawlingBusiReqBO reqBO = new DateCrawlingBusiReqBO();
        reqBO.setEnd(end);
        reqBO.setNetState(netState);
        DateCrawlingBusiRspBO rspBO = dateCrawlingBusiService.getCrawling(reqBO);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(Integer.parseInt(rspBO.getCode()));
        ajaxResult.setMessage(rspBO.getMessage());
        return ajaxResult;
    }
    /**
     * 预处理数据
     */
    @RequestMapping("/dataHandle/{NETSTATE}")
    DateCrawlingHandleBusiRspBO dataHandle(@PathVariable("NETSTATE") String netState){
        DateCrawlingHandleBusiReqBO reqBO = new DateCrawlingHandleBusiReqBO ();
        reqBO.setNetState(Integer.parseInt(netState));
        DateCrawlingHandleBusiRspBO rspBO = dateCrawlingHandleBusiService.dataHandle(reqBO);
        rspBO.setCode(20000);
        rspBO.setMessage(rspBO.getMessage());
        return rspBO;
    }
}
