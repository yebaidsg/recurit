package com.tydic.recruit.controller;

import com.ozo.ability.RoleService;
import com.ozo.ability.bo.RoleBO;
import com.tydic.recruit.dao.po.Role;
import com.tydic.recruit.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping("/findAll")
    public AjaxResult findAllRole(){
        return AjaxResult.success("成功",roleService.findAllRole());
    }

    @PostMapping("/insert")
    public AjaxResult insertRole(@RequestBody RoleBO role){
        roleService.insertRole(role);
        System.out.println(role);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{roleId}")
    public AjaxResult deleteRole(@PathVariable Long roleId){
        roleService.deleteRole(roleId);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update/{id}")
    public AjaxResult updateRole(@PathVariable("id") Long id, @RequestBody RoleBO role){
        roleService.updateRoleById(role,id);
        return AjaxResult.success("成功");
    }


}
