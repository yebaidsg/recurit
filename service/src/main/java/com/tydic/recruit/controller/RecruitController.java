package com.tydic.recruit.controller;

import com.alibaba.fastjson.JSONObject;
import com.tydic.recruit.api.ability.recruit.*;
import com.tydic.recruit.api.ability.recruit.BO.*;
import com.tydic.recruit.api.busi.recruit.*;
import com.tydic.recruit.api.busi.recruit.bo.*;
import com.tydic.recruit.dao.po.User;
import com.tydic.recruit.dao.recurit.*;
import com.tydic.recruit.dao.recurit.po.DeliverPositionPO;
import com.tydic.recruit.util.JwtUtil;
import lombok.extern.ohaotian.HTServiceImpl;
import lombok.extern.ohaotian.HTServiceRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author huzb
 * @标题 RecruitController
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/3 16:26
 */
@RestController
@RequestMapping("/recruit")
public class RecruitController {

    @HTServiceRef
    private GraduationRecruitAddResumeAbilityService graduationRecruitAddResumeAbilityService;

    @HTServiceRef
    private GraduationRecruitUpdateResumeAbilityService graduationRecruitUpdateResumeAbilityService;

    @HTServiceRef
    private GraduationRecruitSelectResumeAbilityService graduationRecruitSelectResumeAbilityService;

    @HTServiceRef
    private GraduationRecruitAddJobIntentionalAbilityService graduationRecruitAddJobIntentionalAbilityService;

    @HTServiceRef
    private GraduationRecruitUpdateJobIntentionalAbilityService graduationRecruitUpdateJobIntentionalAbilityService;

    @HTServiceRef
    private GraduationRecruitSelectJobIntentionalAbilityService graduationRecruitSelectJobIntentionalAbilityService;

    @HTServiceRef
    private GraduationRecruitAddEducationAbilityService graduationRecruitAddEducationAbilityService;

    @HTServiceRef
    private GraduationRecruitUpdateEducationAbilityService graduationRecruitUpdateEducationAbilityService;

    @HTServiceRef
    private GraduationRecruitSelectEducationAbilityService graduationRecruitSelectEducationAbilityService;

    @HTServiceRef
    private GraduationAddWorkExperienceAbilityService graduationAddWorkExperienceAbilityService;

    @HTServiceRef
    private GraduationUpdateWorkExperienceAbilityService graduationUpdateWorkExperienceAbilityService;

    @HTServiceRef
    private GraduationSelectWorkExperienceAbilityService graduationSelectWorkExperienceAbilityService;

    @HTServiceRef
    private GraduationAddProjectExperienceAbilityService graduationAddProjectExperienceAbilityService;

    @HTServiceRef
    private GraduationUpdateProjectExperienceAbilityService graduationUpdateProjectExperienceAbilityService;

    @HTServiceRef
    private GraduationSelectProjectExperienceAbilityService graduationSelectProjectExperienceAbilityService;

    @HTServiceRef
    private GraduationAddTrainingExperienceAbilityService graduationAddTrainingExperienceAbilityService;

    @HTServiceRef
    private GraduationUpdateTrainingExperienceAbilityService graduationUpdateTrainingExperienceAbilityService;

    @HTServiceRef
    private GraduationSelectTrainingExperienceAbilityService graduationSelectTrainingExperienceAbilityService;

    @HTServiceRef
    private GraduationAddLanguageAbilityService graduationAddLanguageAbilityService;

    @HTServiceRef
    private GraduationUpdateLanguageAbilityService graduationUpdateLanguageAbilityService;

    @HTServiceRef
    private GraduationSelectLanguageAbilityService graduationSelectLanguageAbilityService;

    @HTServiceRef
    private GraduationAddProfessionalSkillsAbilityService graduationAddProfessionalSkillsAbilityService;

    @HTServiceRef
    private GraduationUpdateProfessionalSkillsAbilityService graduationUpdateProfessionalSkillsAbilityService;

    @HTServiceRef
    private GraduationSelectProfessionalSkillsAbilityService graduationSelectProfessionalSkillsAbilityService;

    @HTServiceRef
    private GradeAddCertificateAbilityService gradeAddCertificateAbilityService;

    @HTServiceRef
    private GraduationUpdateCertificateAbilityService graduationUpdateCertificateAbilityService;

    @HTServiceRef
    private GraduationSelectCertificateAbilityService graduationSelectCertificateAbilityService;

    @HTServiceRef
    private GraduationSelectJonIntentionalListBusiService graduationSelectJonIntentionalListBusiService;

    @HTServiceRef
    private GraduationSelectEducationListBusiService graduationSelectEducationListBusiService;

    @HTServiceRef
    private GraduationSelectWorkExperienceListBusiService graduationSelectWorkExperienceListBusiService;

    @HTServiceRef
    private GraduationSelectProjectExperienceListBusiService graduationSelectProjectExperienceListBusiService;

    @HTServiceRef
    private GraduationSelectTrainingExperienceBusiService graduationSelectTrainingExperienceBusiService;

    @HTServiceRef
    private GraduationSelectLanguageListBusiService graduationSelectLanguageListBusiService;

    @HTServiceRef
    private GraduationSelectProfessionalSkillsListBusiService graduationSelectProfessionalSkillsListBusiService;

    @HTServiceRef
    private GradeSelectCertificateListBusiService gradeSelectCertificateListBusiService;

    @HTServiceRef
    private SelectPositionListBusiService selectPositionListBusiService;

    @HTServiceRef
    private SelectPositionDetailBusiService selectPositionDetailBusiService;
    @HTServiceRef
    private GraduationSelectTrainingExperienceListBusiService graduationSelectTrainingExperienceListBusiService;
    @HTServiceRef
    private InsertEvaluatePositionBusiService insertEvaluatePositionBusiService;

    @HTServiceRef
    private GraduationSelectRecommendListBusiService graduationSelectRecommendListBusiService;
    @HTServiceRef
    private GraduationSelectEvaluateBusiService graduationSelectEvaluateBusiService;
    @HTServiceRef
    private GraduationSelectEvaluatedListBusiService graduationSelectEvaluatedListBusiService;
    @HTServiceRef
    private GraduationInsertDeliverBusiService graduationInsertDeliverBusiService;
    @HTServiceRef
    private GraduationDeleteDeliverBusiService graduationDeleteDeliverBusiService;
    @HTServiceRef
    private GraduationSelectDeliverBusiService graduationSelectDeliverBusiService;
    @HTServiceRef
    private PositionRecommendBusiService positionRecommendBusiService;
    @HTServiceRef
    private GraduationSelectPositionCommendBusiService graduationSelectPositionCommendBusiService;
    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;

    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;
    @Autowired
    private WorkExperienceMapper workExperienceMapper;
    @Autowired
    private ProjectExperienceMapper projectExperienceMapper;
    @Autowired
    private TrainingExperienceMapper trainingExperienceMapper;
    @Autowired
    private LanguageAbilityMapper languageAbilityMapper;
    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;
    @Autowired
    private CertificateMapper certificateMapper;
    @Autowired
    private EvaluatePositionMapper evaluatePositionMapper;

    @Autowired
    private PositionMapper positionMapper;
    /**
     * 新增个人信息
     */
    @RequestMapping("/addResume")
    @ResponseBody
    GraduationRecruitAddResumeAbilityRspBO addResume(@RequestBody GraduationRecruitAddResumeAbilityReqBO reqBO, HttpServletRequest request){
        System.out.println(request);
        return graduationRecruitAddResumeAbilityService.addResume(reqBO);
    }
    /**
     * 修改个人信息
     */
    @RequestMapping("/updateResume")
    @ResponseBody
    GraduationRecruitUpdateResumeAbilityRspBO updateResume(@RequestBody GraduationRecruitUpdateResumeAbilityReqBO reqBO){
        return graduationRecruitUpdateResumeAbilityService.updateResume(reqBO);
    }
    /**
     * 查询个人信息
     */
    @RequestMapping("/selectResume")
    @ResponseBody
    GraduationRecruitSelectResumeAbilityRspBO selectResume(@RequestBody GraduationRecruitSelectResumeAbilityReqBO reqBO){
        return graduationRecruitSelectResumeAbilityService.selectResume(reqBO);
    }
    /**
     * 新增求职意向
     */
    @RequestMapping("/addJobInternational")
    @ResponseBody
    GraduationRecruitAddJobIntentionalAbilityRspBO addJobInternational(@RequestBody GraduationRecruitAddJobIntentionalAbilityReqBO reqBO){
        return graduationRecruitAddJobIntentionalAbilityService.addJobInternational(reqBO);
    }
    /**
     * 修改求职意向
     */
    @RequestMapping("/updateJob")
    @ResponseBody
    GraduationRecruitUpdateJobIntentionalAbilityRspBO updateJob(@RequestBody GraduationRecruitUpdateJobIntentionalAbilityReqBO reqBO){
        return graduationRecruitUpdateJobIntentionalAbilityService.updateJob(reqBO);
    }
    /**
     * 删除求职意向
     */
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Long id){
        System.out.println(id);
        personJobIntentionMapper.delectById(id);
    }
    /**
     * 查询求职意向
     */
    @RequestMapping("/selectJob")
    @ResponseBody
    GraduationRecruitSelectJobIntentionalAbilityRspBO selectJob(@RequestBody GraduationRecruitSelectJobIntentionalAbilityReqBO reqBO){
        return graduationRecruitSelectJobIntentionalAbilityService.selectJob(reqBO);
    }

    /**
     * 求职意向列表查询
     */
    @RequestMapping("/selectJonIntentionalList")
    @ResponseBody
    GraduationRecruitSelectJobIntentionalListBusiRsp selectJonIntentionalList(@RequestBody GraduationRecruitSelectJobIntentionalListBusiReq req){
        System.out.println(JSONObject.toJSON(graduationSelectJonIntentionalListBusiService.selectJonIntentionalList(req)));
        return graduationSelectJonIntentionalListBusiService.selectJonIntentionalList(req);
    }


    /**
     * 新增教育经历
     */
    @RequestMapping("/addEducation")
    @ResponseBody
    GraduationRecruitAddEducationAbilityRspBO addEducation(@RequestBody GraduationRecruitAddEducationAbilityReqBO reqBO){
        return graduationRecruitAddEducationAbilityService.addEducation(reqBO);
    }

    /**
     * 修改教育经理
     */
    @RequestMapping("/updateEducation")
    @ResponseBody
    GraduationRecruitUpdateEducationAbilityRspBO updateEducation(@RequestBody GraduationRecruitUpdateEducationAbilityReqBO reqBO){
        return graduationRecruitUpdateEducationAbilityService.updateEducation(reqBO);
    }

    /**
     * 查询教育经历
     */
    @RequestMapping("/selectEducation")
    @ResponseBody
    GraduationRecruitSelectEducationAbilityRspBO selectEducation(@RequestBody GraduationRecruitSelectEducationAbilityReqBO reqBO){
        return graduationRecruitSelectEducationAbilityService.selectEducation(reqBO);
    }

    /**
     * 教育经理列表查询
     */
    @RequestMapping("/selectEduList")
    @ResponseBody
    GraduationSelectEducationListBusiRspBo selectList(@RequestBody GraduationSelectEducationListBusiReqBo reqBo){
        return graduationSelectEducationListBusiService.selectList(reqBo);
    }
    @DeleteMapping("/deleteByEduId/{id}")
    public void deleteByEduId(@PathVariable("id") Long id){
        educationalExperienceMapper.deleteById(id);
    }

    /**
     * 新增工作经历
     */
    @RequestMapping("/addWorkExperience")
    @ResponseBody
    GraduationAddWorkExperienceAbilityRspBO addWorkExperience(@RequestBody GraduationAddWorkExperienceAbilityReqBO reqBO){
        return graduationAddWorkExperienceAbilityService.addWorkExperience(reqBO);
    }

    /**
     * 修改工作经历
     */
    @RequestMapping("/updateWork")
    @ResponseBody
    GraduationUpdateWorkExperienceAbilityRspBO updateWork(@RequestBody GraduationUpdateWorkExperienceAbilityReqBO reqBO){
        return graduationUpdateWorkExperienceAbilityService.updateWork(reqBO);
    }

    /**
     * 查询工作经历
     */
    @RequestMapping("/select")
    @ResponseBody
    GraduationSelectWorkExperienceAbilityRspBO select(@RequestBody GraduationSelectWorkExperienceAbilityReqBO reqBO){
        return graduationSelectWorkExperienceAbilityService.select(reqBO);
    }

    /**
     * 删除工作经历
     * @param id
     */
    @DeleteMapping("/deleteByWorkId/{id}")
    public void deleteByWorkId(@PathVariable("id") Long id){
        System.out.println(id);
        workExperienceMapper.deleteByWorkId(id);
    }
    /**
     * 列表查询工作经历
     */
    @RequestMapping("/selectWorList")
    @ResponseBody
    GraduationSelectWorkExperienceListBusiRspBO selectWorList(@RequestBody GraduationSelectWorkExperienceListBusiReqBO reqBO){
        return graduationSelectWorkExperienceListBusiService.selectWorList(reqBO);
    }

    /**
     * 新增项目经历
     */
    @RequestMapping("/addProjectExperience")
    @ResponseBody
    GraduationProjectExperienceAbilityRspBO addProjectExperience(@RequestBody GraduationProjectExperienceAbilityReqBO reqBO){
        return graduationAddProjectExperienceAbilityService.addProjectExperience(reqBO);
    }

    /**
     * 修改项目经历
     */
    @RequestMapping("/updateProjectExperience")
    @ResponseBody
    GraduationUpdateProjectExperienceAbilityRspBO updateProjectExperience(@RequestBody GraduationUpdateProjectExperienceAbilityReqBO reqBO){
        return graduationUpdateProjectExperienceAbilityService.updateProjectExperience(reqBO);
    }

    /**
     * 查询项目经历
     */
    @RequestMapping("/selectProjectExperience")
    @ResponseBody
    GraduationSelectProjectExperienceAbilityRspBO selectProjectExperience(@RequestBody GraduationSelectProjectExperienceAbilityReqBO reqBO){
        return graduationSelectProjectExperienceAbilityService.selectProjectExperience(reqBO);
    }

    /**
     * 列表查询项目经理
     */
    @RequestMapping("/selectProjectList")
    @ResponseBody
    GraduationSelectProjectExperienceListBusiRspBO selectProjectList(@RequestBody GraduationSelectProjectExperienceListBusiReqBO reqBO){
        return graduationSelectProjectExperienceListBusiService.selectProjectList(reqBO);
    }
    /**
     * 删除项目经理
     */
    @DeleteMapping("/deleteProjectById/{id}")
    public void deleteProjectById(@PathVariable("id") Long id){
        projectExperienceMapper.delete(id);
    }
    /**
     * 新增培训经历
     */
    @RequestMapping("/addTraining")
    @ResponseBody
    GraduationAddTrainingExperienceAbilityRspBO addTraining(@RequestBody GraduationAddTrainingExperienceAbilityReqBO reqBO){
        return graduationAddTrainingExperienceAbilityService.addTraining(reqBO);
    }

    /**
     * 修改培训经历
     */
    @RequestMapping("/updateTraining")
    @ResponseBody
    GraduationUpdateTrainingExperienceAbilityRspBO updateTraining(@RequestBody GraduationUpdateTrainingExperienceAbilityReqBO reqBO){
        return graduationUpdateTrainingExperienceAbilityService.updateTraining(reqBO);
    }

    /**
     * 查询培训经历
     */
    @RequestMapping("/selectTraining")
    @ResponseBody
    GraduationSelectTrainingExperienceAbilityRspBO selectTraining(@RequestBody GraduationSelectTrainingExperienceAbilityReqBO reqBO){
        return graduationSelectTrainingExperienceAbilityService.selectTraining(reqBO);
    }

    /**
     * 列表查询培训经历
     */
    @RequestMapping("/selectTrainingList")
    @ResponseBody
    GraduationSelectTrainingExperienceListBusiRspBO selectTrainingList(@RequestBody GraduationSelectTrainingExperienceListBusiReqBO reqBO){
        return graduationSelectTrainingExperienceListBusiService.selectList(reqBO);
    }
    /**
     * 删除培训径流
     */
    @DeleteMapping("/deleteTraining/{id}")
    public void deleteTraining(@PathVariable("id") Long id){
        trainingExperienceMapper.delete(id);
    }
    /**
     * 新增语言能力
     */
    @RequestMapping("/addLanguage")
    @ResponseBody
    GraduationAddLanguageAbilityRspBO addLanguage(@RequestBody GraduationAddLanguageAbilityReqBO reqBO){
        return graduationAddLanguageAbilityService.addLanguage(reqBO);
    }

    /**
     * 修改语言能力
     */
    @RequestMapping("/updateLanguage")
    @ResponseBody
    GraduationUpdateLanguageAbilityRspBO updateLanguage(@RequestBody GraduationUpdateLanguageAbilityReqBO reqBO){
        return graduationUpdateLanguageAbilityService.updateLanguage(reqBO);
    }

    /**
     * 查询语言能力
     */
    @RequestMapping("/selectLanguage")
    @ResponseBody
    GraduationSelectLanguageAbilityRspBO selectLanguage(@RequestBody GraduationSelectLanguageAbilityReqBO reqBO){
        return graduationSelectLanguageAbilityService.selectLanguage(reqBO);
    }

    /**
     * 列表查询语言能力
     */
    @RequestMapping("/selectLanList")
    @ResponseBody
    GraduationSelectLanguageListBusiRspBO selectLanList(@RequestBody GraduationSelectLanguageListBusiReqBO reqBO){
        return graduationSelectLanguageListBusiService.selectLanList(reqBO);
    }
    /**
     * 删除语言能力
     */
    @DeleteMapping("/deleteLanguage/{id}")
    public void deleteLanguage(@PathVariable("id") Long id){
        languageAbilityMapper.delete(id);
    }
    /**
     * 新增专业技能
     */
    @RequestMapping("/addProfession")
    @ResponseBody
    GraduationProfessionalSkillsAbilityRspBO addProfession(@RequestBody GraduationProfessionalSkillsAbilityReqBO reqBO){
        return graduationAddProfessionalSkillsAbilityService.addProfession(reqBO);
    }

    /**
     * 修改专业技能
     */
    @RequestMapping("/updateProfession")
    @ResponseBody
    GraduationUpdateProfessionalSkillsAbilityRspBO updateProfession(@RequestBody GraduationUpdateProfessionalSkillsAbilityReqBO reqBO){
        return graduationUpdateProfessionalSkillsAbilityService.updateProfession(reqBO);
    }

    /**
     * 查询专业技能
     */
    @RequestMapping("/selectProfession")
    @ResponseBody
    GraduationSelectProfessionalSkillsAbilityRspBO selectProfession(@RequestBody GraduationSelectProfessionalSkillsAbilityReqBO reqBO){
        return graduationSelectProfessionalSkillsAbilityService.selectProfession(reqBO);
    }

    /**
     * 列表查询专业技能
     */
    @RequestMapping("/selectSkillsList")
    @ResponseBody
    GraduationSelectProfessionalSkillsListBusiRspBO selectSkillsList(@RequestBody GraduationSelectProfessionalSkillsListBusiReqBO reqBO){
        return graduationSelectProfessionalSkillsListBusiService.selectSkillsList(reqBO);
    }
    /**
     * 删除专业技能
     */
    @DeleteMapping("/deleteSkills/{id}")
    public void deleteSkills(@PathVariable("id") Long id){
        professionalSkillsMapper.delete(id);
    }
    /**
     * 新增证书
     */
    @RequestMapping("/addCertificate")
    @ResponseBody
    GradeAddCertificateAbilityRspBO addCertificate(@RequestBody GradeAddCertificateAbilityReqBO reqBO){
        return gradeAddCertificateAbilityService.addCertificate(reqBO);
    }

    /**
     * 修改证书
     */
    @RequestMapping("/updateCertificate")
    @ResponseBody
    GraduationUpdateCertificateAbilityRspBO updateCertificate(@RequestBody GraduationUpdateCertificateAbilityReqBO reqBO){
        return graduationUpdateCertificateAbilityService.updateCertificate(reqBO);
    }

    /**
     * 查询证书
     */
    @RequestMapping("/selectCertificate")
    @ResponseBody
    GraduationSelectCertificateAbilityRspBO selectCertificate(@RequestBody GraduationSelectCertificateAbilityReqBO reqBO){
        return graduationSelectCertificateAbilityService.selectCertificate(reqBO);
    }

    /**
     * 列表查询证书
     */
    @RequestMapping("/selectCertificateList")
    @ResponseBody
    GradeSelectCertificateListBusiRspBO selectCertificateList(@RequestBody GradeSelectCertificateListBusiReqBO reqBO){
        return gradeSelectCertificateListBusiService.selectCertificateList(reqBO);
    }
    /**
     * 删除
     */
    @DeleteMapping("/deleteCertificate/{id}")
    public void deleteCertificate(@PathVariable("id") Long id){
        certificateMapper.delete(id);
    }

    /**
     * 列表查询所有职位信息
     */
    @RequestMapping("/selectPosiList")
    @ResponseBody
    SelectPositionListBusiRspBO selectPosiList(@RequestBody SelectPositionListBusiReqBO reqBO){
        return selectPositionListBusiService.selectPosiList(reqBO);
    }

    /**
     * 职位详情查询
     */
    @RequestMapping("/selectDetail")
    @ResponseBody
    SelectPositionDetailBusiRspBO selectDetail(@RequestBody SelectPositionDetailBusiReqBO reqBO){
        return selectPositionDetailBusiService.selectDetail(reqBO);
    }
    /**
     * 添加评价
     */
    @RequestMapping("/insertEvaPo")
    @ResponseBody
    public InsertEvaluatePositionBusiRspBO insertEvaPo(@RequestBody InsertEvaluatePositionBusiReqBO reqBO){
        return insertEvaluatePositionBusiService.insertEvaPo(reqBO);
    }

    /**
     * 查找推荐职位
     */
    @RequestMapping("/selectRecommedList")
    @ResponseBody
    GraduationSelectRecommendListBusiRspBO selectRecommedList(@RequestBody GraduationSelectRecommendListBusiReqBO reqBO){
        return graduationSelectRecommendListBusiService.selectRecommedList(reqBO);
    }

    /**
     * 查看用户评价个数
     */
    @RequestMapping("/selectEvaluate")
    @ResponseBody
    GraduationSelectEvaluateBusiRspBO selectEvaluate(@RequestBody GraduationSelectEvaluateBusiReqBO reqBO){
        return graduationSelectEvaluateBusiService.selectEvaluate(reqBO);
    }

    /**
     * 查看已评价职位信息
     */
    @RequestMapping("/selectEvaluateList")
    @ResponseBody
    GraduationSelectEvaluatedListBusiRspBO selectEvaluateList(@RequestBody GraduationSelectEvaluatedListBusiReqBO reqBO){
        return graduationSelectEvaluatedListBusiService.selectEvaluateList(reqBO);
    }

    /**
     * 投递
     */
    @RequestMapping("/insertDeliver")
    @ResponseBody
    GraduationInsertDeliverBusiRspBO insertDeliver(@RequestBody GraduationInsertDeliverBusiReqBO reqBO){
        return graduationInsertDeliverBusiService.insertDeliver(reqBO);
    }
    /**
     * 删除
     */
    @RequestMapping("/deleteDeliver")
    @ResponseBody
    GraduationDeleteDeliverBusiRspBO deleteDeliver(@RequestBody GraduationDeleteDeliverBusiReqBO reqBO){
        return graduationDeleteDeliverBusiService.deleteDeliver(reqBO);
    }
    /**
     * 列表查询
     */
    @RequestMapping("/selectDeliverList")
    @ResponseBody
    GraduationSelectDeliverBusiRspBO selectDeliverList(@RequestBody GraduationSelectDeliverBusiReqBO reqBO){
        return graduationSelectDeliverBusiService.selectDeliverList(reqBO);
    }
    @RequestMapping("/selectDeliverCount")
    @ResponseBody
    GraduationSelectEvaluateBusiRspBO selectDeliverCount(@RequestBody GraduationSelectDeliverBusiReqBO reqBO){
        GraduationSelectEvaluateBusiRspBO rspBO = new GraduationSelectEvaluateBusiRspBO();
        DeliverPositionPO deliverPositionPO = new DeliverPositionPO();
        deliverPositionPO.setUserId(reqBO.getUserId());
        rspBO.setCount(positionMapper.deliverCount(deliverPositionPO));
        rspBO.setCode("20000");
        rspBO.setMessage("查询成功");
        return rspBO;
    }

    /**
     * 基于用户推荐职务列表
     */
    @RequestMapping("/selectCommend")
    @ResponseBody
    PositionRecommendBusiRspBO selectCommend(@RequestBody PositionRecommendBusiReqBO reqBO){
        return positionRecommendBusiService.selectCommend(reqBO);
    }

    /**
     *根据条件推荐职位
     */
    @RequestMapping("/selectPositionCommend")
    @ResponseBody
    GraduationSelectPosionCommendBusiRspBO selectPositionCommend(@RequestBody GraduationSelectPosionCommendBusiReqBO reqBO){
        return graduationSelectPositionCommendBusiService.selectPositionCommend(reqBO);
    }
}
