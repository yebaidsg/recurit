package com.tydic.recruit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tydic.recruit.dao.recurit.*;
import com.tydic.recruit.dao.recurit.po.*;
import com.tydic.recruit.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/back")
public class BackStageController {

    @Autowired
    private PersonalInformationMapper personalInformationMapper;

    @Autowired
    private PositionMapper positionMapper;

    @Autowired
    private EvaluatePositionMapper evaluatePositionMapper;

    @Autowired
    private PersonJobIntentionMapper personJobIntentionMapper;

    @Autowired
    private EducationalExperienceMapper educationalExperienceMapper;

    @Autowired
    private ProfessionalSkillsMapper professionalSkillsMapper;

    @RequestMapping(value = "findByPersonName", method = RequestMethod.GET, params = {"pageNum","pageSize","personName"})
    public AjaxResult findByPersonName(@RequestParam(value = "personName") String personName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<PersonalInformationPO> page = new PageInfo<PersonalInformationPO>(personalInformationMapper.findByPersonName(personName));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "deletePersonInfo", method = RequestMethod.GET, params = {"personId"})
    public AjaxResult deletePersonInfo(@RequestParam(value = "personId") Long personId){
        personalInformationMapper.deletePersonInfo(personId);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "findByJobTitle", method = RequestMethod.GET, params = {"pageNum","pageSize","jobTitle"})
    public AjaxResult findByJobTitle(@RequestParam(value = "jobTitle") String jobTitle, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<PositionPO> page = new PageInfo<PositionPO>(positionMapper.findByJobTitle(jobTitle,null));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }
    @RequestMapping(value = "findByJobTitleNew", method = RequestMethod.GET, params = {"pageNum","pageSize","jobTitle"})
    public AjaxResult findByJobTitleNew(@RequestParam(value = "jobTitle") String jobTitle, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<PositionPO> page = new PageInfo<PositionPO>(positionMapper.findByJobTitle(jobTitle,"1"));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }
    @RequestMapping(value = "deletePosition", method = RequestMethod.GET, params = {"positionId"})
    public AjaxResult deletePosition(@RequestParam(value = "positionId") Integer positionId){
        positionMapper.deletePosition(positionId);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "findEvaluateByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize","userName","jobTitle"})
    public AjaxResult findByJobTitle(@RequestParam(value = "userName") String userName, @RequestParam(value = "jobTitle") String jobTitle, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<EvaluatePositionPO> page = new PageInfo<EvaluatePositionPO>(evaluatePositionMapper.findEvaluateByCondition(userName,jobTitle));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "deleteEvaluate", method = RequestMethod.GET, params = {"evaluateId"})
    public AjaxResult deleteEvaluate(@RequestParam(value = "evaluateId") Long evaluateId){
        evaluatePositionMapper.deleteEvaluate(evaluateId);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "findIntenByPersonName", method = RequestMethod.GET, params = {"pageNum","pageSize","personName"})
    public AjaxResult findIntenByPersonName(@RequestParam(value = "personName") String personName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<PersonJobIntentionPO> page = new PageInfo<PersonJobIntentionPO>(personJobIntentionMapper.findByPersonName(personName));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "deleteInten", method = RequestMethod.GET, params = {"intentionId"})
    public AjaxResult deleteInten(@RequestParam(value = "intentionId") Long intentionId){
        personJobIntentionMapper.delectById(intentionId);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "findEduByUserId", method = RequestMethod.GET, params = {"userId"})
    public AjaxResult findEduByUserId(@RequestParam(value = "userId") Long userId){
        List<EducationalExperiencePO> list = educationalExperienceMapper.findEduByUserId(userId);
        return AjaxResult.success("成功", list);
    }

    @RequestMapping(value = "findSkillByUserId", method = RequestMethod.GET, params = {"userId"})
    public AjaxResult findSkillByUserId(@RequestParam(value = "userId") Long userId){
        List<ProfessionalSkillsPO> list = professionalSkillsMapper.findSkillByUserId(userId);
        return AjaxResult.success("成功", list);
    }

    @RequestMapping(value = "findStatis1", method = RequestMethod.GET)
    public AjaxResult findStatis1(){
        List<StatisPO> list = personalInformationMapper.findStatis1();
        return AjaxResult.success("成功",list);
    }

    @RequestMapping(value = "findStatis2", method = RequestMethod.GET)
    public AjaxResult findStatis2(){
        List<StatisPO> list = personalInformationMapper.findStatis2();
        return AjaxResult.success("成功",list);
    }

}
