package com.tydic.recruit.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.ability.UserService;
import com.ozo.ability.bo.UserBO;
import com.tydic.recruit.dao.po.User;
import com.tydic.recruit.util.AjaxResult;
import com.tydic.recruit.util.GuuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "findAll", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findAllUser(@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<UserBO> page = new PageInfo<UserBO>(userService.findAllUser());
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @PostMapping("/login")
    public AjaxResult findLoginUser(@RequestBody UserBO user){
        System.out.println(user);
        return AjaxResult.success("成功",userService.findLoginUser(user));
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findUserByCondition(@RequestBody UserBO user, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<UserBO> page = new PageInfo<UserBO>(userService.findUserByCondition(user));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "findByUsername", method = RequestMethod.GET, params = {"pageNum","pageSize","username"})
    public AjaxResult findUserByUsername(@RequestParam(value = "username") String username, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize, HttpSession session){
//        Object o = session.getAttribute("user");
//        System.out.println("用户：" +o);
//        String jsonString = JSONObject.toJSONString(o, SerializerFeature.WriteMapNullValue);
//        User user = JSONObject.parseObject(jsonString,User.class);
//        System.out.println("用户ID：" + user.getUserId());
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<UserBO> page = new PageInfo<UserBO>(userService.findUserByUsername(username));
        System.out.println(page.getTotal());
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "findUserByName", method = RequestMethod.GET, params = {"name"})
    public AjaxResult findUserByName(@RequestParam("name") String name){
        return AjaxResult.success("成功",userService.findUserByName(name));
    }

    @PostMapping("/insert")
    public AjaxResult insertUser(@RequestBody UserBO user){
        user.setUserId(GuuidUtil.getUUID());
        userService.insertUser(user);
        System.out.println(user);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"userId"})
    public AjaxResult deleteUser(@RequestParam(value = "userId") Long userId){
        userService.deleteUser(userId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateUser(@RequestBody UserBO user){
        userService.updateUser(user);
        return AjaxResult.success("成功");
    }

}
