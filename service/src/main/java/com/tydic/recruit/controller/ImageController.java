package com.tydic.recruit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.ability.ImageService;
import com.ozo.ability.bo.ImageBO;
import com.tydic.recruit.dao.po.Image;
import com.tydic.recruit.util.AjaxResult;
import com.tydic.recruit.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    private static String baseUrl = "http://localhost:8001/";

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public AjaxResult uploadImage(@RequestParam(value = "connectId") Long connectId, HttpServletRequest request, @RequestParam("file") MultipartFile file) throws IOException{
        String imgPath;
        ImageBO image = new ImageBO();
        //System.out.println(connectId);
        try {
            //ImageUtils为之前添加的工具类
            imgPath = ImageUtil.upload(request, file);
            String imgName = imgPath.replaceAll("image/","");
            if (imgPath != null) {
                // 将上传图片的地址封装到实体类
                image.setConnectId(connectId);
                image.setImageUrl(imgPath);
                image.setImageName(imgName);
                System.out.println(image);
                System.out.println("-----------------图片上传成功！");
            }else{
                System.out.println("-----------------图片上传失败！");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("----------------图片上传失败！");
        }
        if(imageService.findImageByconnectId(connectId) != null){
            String oldImageName = imageService.findImageByconnectId(connectId).getImageName();
            ImageUtil.delFile(oldImageName);
            imageService.updateImageByConnectId(image);
            System.out.println("更新头像成功");
        }else{
            imageService.insertImage(image);
            System.out.println("添加头像成功");
        }
        return AjaxResult.success("成功");
    }

    @GetMapping("/findAll")
    public AjaxResult findAllImage(){
      return AjaxResult.success("成功",imageService.findAllImage()) ;
    }

    @RequestMapping(value = "findByConnectId", method = RequestMethod.GET, params = {"connectId"})
    public AjaxResult findImageByConnectId(@RequestParam(value = "connectId") Long connectId){
        ImageBO image = imageService.findImageByconnectId(connectId);
        if(image != null){
            String url = image.getImageUrl();
            String imgUrl = baseUrl + url;
            image.setImageUrl(imgUrl);
            return AjaxResult.success("成功",image);
        }
        return AjaxResult.success("成功");

    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findImageByCondition(@RequestBody ImageBO image, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<ImageBO> page = new PageInfo<ImageBO>(imageService.findImageByCondition(image));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    @ResponseBody
    public AjaxResult insertImage(@RequestBody ImageBO image) throws IOException {
        imageService.insertImage(image);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{id}")
    public AjaxResult deleteImage(@PathVariable Long id){
        imageService.deleteImage(id);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update")
    public AjaxResult updateImage(@RequestBody ImageBO image){
        imageService.updateImage(image);
        return AjaxResult.success("成功");
    }
}
