package com.tydic.recruit.common.base.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述 doc 入参基类
 * @author tgy
 * @date 2020/5/7 13:05
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@Data
public class ReqBaseBo implements Serializable {

    private static final long serialVersionUID = 7061033644660840982L;
}
