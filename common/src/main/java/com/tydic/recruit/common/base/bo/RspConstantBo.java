package com.tydic.recruit.common.base.bo;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/21 17:48
 */
public class RspConstantBo {

    public static final String SuccRspCode = "20000";
    public static final String SuccRspDesc = "成功";
    public static final String FailRspCode = "88888";
    public static final String FailRspDesc = "失败";
}
