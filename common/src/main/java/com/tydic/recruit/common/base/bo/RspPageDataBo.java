package com.tydic.recruit.common.base.bo;

import com.tydic.utils.generatedoc.annotation.DocField;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述 翻页基类
 *
 * @author tgy
 * @date 2020/3/11 17:06
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@Data
public class RspPageDataBo<T> implements Serializable {

    private static final long serialVersionUID = -7667817030517509074L;

    @DocField(desc = "数据集合")
    private List<T> rows = new ArrayList<>();

    @DocField(desc = "记录总条数")
    private Integer recordsTotal;

    @DocField(desc = "总页数")
    private Integer total;

    @DocField(desc = "当前页")
    private Integer pageNo;

}
