package com.tydic.recruit.common.base.bo;

import com.tydic.utils.generatedoc.annotation.DocField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述 云管平台翻页出参基类
 *
 * @author tgy
 * @date 2020/3/11 17:06
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RspPageBo<T> extends RspBaseBo {

    private static final long serialVersionUID = 8833587100205733035L;

    @DocField(desc = "出参数据对象")
    private RspPageDataBo<T> data = new RspPageDataBo<>();

}
