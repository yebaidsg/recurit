package com.tydic.recruit.common.base.bo;

import com.tydic.utils.generatedoc.annotation.DocField;
import lombok.Data;

import java.io.Serializable;


/**
 * 描述 doc 出参基类
 * @author tgy
 * @date 2020/5/7 13:04
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@Data
public class RspBaseBo implements Serializable {

    private static final long serialVersionUID = 7177172749192136643L;

    @DocField(desc = "返回编码,0000为成功")
    private String code;

    @DocField(desc = "返回描述")
    private String message;

}
