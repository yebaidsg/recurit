package com.tydic.recruit.common.base.bo;

import com.tydic.utils.generatedoc.annotation.DocField;
import lombok.Data;

/**
 * 描述 doc 翻页入参基类
 * @author tgy
 * @date 2020/5/7 13:05
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@Data
public class ReqPageBo extends ReqBaseBo {

    private static final long serialVersionUID = 5725965091147455883L;

    @DocField(desc = "页码, 从1开始")
    private Integer pageNo = 1;

    @DocField(desc = "每页数量")
    private Integer pageSize = 10;

    @DocField(desc = "排序字段名称")
    private String sortName;

    @DocField(desc = "排序顺序，例如：desc asc")
    private String sortOrder;

}
