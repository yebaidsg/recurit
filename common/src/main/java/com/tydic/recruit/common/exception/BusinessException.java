package com.tydic.recruit.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述 自定义异常
 *
 * @author tgy
 * @date 2020/3/11 17:28
 * @Copyright 2019 www.tydic.com Inc. All rights reserved.
 * 注意 本内容仅限于北京天源迪科信息技术有限公司内部传阅，禁止外泄以及用于其他商业目的;
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = -1814155355569144196L;
    private String msgCode;
    private String[] args;

    public BusinessException(String msgId, String message) {
        super(message);
        this.msgCode = msgId;
    }

    public BusinessException(String msgId, String message, Throwable cause) {
        super(message, cause);
        this.args = new String[1];
        this.args[0] = message;
        this.msgCode = msgId;
    }
}
