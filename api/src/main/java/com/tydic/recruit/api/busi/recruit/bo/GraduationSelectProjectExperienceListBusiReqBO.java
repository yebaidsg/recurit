package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectProjectExperienceListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:44
 */
@Data
public class GraduationSelectProjectExperienceListBusiReqBO {
    private Long personId;
}
