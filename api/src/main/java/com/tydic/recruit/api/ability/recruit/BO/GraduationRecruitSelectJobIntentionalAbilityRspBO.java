package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationRecruitSelectJobIntentionalAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:38
 */
@Data
public class GraduationRecruitSelectJobIntentionalAbilityRspBO extends RspBaseBo {
    private Long intentionId;
    private Long userId;
    /**
     * 简历id
     */
    private Long personId;
    /**
     * 一级职位编码
     */
    private String firstPositionCode;
    /**
     * 二级职位编码
     */
    private String secondPositionCode;
    /**
     * 期望城市编码
     */
    private String expectProvinceCode;
    /**
     * 期望城市名称
     */
    private String expectProvinceName;
    /**
     * 期望城市
     */
    private String expectCityCode;
    private String expectCityName;
    /**
     * 期望区县
     */
    private String expectAreaCode;
    private String expectAreaName;
    /**
     * 城镇
     */
    private String expectTownCode;
    private String expectTownName;
    /**
     * 薪资要求
     */
    private String salaryRequirements;
    /**
     * 工作性质
     */
    private String natureOfWork;
}
