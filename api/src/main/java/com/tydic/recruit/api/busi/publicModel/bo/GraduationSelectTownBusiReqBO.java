package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

/**
 * @标题 GraduationSelectTownBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:03
 */
@Data
public class GraduationSelectTownBusiReqBO extends ReqPageBo {
    private String townName;
    private String townCode;
    private String areaCode;
}
