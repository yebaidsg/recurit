package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectWorkExperienceListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:22
 */
@Data
public class GraduationSelectWorkExperienceListBusiReqBO {
    private Long personId;
}
