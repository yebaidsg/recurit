package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectResumeBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:25
 */
@Data
public class GraduationRecruitSelectResumeBusiReqBO {
    private Long personId;
    private Long userId;
}
