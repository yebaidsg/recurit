package com.tydic.recruit.api.busi.python.BO;

import lombok.Data;

/**
 * @author huzb
 * @标题 DateCrawlingHandleBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 17:23
 */
@Data
public class DateCrawlingHandleBusiReqBO {
    /**
     * 网站分类
     * 1 51
     * 2 汇博
     * 3 中华英才
     */
    private Integer netState;
}
