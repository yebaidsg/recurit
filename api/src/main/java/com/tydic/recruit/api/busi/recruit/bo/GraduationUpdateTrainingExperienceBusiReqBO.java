package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationUpdateTrainingExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:55
 */
@Data
public class GraduationUpdateTrainingExperienceBusiReqBO extends ReqBaseBo {
    private Long personId;
    private String trainingName;
    private Date trainingStartTime;
    private Date trainingEndTime;
    private String trainingClass;
    private Long trainingId;
}
