package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectEducationBusiRspBO;

/**
 * @标题 GraduationRecruitSelectEducationBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:14
 */
public interface GraduationRecruitSelectEducationBusiService {
    GraduationRecruitSelectEducationBusiRspBO selectEducation(GraduationRecruitSelectEducationBusiReqBO reqBO);
}
