package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateLanguageBusiRspBO;

/**
 * @标题 GraduationUpdateLanguageBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:37
 */
public interface GraduationUpdateLanguageBusiService {
    GraduationUpdateLanguageBusiRspBO updateLanguage(GraduationUpdateLanguageBusiReqBO reqBO);
}
