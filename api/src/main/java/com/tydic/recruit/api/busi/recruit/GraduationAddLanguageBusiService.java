package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddLanguageBusiRspBO;

/**
 * @标题 GraduationAddLanguageBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:15
 */
public interface GraduationAddLanguageBusiService {
    GraduationAddLanguageBusiRspBO addLanguage(GraduationAddLanguageBusiReqBO reqBO);
}
