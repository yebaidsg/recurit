package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddWorkExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationWorkExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 14:25
 */
@HTServiceAPI
public interface GraduationAddWorkExperienceAbilityService {
    GraduationAddWorkExperienceAbilityRspBO addWorkExperience(GraduationAddWorkExperienceAbilityReqBO reqBO);
}
