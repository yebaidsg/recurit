package com.tydic.recruit.api.ability.recruit.BO;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huzb
 * @标题 TrainingExperienceBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/17 23:56
 */
@Data
public class TrainingExperienceBO implements Serializable {
    private Long personId;
    private String trainingName;
    private Date trainingStartTime;
    private Date trainingEndTime;
    private String trainingClass;
    private String trainingId;
}
