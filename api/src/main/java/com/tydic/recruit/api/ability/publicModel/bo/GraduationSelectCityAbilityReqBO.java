package com.tydic.recruit.api.ability.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

/**
 * @标题 GraduationSelectCityAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:29
 */
@Data
public class GraduationSelectCityAbilityReqBO extends ReqPageBo {
    private String cityCode;
    private String cityName;
    private String provinceCode;
}
