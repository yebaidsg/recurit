package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @标题 GraduationSelectAreaAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:45
 */
@Data
public class GraduationSelectTownAbilityRspBO extends RspPage<TownBO> {
}
