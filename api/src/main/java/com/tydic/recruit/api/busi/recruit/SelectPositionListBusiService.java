package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.SelectPositionListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 SelectPositionListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:43
 */
@HTServiceAPI
public interface SelectPositionListBusiService {
    SelectPositionListBusiRspBO selectPosiList(SelectPositionListBusiReqBO reqBO);
}

