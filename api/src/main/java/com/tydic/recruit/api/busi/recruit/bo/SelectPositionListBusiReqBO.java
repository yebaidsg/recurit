package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

/**
 * @author huzb
 * @标题 SelectPositionListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:43
 */
@Data
public class SelectPositionListBusiReqBO extends ReqPageBo {
    private String jobTitle;
    private String companyName;
    private String[] workPlace;
    private String companyType;
    private String quantity;
    private String education;
    private String lowSalary;
    private String highSalary;
    private String provinceName;
    private String cityName;
    private String areaName;
    private Long userId;
    /**
     * 1 智能匹配
     * 2 求职意向匹配
     * 3 学历匹配
     * 4 薪资要求匹配
     * 5 地区匹配
     */
    private String recommend;
    private Integer positionId;
    private String positionType;
    private String positionResource;
}
