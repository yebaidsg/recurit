package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateDictionaryBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:30
 */
@Data
public class GraduationUpdateDictionaryBusiReqBO extends ReqBaseBo {
    /**
     * 修改 0
     * 删除 1
     */
    private Integer operType;
    private Long dicId;
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
