package com.tydic.recruit.api.ability.recruit.BO;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GradeCertificateAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:18
 */
@Data
public class GradeAddCertificateAbilityReqBO extends ReqBaseBo {
    private Long certificateId;
    private Long personId;
    private String certificateName;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date certificateGetTime;
}
