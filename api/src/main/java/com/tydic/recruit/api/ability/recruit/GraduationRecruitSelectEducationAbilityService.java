package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectEducationAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitSelectEducationAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:12
 */
@HTServiceAPI
public interface GraduationRecruitSelectEducationAbilityService {
    GraduationRecruitSelectEducationAbilityRspBO selectEducation(GraduationRecruitSelectEducationAbilityReqBO reqBO);
}
