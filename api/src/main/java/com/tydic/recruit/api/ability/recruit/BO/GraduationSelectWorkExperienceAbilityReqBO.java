package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectWorkExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:08
 */
@Data
public class GraduationSelectWorkExperienceAbilityReqBO extends ReqBaseBo {
    private Long workId;
    private Long personId;
}
