package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateTrainingExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationAddTrainingExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:43
 */
@HTServiceAPI
public interface GraduationUpdateTrainingExperienceAbilityService {
    GraduationUpdateTrainingExperienceAbilityRspBO updateTraining(GraduationUpdateTrainingExperienceAbilityReqBO reqBO);
}
