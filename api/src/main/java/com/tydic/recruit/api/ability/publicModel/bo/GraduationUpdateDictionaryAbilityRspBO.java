package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @标题 GraduationUpdateDictionaryAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:29
 */
@Data
public class GraduationUpdateDictionaryAbilityRspBO extends RspBaseBO {
}
