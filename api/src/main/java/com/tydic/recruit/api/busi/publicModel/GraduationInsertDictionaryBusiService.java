package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationInsertDictionaryBusiRspBO;
import lombok.Data;

/**
 * @标题 GraduationInsertDictionaryBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:55
 */
public interface GraduationInsertDictionaryBusiService {
    GraduationInsertDictionaryBusiRspBO insertDictionary(GraduationInsertDictionaryBusiReqBO reqBO);
}
