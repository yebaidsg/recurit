package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

/**
 * @标题 GraduationSelectAreaBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:47
 */
@Data
public class GraduationSelectAreaBusiReqBO extends ReqPageBo {
    private String areaCode;
    private String areaName;
    private String cityCode;
}
