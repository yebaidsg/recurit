package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectCityAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectCityAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectCityAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:29
 */
@HTServiceAPI
public interface GraduationSelectCityAbilityService {
    GraduationSelectCityAbilityRspBO selectCityList(GraduationSelectCityAbilityReqBO reqBO);
}
