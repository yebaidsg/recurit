package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateResumeAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitUpdateResumeAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:16
 */
@HTServiceAPI
public interface GraduationRecruitUpdateResumeAbilityService {
    GraduationRecruitUpdateResumeAbilityRspBO updateResume(GraduationRecruitUpdateResumeAbilityReqBO reqBO);
}
