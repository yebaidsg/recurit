package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.api.ability.publicModel.bo.AreaBO;
import com.tydic.recruit.common.base.bo.RspPageBo;
import lombok.Data;

/**
 * @标题 GraduationSelectAreaBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:48
 */
@Data
public class GraduationSelectAreaBusiRspBO extends RspPage<AreaBO> {
}
