package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectProfessionalSkillsListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:29
 */
@Data
public class GraduationSelectProfessionalSkillsListBusiReqBO {
    private Long personId;
}
