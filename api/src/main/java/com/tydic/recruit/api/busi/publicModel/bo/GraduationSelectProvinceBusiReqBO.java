package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

import java.io.Serializable;

/**
 * @标题 GraduationSelectProvinceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:52
 */
@Data
public class GraduationSelectProvinceBusiReqBO extends ReqPageBo {
    private String provinceCode;
    private String provinceName;
}
