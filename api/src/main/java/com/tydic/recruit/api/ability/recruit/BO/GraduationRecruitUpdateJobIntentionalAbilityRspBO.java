package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:32
 */
@Data
public class GraduationRecruitUpdateJobIntentionalAbilityRspBO extends RspBaseBo {
}
