package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectProfessionalSkillsAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:03
 */
@Data
public class GraduationSelectProfessionalSkillsAbilityReqBO extends ReqBaseBo {
    private Long personId;
    private Long skillsId;
}
