package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationRecruitAddJobIntentionalAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:46
 */
@Data
public class GraduationRecruitAddJobIntentionalAbilityRspBO extends RspBaseBo {
}
