package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectDeliverBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectDeliverBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectDeliverBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:06
 */
@HTServiceAPI
public interface GraduationSelectDeliverBusiService {
    GraduationSelectDeliverBusiRspBO selectDeliverList(GraduationSelectDeliverBusiReqBO reqBO);
}
