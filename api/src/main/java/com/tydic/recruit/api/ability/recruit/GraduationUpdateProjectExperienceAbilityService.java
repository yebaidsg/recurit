package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationProjectExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationProjectExperienceAbilityRspBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProjectExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProjectExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationProjectExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:01
 */
@HTServiceAPI
public interface GraduationUpdateProjectExperienceAbilityService {
    GraduationUpdateProjectExperienceAbilityRspBO updateProjectExperience(GraduationUpdateProjectExperienceAbilityReqBO reqBO);
}
