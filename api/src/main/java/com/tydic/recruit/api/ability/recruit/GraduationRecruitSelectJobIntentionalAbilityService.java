package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectJobIntentionalAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitSelectJobIntentionalAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:37
 */
@HTServiceAPI
public interface GraduationRecruitSelectJobIntentionalAbilityService {
    GraduationRecruitSelectJobIntentionalAbilityRspBO selectJob(GraduationRecruitSelectJobIntentionalAbilityReqBO reqBO);
}
