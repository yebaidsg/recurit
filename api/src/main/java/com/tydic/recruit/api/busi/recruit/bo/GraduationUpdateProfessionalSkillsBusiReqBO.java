package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateProfessionalSkillsBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:52
 */
@Data
public class GraduationUpdateProfessionalSkillsBusiReqBO extends ReqBaseBo {
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
    private Long skillsId;
}
