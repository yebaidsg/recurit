package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectProfessionalSkillsListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:29
 */
@HTServiceAPI
public interface GraduationSelectProfessionalSkillsListBusiService {
    GraduationSelectProfessionalSkillsListBusiRspBO selectSkillsList(GraduationSelectProfessionalSkillsListBusiReqBO reqBO);
}
