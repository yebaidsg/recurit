package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationUpdateCertificateBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:33
 */
@Data
public class GraduationUpdateCertificateBusiReqBO extends ReqBaseBo {
    private Long personId;
    private Long certificateId;
    private String certificateName;
    private Date certificateGetTime;
}
