package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectProvinceBusiRspBO;

/**
 * @标题 GraduationSelectProvinceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:52
 */
public interface GraduationSelectProvinceBusiService {
    GraduationSelectProvinceBusiRspBO selectProvince(GraduationSelectProvinceBusiReqBO reqBO);
}
