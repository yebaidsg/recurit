package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectProfessionalSkillsAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:04
 */
@Data
public class GraduationSelectProfessionalSkillsAbilityRspBO extends RspBaseBo {

}
