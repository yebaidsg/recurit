package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateLanguageAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationUpdateLanguageAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:34
 */
@HTServiceAPI
public interface GraduationUpdateLanguageAbilityService {
    GraduationUpdateLanguageAbilityRspBO updateLanguage(GraduationUpdateLanguageAbilityReqBO reqBO);
}
