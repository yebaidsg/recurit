package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectPosionCommendBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectPosionCommendBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectPosionCommendBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 21:42
 */
@HTServiceAPI
public interface GraduationSelectPositionCommendBusiService {
    GraduationSelectPosionCommendBusiRspBO selectPositionCommend(GraduationSelectPosionCommendBusiReqBO reqBO);
}
