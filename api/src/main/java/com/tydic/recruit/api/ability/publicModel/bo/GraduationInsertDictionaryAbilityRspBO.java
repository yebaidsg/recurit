package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @标题 GraduationInsertDictionaryAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:54
 */
@Data
public class GraduationInsertDictionaryAbilityRspBO extends RspBaseBO {
}
