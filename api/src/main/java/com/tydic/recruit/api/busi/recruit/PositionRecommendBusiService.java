package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.PositionRecommendBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.PositionRecommendBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

import java.io.IOException;

/**
 * @author huzb
 * @标题 PositionRecommendBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:19
 */
@HTServiceAPI
public interface PositionRecommendBusiService {
    PositionRecommendBusiRspBO selectCommend(PositionRecommendBusiReqBO reqBO);
}
