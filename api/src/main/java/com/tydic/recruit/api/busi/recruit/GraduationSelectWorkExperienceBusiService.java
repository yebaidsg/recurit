package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceBusiRspBO;

/**
 * @标题 GraduationSelectWorkExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:10
 */
public interface GraduationSelectWorkExperienceBusiService {
    GraduationSelectWorkExperienceBusiRspBO select(GraduationSelectWorkExperienceBusiReqBO reqBO);
}
