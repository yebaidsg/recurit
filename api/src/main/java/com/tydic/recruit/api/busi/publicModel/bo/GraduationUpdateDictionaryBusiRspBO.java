package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @标题 GraduationUpdateDictionaryBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:32
 */
@Data
public class GraduationUpdateDictionaryBusiRspBO extends RspBaseBO {
}
