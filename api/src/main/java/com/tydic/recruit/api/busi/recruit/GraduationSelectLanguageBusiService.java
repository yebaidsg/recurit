package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageBusiRspBO;

/**
 * @标题 GraduationSelectLanguageBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:11
 */
public interface GraduationSelectLanguageBusiService {
    GraduationSelectLanguageBusiRspBO selectLanguage(GraduationSelectLanguageBusiReqBO reqBO);
}
