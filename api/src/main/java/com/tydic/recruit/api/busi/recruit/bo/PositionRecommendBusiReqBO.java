package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 PositionRecommendBusiRepBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:20
 */
@Data
public class PositionRecommendBusiReqBO {
    private Long userId;
}
