package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateWorkExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationUpdateWorkExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:00
 */
@HTServiceAPI
public interface GraduationUpdateWorkExperienceAbilityService {
    GraduationUpdateWorkExperienceAbilityRspBO updateWork(GraduationUpdateWorkExperienceAbilityReqBO reqBO);
}
