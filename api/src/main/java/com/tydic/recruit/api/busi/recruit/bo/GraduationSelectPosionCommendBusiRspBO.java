package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectPosionCommendBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 21:43
 */
@Data
public class GraduationSelectPosionCommendBusiRspBO extends RspPage<PositionBO> {
}
