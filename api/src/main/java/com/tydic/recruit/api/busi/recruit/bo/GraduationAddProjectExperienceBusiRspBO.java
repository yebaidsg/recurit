package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationAddProjectExperienceBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:05
 */
@Data
public class GraduationAddProjectExperienceBusiRspBO extends RspBaseBo {
}
