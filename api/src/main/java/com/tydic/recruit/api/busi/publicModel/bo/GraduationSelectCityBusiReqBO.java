package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

/**
 * @标题 GraduationSelectCityBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:37
 */
@Data
public class GraduationSelectCityBusiReqBO extends ReqPageBo {
    private String cityCode;
    private String cityName;
    private String provinceCode;
}
