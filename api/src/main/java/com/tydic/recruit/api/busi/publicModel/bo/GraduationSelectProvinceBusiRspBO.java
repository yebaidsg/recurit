package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.api.ability.publicModel.bo.ProvinceBO;
import lombok.Data;

/**
 * @标题 GraduationSelectProvinceBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:53
 */
@Data
public class GraduationSelectProvinceBusiRspBO extends RspPage<ProvinceBO> {
}
