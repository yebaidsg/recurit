package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationAddProfessionalSkillsBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:34
 */
@Data
public class GraduationAddProfessionalSkillsBusiReqBO extends ReqBaseBo {
    private Long skillsId;
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
}
