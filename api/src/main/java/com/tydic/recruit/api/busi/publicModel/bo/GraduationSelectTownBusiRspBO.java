package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.api.ability.publicModel.bo.TownBO;
import lombok.Data;

/**
 * @标题 GraduationSelectTownBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:03
 */
@Data
public class GraduationSelectTownBusiRspBO extends RspPage<TownBO> {
}
