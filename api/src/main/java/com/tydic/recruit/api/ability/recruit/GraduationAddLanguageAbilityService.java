package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddLanguageAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationAddLanguageAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:09
 */
@HTServiceAPI
public interface GraduationAddLanguageAbilityService {
    GraduationAddLanguageAbilityRspBO addLanguage(GraduationAddLanguageAbilityReqBO reqBO);
}
