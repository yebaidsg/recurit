package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;
import sun.rmi.runtime.Log;

/**
 * @标题 GraduationAddLanguageAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:09
 */
@Data
public class GraduationAddLanguageAbilityReqBO extends ReqBaseBo {
    private Long languageId;
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
}
