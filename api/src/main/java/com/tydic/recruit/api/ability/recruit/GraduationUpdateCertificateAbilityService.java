package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateCertificateAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationUpdateCertificateAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:30
 */
@HTServiceAPI
public interface GraduationUpdateCertificateAbilityService {
    GraduationUpdateCertificateAbilityRspBO updateCertificate(GraduationUpdateCertificateAbilityReqBO reqBO);
}
