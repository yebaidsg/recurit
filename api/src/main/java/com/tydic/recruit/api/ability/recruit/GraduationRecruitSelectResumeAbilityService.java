package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitSelectResumeAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitSelectResumeAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:23
 */
@HTServiceAPI
public interface GraduationRecruitSelectResumeAbilityService {
    GraduationRecruitSelectResumeAbilityRspBO selectResume(GraduationRecruitSelectResumeAbilityReqBO reqBO);
}
