package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectCertificateBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:41
 */
@Data
public class GraduationSelectCertificateBusiReqBO extends ReqBaseBo {
    private Long personId;
    private Long certificateId;
}
