package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddJobIntentionalAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecuritAddJobIntentionalAbilityService
 * @说明 新增求职意向
 * @作者 胡中宝
 * @时间 2021/4/20 23:37
 */
@HTServiceAPI
public interface GraduationRecruitAddJobIntentionalAbilityService {
    GraduationRecruitAddJobIntentionalAbilityRspBO addJobInternational(GraduationRecruitAddJobIntentionalAbilityReqBO reqBO);
}
