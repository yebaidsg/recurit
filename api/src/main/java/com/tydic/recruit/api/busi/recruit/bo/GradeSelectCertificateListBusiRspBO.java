package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author huzb
 * @标题 GradeSelectCertificateListBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:45
 */
@Data
public class GradeSelectCertificateListBusiRspBO extends RspBaseBO {
    private List<CertificateBO> certificateBOS;
}
