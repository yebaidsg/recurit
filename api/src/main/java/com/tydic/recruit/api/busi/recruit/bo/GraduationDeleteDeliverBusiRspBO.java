package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationDeleteDeliverBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:03
 */
@Data
public class GraduationDeleteDeliverBusiRspBO extends RspBaseBO {
}
