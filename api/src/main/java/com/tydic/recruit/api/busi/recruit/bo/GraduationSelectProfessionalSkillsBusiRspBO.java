package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectProfessionalSkillsBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:06
 */
@Data
public class GraduationSelectProfessionalSkillsBusiRspBO extends RspBaseBo {
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
    private Long skillsId;
}
