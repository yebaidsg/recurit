package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationUpdateDictionaryAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationUpdateDictionaryAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationUpdateDictionaryAbilityReqBO
 * @说明 字典修改
 * @作者 胡中宝
 * @时间 2021/4/4 16:28
 */
@HTServiceAPI
public interface GraduationUpdateDictionaryAbilityService {
    GraduationUpdateDictionaryAbilityRspBO updateDictionary(GraduationUpdateDictionaryAbilityReqBO reqBO);
}
