package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationRecruitAddEducationBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:00
 */
@Data
public class GraduationRecruitAddEducationBusiReqBO {
    private Long eduId;
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
}
