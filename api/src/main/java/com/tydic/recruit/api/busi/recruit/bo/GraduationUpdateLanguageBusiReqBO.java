package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateLanguageBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:37
 */
@Data
public class GraduationUpdateLanguageBusiReqBO extends ReqBaseBo {
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
    private Long languageId;
}
