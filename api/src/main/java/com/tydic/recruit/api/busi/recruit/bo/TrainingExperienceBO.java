package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.util.Date;

/**
 * @author huzb
 * @标题 TrainingExperienceBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:55
 */
@Data
public class TrainingExperienceBO {
    private String trainingId;
    private Long personId;
    private String trainingName;
    private String trainingStartTime;
    private String trainingEndTime;
    private String trainingClass;
}
