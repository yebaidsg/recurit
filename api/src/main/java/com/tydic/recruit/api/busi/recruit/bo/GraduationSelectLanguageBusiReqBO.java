package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectLanguageBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:11
 */
@Data
public class GraduationSelectLanguageBusiReqBO extends ReqBaseBo {
    private Long personId;
    private Long languageId;
}
