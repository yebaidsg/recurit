package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 InsertEvaluatePositionBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/19 16:08
 */
@Data
public class InsertEvaluatePositionBusiReqBO {
    private Integer row;
    private Long evaluateId;
    private Long userId;
    private Integer evaluateStart;
    private Integer positionId;
}
