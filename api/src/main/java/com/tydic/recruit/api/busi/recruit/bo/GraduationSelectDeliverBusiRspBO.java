package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectDeliverBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:07
 */
@Data
public class GraduationSelectDeliverBusiRspBO extends RspPage<PositionBO> {
}
