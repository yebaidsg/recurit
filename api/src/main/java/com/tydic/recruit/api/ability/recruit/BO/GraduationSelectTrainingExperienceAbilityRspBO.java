package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationSelectTrainingExperienceAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 15:58
 */
@Data
public class GraduationSelectTrainingExperienceAbilityRspBO extends RspBaseBo {
    private Long personId;
    private String trainingName;
    private Date trainingStartTime;
    private Date trainingEndTime;
    private String trainingClass;
    private Long trainingId;
}
