package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @标题 InsertPositionBusiServiceReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 19:09
 */
@Data
public class InsertPositionBusiServiceReqBO {
    private String path;
    private String positionResource;
}
