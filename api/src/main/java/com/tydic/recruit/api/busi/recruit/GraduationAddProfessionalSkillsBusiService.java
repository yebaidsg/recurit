package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProfessionalSkillsBusiRspBO;

/**
 * @标题 GraduationAddProfessionalSkillsBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:34
 */
public interface GraduationAddProfessionalSkillsBusiService {
    GraduationAddProfessionalSkillsBusiRspBO addProfession(GraduationAddProfessionalSkillsBusiReqBO reqBO);
}
