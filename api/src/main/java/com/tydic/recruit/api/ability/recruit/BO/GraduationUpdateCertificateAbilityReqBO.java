package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationUpdateCertificateAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:31
 */
@Data
public class GraduationUpdateCertificateAbilityReqBO extends ReqBaseBo {
    private Long personId;
    private Long certificateId;
    private String certificateName;
    private Date certificateGetTime;
}
