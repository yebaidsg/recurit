package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 PositionBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:44
 */
@Data
public class PositionBO {
    private Long positionId;
    private String jobTitle;
    private String companyName;
    private String workPlace;
    private String companyType;
    private String quantity;
    private String education;
    private String lowSalary;
    private String highSalary;
    private String jobInfo;
    private String salary;
    private Integer evaluateStart;
    private Long userId;
    private String experience;
    private Integer deliverId;
    private String deliverStatus;
    private String positionResource;
    private String positionResourceStr;
}
