package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationInsertDeliverBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationInsertDeliverBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationInsertDeliverBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 23:54
 */
@HTServiceAPI
public interface GraduationInsertDeliverBusiService {
    GraduationInsertDeliverBusiRspBO insertDeliver(GraduationInsertDeliverBusiReqBO reqBO);
}
