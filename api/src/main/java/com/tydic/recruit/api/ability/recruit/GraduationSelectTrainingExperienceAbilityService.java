package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectTrainingExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectTrainingExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 15:56
 */
@HTServiceAPI
public interface GraduationSelectTrainingExperienceAbilityService {
    GraduationSelectTrainingExperienceAbilityRspBO selectTraining(GraduationSelectTrainingExperienceAbilityReqBO reqBO);
}
