package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateJobIntentionalBusiRspBO;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:33
 */
public interface GraduationRecruitUpdateJobIntentionalBusiService {
    GraduationRecruitUpdateJobIntentionalBusiRspBO updateJon(GraduationRecruitUpdateJobIntentionalBusiReqBO reqBO);
}
