package com.tydic.recruit.api.busi.python.BO;

import lombok.Data;

/**
 * @author huzb
 * @标题 DateCrawlingBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 16:55
 */
@Data
public class DateCrawlingBusiReqBO {
    /**
     * 页数
     */
    private String end;
    /**
     * 网站分类
     * 1 51
     * 2 汇博
     * 3 中华英才
     */
    private String netState;
}
