package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationSelectProjectExperienceBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:17
 */
@Data
public class GraduationSelectProjectExperienceBusiRspBO extends RspBaseBo {
    private Long personId;
    private String projectName;
    private Date projectStartTime;
    private Date projectEndTime;
    private String projectDescribe;
    private Long projectId;
}
