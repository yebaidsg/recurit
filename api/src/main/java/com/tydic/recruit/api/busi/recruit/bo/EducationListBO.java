package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.util.Date;

/**
 * @author huzb
 * @标题 EducationListBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:14
 */
@Data
public class EducationListBO {
    private String eduSchoolName;
    private String eduBeginTime;
    private String eduEndTime;
    private Long personId;
    private String education;
    private String major;
    private String eduId;
}
