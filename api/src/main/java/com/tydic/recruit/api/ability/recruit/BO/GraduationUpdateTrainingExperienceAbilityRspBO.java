package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateTrainingExperienceAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:45
 */
@Data
public class GraduationUpdateTrainingExperienceAbilityRspBO extends RspBaseBo {
}
