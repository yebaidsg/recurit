package com.tydic.recruit.api.ability.publicModel.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 GraduationSelectProvinceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:50
 */
@Data
public class GraduationSelectProvinceAbilityReqBO implements Serializable {
    /**
     * 省编码
     */
    private String provinceCode;
    private String provinceName;
}
