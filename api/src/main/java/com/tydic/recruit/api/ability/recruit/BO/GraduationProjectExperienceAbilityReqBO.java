package com.tydic.recruit.api.ability.recruit.BO;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationProjectExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:01
 */
@Data
public class GraduationProjectExperienceAbilityReqBO extends ReqBaseBo {
    private Long projectId;
    private Long personId;
    private String projectName;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date projectStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date projectEndTime;
    private String projectDescribe;
}
