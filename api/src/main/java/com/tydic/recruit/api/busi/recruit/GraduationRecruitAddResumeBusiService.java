package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddResumeBusiRspBO;

/**
 * @标题 GraduationRecruitAddResumeBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 22:44
 */
public interface GraduationRecruitAddResumeBusiService {
    GraduationRecruitAddResumeBusiRspBO addResume(GraduationRecruitAddResumeBusiReqBO reqBO);
}
