package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @标题 GraduationSelectProvinceAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:50
 */
@Data
public class GraduationSelectProvinceAbilityRspBO extends RspPage<ProvinceBO> {
}
