package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationInsertDictionaryBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:55
 */
@Data
public class GraduationInsertDictionaryBusiReqBO extends ReqBaseBo {
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
