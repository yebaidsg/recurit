package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @标题 GraduationSelectCityAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:35
 */
@Data
public class GraduationSelectCityAbilityRspBO extends RspPage<CityBO> {

}
