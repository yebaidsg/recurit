package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectAreaBusiRspBO;

/**
 * @标题 GraduationSelectAreaBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:47
 */
public interface GraduationSelectAreaBusiService {
    GraduationSelectAreaBusiRspBO selectAreaList(GraduationSelectAreaBusiReqBO reqBO);
}
