package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationUpdateProjectExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:18
 */
@Data
public class GraduationUpdateProjectExperienceAbilityReqBO extends ReqBaseBo {
    private Long projectId;
    private Long personId;
    private String projectName;
    private Date projectStartTime;
    private Date projectEndTime;
    private String projectDescribe;
}
