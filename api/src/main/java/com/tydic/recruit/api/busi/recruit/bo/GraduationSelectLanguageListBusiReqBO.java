package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectLanguageListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:16
 */
@Data
public class GraduationSelectLanguageListBusiReqBO {
    private Long personId;
}
