package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceBusiRspBO;

/**
 * @标题 GraduationSelectProjectExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:16
 */
public interface GraduationSelectProjectExperienceBusiService {
    GraduationSelectProjectExperienceBusiRspBO selectProject(GraduationSelectProjectExperienceBusiReqBO reqBO);
}
