package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProfessionalSkillsBusiRspBO;

/**
 * @标题 GraduationUpdateProfessionalSkillsBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:52
 */
public interface GraduationUpdateProfessionalSkillsBusiService {
    GraduationUpdateProfessionalSkillsBusiRspBO updateProfession(GraduationUpdateProfessionalSkillsBusiReqBO reqBO);
}
