package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationSelectCertificateAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:40
 */
@Data
public class GraduationSelectCertificateAbilityRspBO extends RspBaseBo {
    private Long personId;
    private Long certificateId;
    private String certificateName;
    private Date certificateGetTime;
}
