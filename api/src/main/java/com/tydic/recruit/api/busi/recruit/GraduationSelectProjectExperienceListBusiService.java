package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProjectExperienceListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectProjectExperienceListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:44
 */
@HTServiceAPI
public interface GraduationSelectProjectExperienceListBusiService {
    GraduationSelectProjectExperienceListBusiRspBO selectProjectList(GraduationSelectProjectExperienceListBusiReqBO reqBO);
}
