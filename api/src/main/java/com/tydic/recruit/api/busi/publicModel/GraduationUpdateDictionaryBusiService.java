package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationUpdateDictionaryBusiRspBO;

/**
 * @标题 GraduationUpdateDictionaryBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 16:30
 */
public interface GraduationUpdateDictionaryBusiService {
    GraduationUpdateDictionaryBusiRspBO updateDictionary(GraduationUpdateDictionaryBusiReqBO reqBO);
}
