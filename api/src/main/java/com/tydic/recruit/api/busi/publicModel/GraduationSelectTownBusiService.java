package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectTownBusiRspBO;

/**
 * @标题 GraduationSelectTownBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:02
 */
public interface GraduationSelectTownBusiService {
    GraduationSelectTownBusiRspBO selectTownList(GraduationSelectTownBusiReqBO reqBO);
}
