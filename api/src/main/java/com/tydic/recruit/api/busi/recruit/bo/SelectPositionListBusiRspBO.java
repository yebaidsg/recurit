package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.common.base.bo.RspPageBo;
import lombok.Data;

import java.util.List;

/**
 * @author huzb
 * @标题 SelectPositionListBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:44
 */
@Data
public class SelectPositionListBusiRspBO extends RspPage<PositionBO> {
}
