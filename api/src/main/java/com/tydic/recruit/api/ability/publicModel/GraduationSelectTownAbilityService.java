package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectTownAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectTownAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectTownAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/13 0:01
 */
@HTServiceAPI
public interface GraduationSelectTownAbilityService {
    GraduationSelectTownAbilityRspBO selectTownList(GraduationSelectTownAbilityReqBO reqBO);
}
