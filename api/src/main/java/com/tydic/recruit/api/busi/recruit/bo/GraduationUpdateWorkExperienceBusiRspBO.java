package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateWorkExperienceBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:03
 */
@Data
public class GraduationUpdateWorkExperienceBusiRspBO extends RspBaseBo {
}
