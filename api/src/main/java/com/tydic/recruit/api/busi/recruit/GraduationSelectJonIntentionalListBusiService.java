package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalListBusiReq;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalListBusiRsp;
import lombok.extern.ohaotian.HTServiceAPI;
import lombok.extern.ohaotian.HTServiceRef;

import java.util.List;

/**
 * @author huzb
 * @标题 GraduationSelectJonIntentionalListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 15:46
 */
@HTServiceAPI
public interface GraduationSelectJonIntentionalListBusiService {
    GraduationRecruitSelectJobIntentionalListBusiRsp selectJonIntentionalList(GraduationRecruitSelectJobIntentionalListBusiReq req);
}
