package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationRecruitSelectEducationAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:13
 */
@Data
public class GraduationRecruitSelectEducationAbilityRspBO extends RspBaseBo {
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
    private Long userId;
    private Long eduId;
}
