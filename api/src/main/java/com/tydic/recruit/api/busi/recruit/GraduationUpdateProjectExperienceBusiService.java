package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateProjectExperienceBusiRspBO;

/**
 * @标题 GraduationUpdateProjectExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:19
 */
public interface GraduationUpdateProjectExperienceBusiService {
    GraduationUpdateProjectExperienceBusiRspBO updateProject(GraduationUpdateProjectExperienceBusiReqBO reqBO);
}
