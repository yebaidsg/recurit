package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddWorkExperienceBusiRspBO;

/**
 * @标题 GraduationAddWorkExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 14:32
 */
public interface GraduationAddWorkExperienceBusiService {
    GraduationAddWorkExperienceBusiRspBO addWorkExperience(GraduationAddWorkExperienceBusiReqBO reqBO);
}
