package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 SelectPositionDetailBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:52
 */
@Data
public class SelectPositionDetailBusiRspBO extends RspBaseBO {
    private Long positionId;
    private String jobTitle;
    private String companyName;
    private String workPlace;
    private String companyType;
    private String quantity;
    private String education;
    private String lowSalary;
    private String highSalary;
    private String jobInfo;
    private String salary;
    private String experience;
    private String positionResource;
    private String positionResourceStr;
}
