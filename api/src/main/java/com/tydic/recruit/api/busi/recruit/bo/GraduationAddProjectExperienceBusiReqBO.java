package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationAddProjectExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:05
 */
@Data
public class GraduationAddProjectExperienceBusiReqBO extends ReqBaseBo {
    private Long projectId;
    private Long personId;
    private String projectName;
    private Date projectStartTime;
    private Date projectEndTime;
    private String projectDescribe;
}
