package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectLanguageBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:12
 */
@Data
public class GraduationSelectLanguageBusiRspBO extends RspBaseBo {
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
    private Long languageId;
}
