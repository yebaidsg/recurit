package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddResumeAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddResumeAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitAddResumeAbilityService
 * @说明 新增简历
 * @作者 胡中宝
 * @时间 2021/4/18 13:44
 */
@HTServiceAPI
public interface GraduationRecruitAddResumeAbilityService {
    GraduationRecruitAddResumeAbilityRspBO addResume(GraduationRecruitAddResumeAbilityReqBO reqBO);
}
