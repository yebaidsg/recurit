package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluatedListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluatedListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluatedListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:22
 */
@HTServiceAPI
public interface GraduationSelectEvaluatedListBusiService {
    GraduationSelectEvaluatedListBusiRspBO selectEvaluateList(GraduationSelectEvaluatedListBusiReqBO reqBO);
}
