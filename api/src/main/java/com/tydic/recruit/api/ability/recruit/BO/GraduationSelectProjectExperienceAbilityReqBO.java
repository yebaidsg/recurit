package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;
import org.omg.CORBA.LongHolder;

/**
 * @标题 GraduationSelectProjectExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:13
 */
@Data
public class GraduationSelectProjectExperienceAbilityReqBO extends ReqBaseBo {
    private Long projectId;
    private Long personId;
}
