package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddEducationBusiRspBO;

/**
 * @标题 GraduationRecruitAddEducationBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:00
 */
public interface GraduationRecruitAddEducationBusiService {
    GraduationRecruitAddEducationBusiRspBO addEducation(GraduationRecruitAddEducationBusiReqBO reqBO);
}
