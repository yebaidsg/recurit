package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 ProfessionalSkillsBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:30
 */
@Data
public class ProfessionalSkillsBO {
    private String skillsId;
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
}
