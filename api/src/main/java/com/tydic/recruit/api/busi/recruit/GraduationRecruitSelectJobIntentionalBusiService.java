package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectJobIntentionalBusiRspBO;

/**
 * @标题 GraduationRecruitSelectJobIntentionalBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:39
 */
public interface GraduationRecruitSelectJobIntentionalBusiService {
    GraduationRecruitSelectJobIntentionalBusiRspBO selectJob(GraduationRecruitSelectJobIntentionalBusiReqBO reqBO);
}
