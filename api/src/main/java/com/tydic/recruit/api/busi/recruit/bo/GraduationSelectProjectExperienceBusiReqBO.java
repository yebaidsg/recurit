package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectProjectExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:16
 */
@Data
public class GraduationSelectProjectExperienceBusiReqBO extends ReqBaseBo {
    private Long projectId;
    private Long personId;
}
