package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GradeAddCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GradeAddCertificateAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GradeCertificateAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:18
 */
@HTServiceAPI
public interface GradeAddCertificateAbilityService {
    GradeAddCertificateAbilityRspBO addCertificate(GradeAddCertificateAbilityReqBO reqBO);
}
