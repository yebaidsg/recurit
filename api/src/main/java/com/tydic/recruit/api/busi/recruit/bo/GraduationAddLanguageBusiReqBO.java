package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @标题 GraduationAddLanguageBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:17
 */
@Data
public class GraduationAddLanguageBusiReqBO {
    private Long languageId;
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
}
