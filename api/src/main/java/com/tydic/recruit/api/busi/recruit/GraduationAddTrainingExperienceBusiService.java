package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddTrainingExperienceBusiRspBO;

/**
 * @标题 GraduationAddTrainingExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:31
 */
public interface GraduationAddTrainingExperienceBusiService {
    GraduationAddTrainingExperienceBusiRspBO addTraining(GraduationAddTrainingExperienceBusiReqBO reqBO);
}
