package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huzb
 * @标题 ProjectExperienceBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:45
 */
@Data
public class ProjectExperienceBO implements Serializable {
    private String projectId;
    private Long personId;
    private String projectName;
    private String projectStartTime;
    private String projectEndTime;
    private String projectDescribe;
}
