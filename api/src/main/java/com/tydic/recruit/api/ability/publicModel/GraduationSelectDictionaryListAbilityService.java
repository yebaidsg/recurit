package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryListAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryListAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectDictionaryListAbilityService
 * @说明 查询字典列表信息
 * @作者 胡中宝
 * @时间 2021/4/4 15:28
 */
@HTServiceAPI
public interface GraduationSelectDictionaryListAbilityService {
    GraduationSelectDictionaryListAbilityRspBO selectDictionaryList(GraduationSelectDictionaryListAbilityReqBO reqBO);
}
