package com.tydic.recruit.api.ability.recruit.BO;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectJobIntentionalAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:37
 */
@Data
public class GraduationRecruitSelectJobIntentionalAbilityReqBO {
    private Long intentionId;
    private Long userId;
    /**
     * 简历id
     */
    private Long personId;
}
