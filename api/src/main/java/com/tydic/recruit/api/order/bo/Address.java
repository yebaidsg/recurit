package com.tydic.recruit.api.order.bo;

import lombok.Data;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/20 11:06
 */

@Data
public class Address {
    private Integer aid;
    private String location;
    private String aname;
    private String aphone;
    private User user;
}
