package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectTrainingExperienceListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:54
 */
@Data
public class GraduationSelectTrainingExperienceListBusiReqBO extends ReqBaseBo {
    private Long personId;
}
