package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitAddJobIntentionalBusiRspBO;

/**
 * @标题 GraduationRecruitAddJobIntentionalBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:48
 */
public interface GraduationRecruitAddJobIntentionalBusiService {
    GraduationRecruitAddJobIntentionalBusiRspBO addJobInternational(GraduationRecruitAddJobIntentionalBusiReqBO reqBO);
}
