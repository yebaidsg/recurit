package com.tydic.recruit.api.ability.publicModel.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 CityBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:35
 */
@Data
public class CityBO implements Serializable {
    private String cityCode;
    private String cityName;
    private String provinceCode;
}
