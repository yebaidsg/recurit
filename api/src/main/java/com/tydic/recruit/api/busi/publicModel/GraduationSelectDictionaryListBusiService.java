package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectDictionaryListBusiRspBO;

/**
 * @标题 GraduationSelectDictionaryListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:32
 */
public interface GraduationSelectDictionaryListBusiService {
    GraduationSelectDictionaryListBusiRspBO selectDictionaryList(GraduationSelectDictionaryListBusiReqBO reqBO);
}
