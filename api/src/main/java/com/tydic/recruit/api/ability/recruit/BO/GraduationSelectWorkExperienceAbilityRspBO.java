package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationSelectWorkExperienceAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:09
 */
@Data
public class GraduationSelectWorkExperienceAbilityRspBO extends RspBaseBo {
    private Long workId;
    /**
     * 简历编号
     */
    private Long personId;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 所属行业一级编码
     */
    private String workFirstIndustryCode;
    /**
     * 一级名称
     */
    private String workFirstIndustryName;
    /**
     * 所属行业二级编码
     */
    private String workSecondIndustryCode;
    /**
     * 所属行业二级名称
     */
    private String workSecondIndustryName;
    /**
     * 职位名称
     */
    private String titlePosition;
    /**
     * 入职时间
     */
    private Date entryTime;
    /**
     * 离职时间
     */
    private Date departureTime;
    /**
     * 当前月星
     */
    private String monthlySalary;
    /**
     * 工作描述
     */
    private String jonDescription;
}
