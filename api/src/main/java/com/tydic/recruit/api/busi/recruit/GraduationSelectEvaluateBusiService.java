package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEvaluateBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluateBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:09
 */
@HTServiceAPI
public interface GraduationSelectEvaluateBusiService {
    GraduationSelectEvaluateBusiRspBO selectEvaluate(GraduationSelectEvaluateBusiReqBO reqBO);
}
