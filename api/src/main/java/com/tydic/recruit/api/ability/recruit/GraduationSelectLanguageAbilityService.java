package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectLanguageAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectLanguageAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectLanguageAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:05
 */
@HTServiceAPI
public interface GraduationSelectLanguageAbilityService {
    GraduationSelectLanguageAbilityRspBO selectLanguage(GraduationSelectLanguageAbilityReqBO reqBO);
}
