package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GradeSelectCertificateListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeSelectCertificateListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GradeSelectCertificateListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:44
 */
@HTServiceAPI
public interface GradeSelectCertificateListBusiService {
    GradeSelectCertificateListBusiRspBO selectCertificateList(GradeSelectCertificateListBusiReqBO reqBO);
}
