package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectJobIntentionalBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:39
 */
@Data
public class GraduationRecruitSelectJobIntentionalBusiReqBO {
    private Long intentionId;
    private Long userId;
    /**
     * 简历id
     */
    private Long personId;
}
