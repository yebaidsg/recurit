package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationAddProjectExperienceBusiRspBO;

/**
 * @标题 GraduationAddProjectExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 18:04
 */
public interface GraduationAddProjectExperienceBusiService {
    GraduationAddProjectExperienceBusiRspBO addProjectExperience(GraduationAddProjectExperienceBusiReqBO reqBO);
}
