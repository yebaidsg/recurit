package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectProvinceAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectProvinceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectProvinceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:52
 */
@HTServiceAPI
public interface GraduationSelectProvinceAbilityService {
    GraduationSelectProvinceAbilityRspBO SelectProvince(GraduationSelectProvinceAbilityReqBO reqBO);
}
