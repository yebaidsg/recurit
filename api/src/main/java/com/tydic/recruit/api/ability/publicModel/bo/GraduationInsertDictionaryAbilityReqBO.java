package com.tydic.recruit.api.ability.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationInsertDictionaryAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:53
 */
@Data
public class GraduationInsertDictionaryAbilityReqBO extends ReqBaseBo {
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
