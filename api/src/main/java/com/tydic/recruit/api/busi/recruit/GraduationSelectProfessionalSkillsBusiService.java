package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectProfessionalSkillsBusiRspBO;

/**
 * @标题 GraduationSelectProfessionalSkillsBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:05
 */
public interface GraduationSelectProfessionalSkillsBusiService {
    GraduationSelectProfessionalSkillsBusiRspBO selectProfession(GraduationSelectProfessionalSkillsBusiReqBO reqBO);
}
