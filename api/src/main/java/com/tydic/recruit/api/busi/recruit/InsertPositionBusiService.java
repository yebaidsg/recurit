package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertPositionBusiServiceRspBO;

/**
 * @author huzb
 * @标题 InsertPositionBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 19:09
 */
public interface InsertPositionBusiService {
    InsertPositionBusiServiceRspBO insert(InsertPositionBusiServiceReqBO reqBO);
}
