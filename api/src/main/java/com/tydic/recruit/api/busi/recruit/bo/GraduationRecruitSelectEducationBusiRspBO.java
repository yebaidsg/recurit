package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationRecruitSelectEducationBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:14
 */
@Data
public class GraduationRecruitSelectEducationBusiRspBO extends RspBaseBo {
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
    private Long userId;
    private Long eduId;
}
