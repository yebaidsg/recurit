package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationDeleteDeliverBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationDeleteDeliverBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationDeleteDeliverBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:02
 */
@HTServiceAPI
public interface GraduationDeleteDeliverBusiService {
    GraduationDeleteDeliverBusiRspBO deleteDeliver(GraduationDeleteDeliverBusiReqBO reqBO);
}
