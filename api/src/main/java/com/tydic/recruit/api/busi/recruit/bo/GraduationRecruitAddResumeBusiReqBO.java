package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @标题 GraduationRecruitAddResumeBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 22:44
 */
@Data
public class GraduationRecruitAddResumeBusiReqBO implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 姓名
     */
    private String personName;
    /**
     * 性别
     */
    private String personSex;
    /**
     * 当前状态
     */
    private String personCurrentState;
    /**
     * 出生年月
     */
    private Date personBirthday;
    /**
     * 工作时间
     */
    private Date personGoWorkTime;
    /**
     * 省份
     */
    private String personProvinceCode;
    private String personProvinceName;
    /**
     * 市
     */
    private String personCityCode;
    private String personCityName;
    /**
     * 地区
     */
    private String personAreaCode;
    private String personAreaName;
    /**
     * 镇
     */
    private String personTownCode;
    private String personTownName;
    /**
     * 居住地
     */
    private String personAddressProvinceCode;
    private String personAddressProvinceName;
    private String personAddressCityCode;
    private String personAddressCityName;
    private String personAddressAreaCode;
    private String personAddressAreaName;
    private String personAddressTownCode;
    private String personAddressTownName;
    /**
     * 政治面貌
     */
    private String personPoliticalOutLook;
    /**
     * 联系方式
     */
    private String personContactNumber;
    /**
     * 邮箱
     */
    private String personEmail;
    private String[] address;
    private String[] juzhu;
}
