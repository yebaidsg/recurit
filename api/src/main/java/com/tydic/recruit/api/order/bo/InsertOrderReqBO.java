package com.tydic.recruit.api.order.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/20 14:19
 */
@Data
public class InsertOrderReqBO extends ReqInfo {
    private Integer nid;
    private MainOrder mainOrder;
    private State state;
    private Integer gid;
    private String gname;
    private String gprice;
    private Integer gnumber;
    private String gallprice;
    private String ncreatetime;
    private String nendtime;
    private String nremarks;
    private Address address;
}
