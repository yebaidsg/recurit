package com.tydic.recruit.api.busi.BO;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/22 0:13
 */
@Data
public class AllOrderRspBO implements Serializable {
    private String username;
    private Integer oid;
    private String sname;
    private Integer gid;
    private String gname;
    private Double gprice;
    private Integer gnumber;
    private Double gallprice;
    private String ocreatetime;
    private Double oallmoney;
    private String aname;
    private String aphone;
    private String location;
}
