package com.tydic.recruit.api.busi.publicModel.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationInsertDictionaryBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:56
 */
@Data
public class GraduationInsertDictionaryBusiRspBO extends RspBaseBo {
}
