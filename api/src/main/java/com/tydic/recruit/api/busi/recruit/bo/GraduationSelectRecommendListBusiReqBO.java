package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectRecommendListBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/22 14:13
 */
@Data
public class GraduationSelectRecommendListBusiReqBO extends ReqInfo {
}
