package com.tydic.recruit.api.ability.recruit.BO;

import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationRecruitUpdateEducationAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:06
 */
@Data
public class GraduationRecruitUpdateEducationAbilityReqBO {
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
    private Long eduId;
}
