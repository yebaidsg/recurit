package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateJobIntentionalAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateJobIntentionalAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalAbilityService
 * @说明 修改求职意向
 * @作者 胡中宝
 * @时间 2021/4/22 0:31
 */
@HTServiceAPI
public interface GraduationRecruitUpdateJobIntentionalAbilityService {
    GraduationRecruitUpdateJobIntentionalAbilityRspBO updateJob(GraduationRecruitUpdateJobIntentionalAbilityReqBO reqBO);
}
