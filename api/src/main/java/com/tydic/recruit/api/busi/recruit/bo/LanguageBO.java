package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 LanguageBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:17
 */
@Data
public class LanguageBO {
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
    private String languageId;
}
