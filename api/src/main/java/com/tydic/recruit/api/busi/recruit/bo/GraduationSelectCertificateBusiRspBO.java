package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationSelectCertificateBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:42
 */
@Data
public class GraduationSelectCertificateBusiRspBO extends RspBaseBo {
    private Long personId;
    private Long certificateId;
    private String certificateName;
    private Date certificateGetTime;
}
