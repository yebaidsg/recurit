package com.tydic.recruit.api.busi.python.BO;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 DateCrawlingBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 16:57
 */
@Data
public class DateCrawlingBusiRspBO extends RspBaseBO {
}
