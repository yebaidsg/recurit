package com.tydic.recruit.api.busi.publicModel;

import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiReqBO;
import com.tydic.recruit.api.busi.publicModel.bo.GraduationSelectCityBusiRspBO;

/**
 * @标题 GraduationSelectCityBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:37
 */
public interface GraduationSelectCityBusiService {
    GraduationSelectCityBusiRspBO selectCityList(GraduationSelectCityBusiReqBO reqBO);
}
