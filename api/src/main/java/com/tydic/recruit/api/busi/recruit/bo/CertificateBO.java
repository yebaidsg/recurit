package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huzb
 * @标题 CertificateBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:46
 */
@Data
public class CertificateBO implements Serializable {
    private String certificateId;
    private Long personId;
    private String certificateName;
    private String certificateGetTime;
}
