package com.tydic.recruit.api.ability.publicModel.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 AreaBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:44
 */
@Data
public class AreaBO implements Serializable {
    private String areaCode;
    private String areaName;
    private String cityCode;
}
