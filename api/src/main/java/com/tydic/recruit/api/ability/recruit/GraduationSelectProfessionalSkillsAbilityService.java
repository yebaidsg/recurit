package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectProfessionalSkillsAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectProfessionalSkillsAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 18:03
 */
@HTServiceAPI
public interface GraduationSelectProfessionalSkillsAbilityService {
    GraduationSelectProfessionalSkillsAbilityRspBO selectProfession(GraduationSelectProfessionalSkillsAbilityReqBO reqBO);
}
