package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateTrainingExperienceBusiRspBO;

/**
 * @标题 GraduationUpdateTrainingExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:55
 */
public interface GraduationUpdateTrainingExperienceBusiService {
    GraduationUpdateTrainingExperienceBusiRspBO updateTraining(GraduationUpdateTrainingExperienceBusiReqBO reqBO);
}
