package com.tydic.recruit.api.ability.publicModel.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 GraduationSelectDictionaryBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:30
 */
@Data
public class GraduationSelectDictionaryBO implements Serializable {
    private Long dicId;
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
