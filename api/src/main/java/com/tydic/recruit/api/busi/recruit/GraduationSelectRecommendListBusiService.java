package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectRecommendListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectRecommendListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectRecommendListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/22 14:13
 */
@HTServiceAPI
public interface GraduationSelectRecommendListBusiService {
    GraduationSelectRecommendListBusiRspBO selectRecommedList(GraduationSelectRecommendListBusiReqBO reqBO);
}
