package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectLanguageAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:05
 */
@Data
public class GraduationSelectLanguageAbilityReqBO extends ReqBaseBo {
    private Long personId;
    private Long languageId;
}
