package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 InsertPositionBusiServiceRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/2 19:09
 */
@Data
public class InsertPositionBusiServiceRspBO extends RspBaseBO {
}
