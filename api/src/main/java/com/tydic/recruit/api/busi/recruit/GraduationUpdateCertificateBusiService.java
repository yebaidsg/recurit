package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateCertificateBusiRspBO;

/**
 * @标题 GraduationUpdateCertificateBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:33
 */
public interface GraduationUpdateCertificateBusiService {
    GraduationUpdateCertificateBusiRspBO updateCertificate(GraduationUpdateCertificateBusiReqBO reqBO);
}
