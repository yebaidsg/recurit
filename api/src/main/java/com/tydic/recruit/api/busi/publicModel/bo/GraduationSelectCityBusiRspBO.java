package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.api.ability.publicModel.bo.CityBO;
import lombok.Data;

/**
 * @标题 GraduationSelectCityBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:38
 */
@Data
public class GraduationSelectCityBusiRspBO extends RspPage<CityBO> {
}
