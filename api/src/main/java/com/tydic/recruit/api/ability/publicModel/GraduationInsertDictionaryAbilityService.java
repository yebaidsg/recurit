package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationInsertDictionaryAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationInsertDictionaryAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationInsertDictionaryAbilityService
 * @说明 添加字典
 * @作者 胡中宝
 * @时间 2021/4/4 15:53
 */
@HTServiceAPI
public interface GraduationInsertDictionaryAbilityService {
    GraduationInsertDictionaryAbilityRspBO insertDictionary(GraduationInsertDictionaryAbilityReqBO reqBO);
}
