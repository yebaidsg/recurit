package com.tydic.recruit.api.busi.python;

import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 DateCrawlingBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 16:54
 */
@HTServiceAPI
public interface DateCrawlingBusiService {
    DateCrawlingBusiRspBO getCrawling(DateCrawlingBusiReqBO reqBO);
}
