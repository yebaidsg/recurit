package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectEducationBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:14
 */
@Data
public class GraduationRecruitSelectEducationBusiReqBO {
    private Long userId;
    private Long personId;
    private Long eduId;
}
