package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectLanguageListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;
import lombok.extern.ohaotian.HTServiceRef;

/**
 * @author huzb
 * @标题 GraduationSelectLanguageListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 17:16
 */
@HTServiceAPI
public interface GraduationSelectLanguageListBusiService {
    GraduationSelectLanguageListBusiRspBO selectLanList(GraduationSelectLanguageListBusiReqBO reqBO);
}
