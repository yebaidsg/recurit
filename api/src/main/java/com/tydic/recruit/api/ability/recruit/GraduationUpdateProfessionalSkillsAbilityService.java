package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationUpdateProfessionalSkillsAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationUpdateProfessionalSkillsBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:49
 */
@HTServiceAPI
public interface GraduationUpdateProfessionalSkillsAbilityService {
    GraduationUpdateProfessionalSkillsAbilityRspBO updateProfession(GraduationUpdateProfessionalSkillsAbilityReqBO reqBO);
}
