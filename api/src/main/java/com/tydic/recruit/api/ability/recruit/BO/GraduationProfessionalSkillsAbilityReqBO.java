package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationProfessionalSkillsAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:30
 */
@Data
public class GraduationProfessionalSkillsAbilityReqBO extends ReqBaseBo {
    private Long skillsId;
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
}
