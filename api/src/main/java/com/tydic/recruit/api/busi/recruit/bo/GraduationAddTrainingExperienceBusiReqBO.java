package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationAddTrainingExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:32
 */
@Data
public class GraduationAddTrainingExperienceBusiReqBO extends ReqBaseBo {
    private Long trainingId;
    private Long personId;
    private String trainingName;
    private Date trainingStartTime;
    private Date trainingEndTime;
    private String trainingClass;
}
