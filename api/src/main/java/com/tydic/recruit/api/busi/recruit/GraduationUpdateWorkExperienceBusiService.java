package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationUpdateWorkExperienceBusiRspBO;

/**
 * @标题 GraduationUpdateWorkExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:03
 */
public interface GraduationUpdateWorkExperienceBusiService {
    GraduationUpdateWorkExperienceBusiRspBO updateWork(GraduationUpdateWorkExperienceBusiReqBO reqBO);
}
