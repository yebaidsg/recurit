package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @author huzb
 * @标题 SelectPositionDetailBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:52
 */
@Data
public class SelectPositionDetailBusiReqBO extends ReqBaseBo {
    private Long positionId;
}

