package com.tydic.recruit.api.ability.recruit.BO;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectResumeAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:23
 */
@Data
public class GraduationRecruitSelectResumeAbilityReqBO {
    private Long personId;
    private Long userId;
}
