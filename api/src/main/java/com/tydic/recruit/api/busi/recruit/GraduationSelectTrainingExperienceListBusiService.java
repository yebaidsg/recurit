package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;
import lombok.extern.ohaotian.HTServiceRef;

/**
 * @author huzb
 * @标题 GraduationSelectTrainingExperienceListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:54
 */
@HTServiceAPI
public interface GraduationSelectTrainingExperienceListBusiService {
    GraduationSelectTrainingExperienceListBusiRspBO selectList(GraduationSelectTrainingExperienceListBusiReqBO reqBO);
}
