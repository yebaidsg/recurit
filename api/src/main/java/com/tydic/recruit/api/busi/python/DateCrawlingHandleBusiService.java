package com.tydic.recruit.api.busi.python;

import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiReqBO;
import com.tydic.recruit.api.busi.python.BO.DateCrawlingHandleBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 DateCrawlingHandleBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 17:22
 */
@HTServiceAPI
public interface DateCrawlingHandleBusiService {
    DateCrawlingHandleBusiRspBO dataHandle(DateCrawlingHandleBusiReqBO reqBO);
}
