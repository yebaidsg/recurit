package com.tydic.recruit.api.busi.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectDictionaryBO;
import lombok.Data;

/**
 * @标题 GraduationSelectDictionaryListAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:30
 */
@Data
public class GraduationSelectDictionaryListBusiRspBO extends RspPage<GraduationSelectDictionaryBO> {

}
