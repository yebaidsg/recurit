package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationInsertDeliverBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 23:54
 */
@Data
public class GraduationInsertDeliverBusiReqBO extends ReqInfo {
    private Integer positionId;
    private Long userId;
}
