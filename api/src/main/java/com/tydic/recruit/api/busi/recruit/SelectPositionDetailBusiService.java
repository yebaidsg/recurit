package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.SelectPositionDetailBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.SelectPositionDetailBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 SelectPositionDetailBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/7 13:52
 */
@HTServiceAPI
public interface SelectPositionDetailBusiService {
    SelectPositionDetailBusiRspBO selectDetail(SelectPositionDetailBusiReqBO reqBO);
}
