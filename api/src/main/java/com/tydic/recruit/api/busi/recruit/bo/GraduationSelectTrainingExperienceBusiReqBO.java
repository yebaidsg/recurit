package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectTrainingExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:00
 */
@Data
public class GraduationSelectTrainingExperienceBusiReqBO extends ReqBaseBo {
    private Long personId;
    private Long trainingId;
}
