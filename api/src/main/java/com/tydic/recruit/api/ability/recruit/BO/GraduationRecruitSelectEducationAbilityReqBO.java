package com.tydic.recruit.api.ability.recruit.BO;

import lombok.Data;

/**
 * @标题 GraduationRecruitSelectEducationAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:12
 */
@Data
public class GraduationRecruitSelectEducationAbilityReqBO {
    private Long userId;
    private Long personId;
    private Long eduId;
}
