package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectTrainingExperienceBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectTrainingExperienceBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 16:00
 */
@HTServiceAPI
public interface GraduationSelectTrainingExperienceBusiService {
    GraduationSelectTrainingExperienceBusiRspBO selectTrainingList(GraduationSelectTrainingExperienceBusiReqBO reqBO);
}
