package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectCertificateBusiRspBO;

/**
 * @标题 GraduationSelectCertificateBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:41
 */
public interface GraduationSelectCertificateBusiService {
    GraduationSelectCertificateBusiRspBO selectCertificate(GraduationSelectCertificateBusiReqBO reqBO);
}
