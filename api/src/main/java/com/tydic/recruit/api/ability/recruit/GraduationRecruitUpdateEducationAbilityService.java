package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitUpdateEducationAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitUpdateEducationAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:05
 */
@HTServiceAPI
public interface GraduationRecruitUpdateEducationAbilityService {
    GraduationRecruitUpdateEducationAbilityRspBO updateEducation(GraduationRecruitUpdateEducationAbilityReqBO reqBO);
}
