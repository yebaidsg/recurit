package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectWorkExperienceBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:10
 */
@Data
public class GraduationSelectWorkExperienceBusiReqBO extends ReqBaseBo {
    private Long workId;
    private Long personId;
}
