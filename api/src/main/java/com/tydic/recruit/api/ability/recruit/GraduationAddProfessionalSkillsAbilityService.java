package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationProfessionalSkillsAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationProfessionalSkillsAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationProfessionalSkillsAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:30
 */
@HTServiceAPI
public interface GraduationAddProfessionalSkillsAbilityService {
    GraduationProfessionalSkillsAbilityRspBO addProfession(GraduationProfessionalSkillsAbilityReqBO reqBO);
}
