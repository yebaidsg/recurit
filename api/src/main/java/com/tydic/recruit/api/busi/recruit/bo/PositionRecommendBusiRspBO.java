package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @author huzb
 * @标题 PositionRecommendBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:21
 */
@Data
public class PositionRecommendBusiRspBO extends RspPage<PositionBO> {
}
