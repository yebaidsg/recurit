package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GradeAddCertificateBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:21
 */
@Data
public class GradeAddCertificateBusiReqBO extends ReqBaseBo {
    private Long certificateId;
    private Long personId;
    private String certificateName;
    private Date certificateGetTime;
}
