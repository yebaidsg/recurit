package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddEducationAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationRecruitAddEducationAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationRecruitAddEducationAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:57
 */
@HTServiceAPI
public interface GraduationRecruitAddEducationAbilityService {
    GraduationRecruitAddEducationAbilityRspBO addEducation(GraduationRecruitAddEducationAbilityReqBO reqBO);
}
