package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectPosionCommendBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 21:42
 */
@Data
public class GraduationSelectPosionCommendBusiReqBO extends ReqInfo {
    /**
     * 1 职位
     * 2 地区
     * 3 职位加地区  最佳职位
     */
    private String status;
    private Long userId;
}
