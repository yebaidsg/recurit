package com.tydic.recruit.api.ability.publicModel;

import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectAreaAbilityReqBO;
import com.tydic.recruit.api.ability.publicModel.bo.GraduationSelectAreaAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectAreaAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 23:46
 */
@HTServiceAPI
public interface GraduationSelectAreaAbilityService {
    GraduationSelectAreaAbilityRspBO selectAreaList(GraduationSelectAreaAbilityReqBO reqBO);
}
