package com.tydic.recruit.api.ability.recruit.BO;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationRecruitAddEducationAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:58
 */
@Data
public class GraduationRecruitAddEducationAbilityReqBO {
    private Long eduId;

    private String eduSchoolName;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date eduBeginTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
}
