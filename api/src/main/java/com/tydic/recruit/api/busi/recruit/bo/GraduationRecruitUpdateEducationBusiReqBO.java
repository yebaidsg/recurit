package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @标题 GraduationRecruitUpdateEducationBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:07
 */
@Data
public class GraduationRecruitUpdateEducationBusiReqBO implements Serializable {
    private String eduSchoolName;
    private Date eduBeginTime;
    private Date eduEndTime;
    private Long personId;
    private String education;
    private String major;
    private Long eduId;
}
