package com.tydic.recruit.api.order.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/20 11:07
 */
@Data
public class InsertOrderMainReqBO extends ReqInfo {
    private Integer oid;
    private User user;
    private String ocreatetime;
    private String oendtime;
    private String oremarks;
    private String oallmoney;
    private State state;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOcreatetime() {
        return ocreatetime;
    }

    public void setOcreatetime(String ocreatetime) {
        this.ocreatetime = ocreatetime;
    }

    public String getOendtime() {
        return oendtime;
    }

    public void setOendtime(String oendtime) {
        this.oendtime = oendtime;
    }

    public String getOremarks() {
        return oremarks;
    }

    public void setOremarks(String oremarks) {
        this.oremarks = oremarks;
    }

    public String getOallmoney() {
        return oallmoney;
    }

    public void setOallmoney(String oallmoney) {
        this.oallmoney = oallmoney;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
