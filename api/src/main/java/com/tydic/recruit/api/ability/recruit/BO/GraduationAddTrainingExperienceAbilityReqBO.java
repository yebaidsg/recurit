package com.tydic.recruit.api.ability.recruit.BO;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

import java.util.Date;

/**
 * @标题 GraduationAddTrainingExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:29
 */
@Data
public class GraduationAddTrainingExperienceAbilityReqBO extends ReqBaseBo {
    private Long trainingId;
    private Long personId;
    private String trainingName;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date trainingStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date trainingEndTime;
    private String trainingClass;
}
