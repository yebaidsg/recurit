package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.InsertEvaluatePositionBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.InsertEvaluatePositionBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 InsertEvaluatePositionBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/19 16:08
 */
@HTServiceAPI
public interface InsertEvaluatePositionBusiService {
    InsertEvaluatePositionBusiRspBO insertEvaPo(InsertEvaluatePositionBusiReqBO reqBO);
}
