package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluateBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:09
 */
@Data
public class GraduationSelectEvaluateBusiRspBO extends RspBaseBO {
    private Integer count;
}
