package com.tydic.recruit.api.busi.python.BO;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import lombok.Data;

/**
 * @author huzb
 * @标题 DateCrawlingHandleBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/26 17:23
 */
@Data
public class DateCrawlingHandleBusiRspBO {
    private Integer code;
    private String message;
}
