package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectEducationListBusiReqBo
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:14
 */
@Data
public class GraduationSelectEducationListBusiReqBo extends ReqBaseBo {
    private Long personId;
}
