package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationAddTrainingExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationAddTrainingExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationAddTrainingExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 14:29
 */
@HTServiceAPI
public interface GraduationAddTrainingExperienceAbilityService {
    GraduationAddTrainingExperienceAbilityRspBO addTraining(GraduationAddTrainingExperienceAbilityReqBO reqBO);
}
