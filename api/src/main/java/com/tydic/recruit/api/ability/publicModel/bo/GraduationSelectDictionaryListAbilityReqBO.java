package com.tydic.recruit.api.ability.publicModel.bo;

import com.tydic.recruit.common.base.bo.ReqPageBo;
import lombok.Data;

import java.io.Serializable;

/**
 * @标题 GraduationSelectDictionaryListAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:28
 */
@Data
public class GraduationSelectDictionaryListAbilityReqBO extends ReqPageBo {
    private String code;
    private String pCode;
    private String title;
    private String describe;
    private String status;
}
