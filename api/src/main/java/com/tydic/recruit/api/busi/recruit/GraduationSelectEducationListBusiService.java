package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEducationListBusiReqBo;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectEducationListBusiRspBo;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectEudationListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:13
 */
@HTServiceAPI
public interface GraduationSelectEducationListBusiService {
    GraduationSelectEducationListBusiRspBo selectList(GraduationSelectEducationListBusiReqBo reqBo);
}
