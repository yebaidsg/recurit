package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationUpdateProfessionalSkillsAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:50
 */
@Data
public class GraduationUpdateProfessionalSkillsAbilityReqBO extends ReqBaseBo {
    private Long personId;
    private String skillName;
    private String usedTime;
    private String mastery;
    private Long skillsId;
}
