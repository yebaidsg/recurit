package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GradeAddCertificateBusiRspBO;

/**
 * @标题 GradeAddCertificateBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:21
 */
public interface GradeAddCertificateBusiService {
    GradeAddCertificateBusiRspBO addCertificate(GradeAddCertificateBusiReqBO reqBO);
}
