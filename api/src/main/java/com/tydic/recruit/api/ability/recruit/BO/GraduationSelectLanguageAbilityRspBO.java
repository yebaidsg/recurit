package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectLanguageAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 17:06
 */
@Data
public class GraduationSelectLanguageAbilityRspBO extends RspBaseBo {
    private Long personId;
    private String languages;
    private String lisSpeAbility;
    private String readWriteAbility;
    private Long languageId;
}
