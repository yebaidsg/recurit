package com.tydic.recruit.api.order.service;

import com.ohaotian.plugin.base.exception.ZTBusinessException;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author 胡中宝
 * @Description TODO
 * @copyright 2020 www.tydic.com Inc. All rights reserved.
 * 注意：本内容仅限于北京天源迪科网络科技有限公司内部传阅，禁止外泄以及用于其他的商业目的
 * @since 2020/10/20 14:23
 */
@HTServiceAPI
public interface InsertOrderService {


    /**
     * 生成子订单表
     * @param insertOrderReqBO
     * @throws ZTBusinessException
     */
//    void insert(InsertOrderReqBO insertOrderReqBO) throws ZTBusinessException;
}
