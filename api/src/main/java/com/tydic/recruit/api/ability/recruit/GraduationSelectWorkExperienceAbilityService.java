package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectWorkExperienceAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectWorkExperienceAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectWorkExperienceAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/24 15:08
 */
@HTServiceAPI
public interface GraduationSelectWorkExperienceAbilityService {
    GraduationSelectWorkExperienceAbilityRspBO select(GraduationSelectWorkExperienceAbilityReqBO reqBO);
}
