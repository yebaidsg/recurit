package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateResumeBusiRspBO;

/**
 * @标题 GraduationRecruitUpdateResumeBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:18
 */
public interface GraduationRecruitUpdateResumeBusiService {
    GraduationRecruitUpdateResumeBusiRspBO updateResume(GraduationRecruitUpdateResumeBusiReqBO reqBO);
}
