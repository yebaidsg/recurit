package com.tydic.recruit.api.ability.publicModel.bo;

import com.ohaotian.plugin.base.bo.RspPage;
import lombok.Data;

/**
 * @标题 GraduationSelectDictionaryListAbilityRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/4 15:30
 */
@Data
public class GraduationSelectDictionaryListAbilityRspBO extends RspPage<GraduationSelectDictionaryBO> {
}
