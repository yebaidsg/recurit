package com.tydic.recruit.api.ability.publicModel.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * @标题 ProvinceBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/12 22:51
 */
@Data
public class ProvinceBO implements Serializable {
    private String provinceCode;
    private String provinceName;
}
