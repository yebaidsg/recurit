package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceListBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationSelectWorkExperienceListBusiRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @author huzb
 * @标题 GraduationSelectWorkExperienceListBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:22
 */
@HTServiceAPI
public interface GraduationSelectWorkExperienceListBusiService {
    GraduationSelectWorkExperienceListBusiRspBO selectWorList(GraduationSelectWorkExperienceListBusiReqBO reqBO);
}
