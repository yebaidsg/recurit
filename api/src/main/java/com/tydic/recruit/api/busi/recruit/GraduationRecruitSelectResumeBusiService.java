package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitSelectResumeBusiRspBO;

/**
 * @标题 GraduationRecruitSelectResumeBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/20 23:25
 */
public interface GraduationRecruitSelectResumeBusiService {
    GraduationRecruitSelectResumeBusiRspBO selectResume(GraduationRecruitSelectResumeBusiReqBO reqBO);
}
