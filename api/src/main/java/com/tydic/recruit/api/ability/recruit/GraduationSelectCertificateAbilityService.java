package com.tydic.recruit.api.ability.recruit;

import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectCertificateAbilityReqBO;
import com.tydic.recruit.api.ability.recruit.BO.GraduationSelectCertificateAbilityRspBO;
import lombok.extern.ohaotian.HTServiceAPI;

/**
 * @标题 GraduationSelectCertificateAbilityService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 22:39
 */
@HTServiceAPI
public interface GraduationSelectCertificateAbilityService {
    GraduationSelectCertificateAbilityRspBO selectCertificate(GraduationSelectCertificateAbilityReqBO reqBO);
}
