package com.tydic.recruit.api.busi.recruit.bo;

import com.tydic.recruit.common.base.bo.RspBaseBo;
import lombok.Data;

/**
 * @标题 GraduationRecruitUpdateJobIntentionalBusiRspBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 0:33
 */
@Data
public class GraduationRecruitUpdateJobIntentionalBusiRspBO extends RspBaseBo {
}
