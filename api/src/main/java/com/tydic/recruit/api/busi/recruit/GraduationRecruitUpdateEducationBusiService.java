package com.tydic.recruit.api.busi.recruit;

import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiReqBO;
import com.tydic.recruit.api.busi.recruit.bo.GraduationRecruitUpdateEducationBusiRspBO;

/**
 * @标题 GraduationRecruitUpdateEducationBusiService
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/22 1:07
 */
public interface GraduationRecruitUpdateEducationBusiService {
    GraduationRecruitUpdateEducationBusiRspBO updateEducation(GraduationRecruitUpdateEducationBusiReqBO reqBO);
}
