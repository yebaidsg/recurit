package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huzb
 * @标题 WorkExperienceListBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/5 16:23
 */
@Data
public class WorkExperienceListBO implements Serializable {
    private String workId;
    /**
     * 简历编号
     */
    private Long personId;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 所属行业一级编码
     */
    private String workFirstIndustryCode;
    /**
     * 一级名称
     */
    private String workFirstIndustryName;
    /**
     * 所属行业二级编码
     */
    private String workSecondIndustryCode;
    /**
     * 所属行业二级名称
     */
    private String workSecondIndustryName;
    /**
     * 职位名称
     */
    private String titlePosition;
    /**
     * 入职时间
     */
    private String entryTime;
    /**
     * 离职时间
     */
    private String departureTime;
    /**
     * 当前月星
     */
    private String monthlySalary;
    /**
     * 工作描述
     */
    private String jobDescription;
}
