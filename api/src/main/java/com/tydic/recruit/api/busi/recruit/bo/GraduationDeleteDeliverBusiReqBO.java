package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationDeleteDeliverBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 0:03
 */
@Data
public class GraduationDeleteDeliverBusiReqBO extends ReqInfo {
    private Integer deliverId;
}
