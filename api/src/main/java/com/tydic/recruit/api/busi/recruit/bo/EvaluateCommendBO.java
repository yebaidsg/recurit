package com.tydic.recruit.api.busi.recruit.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 EvaluateCommendBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/25 22:39
 */
@Data
public class EvaluateCommendBO {
    private String userId;
    private String evaluateStart;
    private Integer positionId;
}
