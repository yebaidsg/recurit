package com.tydic.recruit.api.busi.recruit.bo;

import com.ohaotian.plugin.base.bo.ReqInfo;
import lombok.Data;

/**
 * @author huzb
 * @标题 GraduationSelectEvaluateBusiReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/24 22:09
 */
@Data
public class GraduationSelectEvaluateBusiReqBO extends ReqInfo {
    private Long userId;
}
