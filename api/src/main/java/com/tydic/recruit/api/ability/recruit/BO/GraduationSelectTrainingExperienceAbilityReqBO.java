package com.tydic.recruit.api.ability.recruit.BO;

import com.tydic.recruit.common.base.bo.ReqBaseBo;
import lombok.Data;

/**
 * @标题 GraduationSelectTrainingExperienceAbilityReqBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/4/25 15:57
 */
@Data
public class GraduationSelectTrainingExperienceAbilityReqBO extends ReqBaseBo {
    private Long personId;
    private Long trainingId;
}
