package com.ozo.ability;

import com.ozo.ability.bo.ImageBO;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ImageService {

    List<ImageBO> findAllImage();

    List<ImageBO> findImageByCondition(ImageBO image);

    ImageBO findImageByconnectId(@Param("connectId") Long connectId);

    ImageBO findImageById(@Param("imageId") Long imageId);

    ImageBO findImageByName(@Param("imageName") String imageName);

    void insertImage(ImageBO image);

    void deleteImage(Long id);

    void updateImage(ImageBO image);

    void updateImageByConnectId(ImageBO image);
}
