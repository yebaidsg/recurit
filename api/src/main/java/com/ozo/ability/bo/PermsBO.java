package com.ozo.ability.bo;

import lombok.Data;

import java.util.List;

/**
 * @author huzb
 * @标题 PermsBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/9 19:11
 */
@Data
public class PermsBO {
    private Long permsId;
    private String permsName;
    private String permsDescribe;
    private String permission;
    private List<RoleBO> roles;
}
