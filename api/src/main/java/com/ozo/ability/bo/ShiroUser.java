package com.ozo.ability.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

public class ShiroUser implements Serializable {

    private Long userId;
    private String name;
    private String password;
    private List<String> roles;
    private String avatar;
    private String token;
    private String authCacheKey;

    public ShiroUser(Long userId, String name, String password, List<String> roles, String avatar, String token, String authCacheKey) {
        this.userId = userId;
        this.name = name;
        this.password = password;
        this.roles = roles;
        this.avatar = avatar;
        this.token = token;
        this.authCacheKey = authCacheKey;
    }

    public ShiroUser() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthCacheKey() {
        return authCacheKey;
    }

    public void setAuthCacheKey(String authCacheKey) {
        this.authCacheKey = authCacheKey;
    }
}
