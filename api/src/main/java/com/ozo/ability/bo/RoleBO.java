package com.ozo.ability.bo;

import lombok.Data;

import java.util.List;

/**
 * @author huzb
 * @标题 RoleBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/9 19:12
 */
@Data
public class RoleBO {
    private Long roleId;
    private String roleName;
    private String roleDescribe;
    private List<UserBO> users;
}
