package com.ozo.ability.bo;

import lombok.Data;

/**
 * @author huzb
 * @标题 ImageBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/9 19:04
 */
@Data
public class ImageBO {
    private Long imageId;
    private String imageName;
    private Long connectId;
    private String imageUrl;
}
