package com.ozo.ability.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huzb
 * @标题 UserBO
 * @说明 TODO
 * @作者 胡中宝
 * @时间 2021/5/9 19:13
 */
@Data
public class UserBO implements Serializable {
    private Long userId;
    private String username;
    private String password;
    private String userPhone;
    private String userStatus;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date userEnrollTime;

}
