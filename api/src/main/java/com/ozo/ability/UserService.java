package com.ozo.ability;

import com.ohaotian.plugin.base.bo.RspBaseBO;
import com.ozo.ability.bo.UserBO;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public
interface UserService {

     List<UserBO> findAllUser();

     UserBO findLoginUser(UserBO user);

     UserBO findUserByName(String username);

     List<UserBO> findUserByCondition(UserBO user);

     List<UserBO> findUserByUsername(@Param("username") String username);

     RspBaseBO insertUser(UserBO user);

     void insertUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

     void deleteUser(Long id);

     void updateUser(UserBO user);
}

