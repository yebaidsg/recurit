package com.ozo.ability;



import com.ozo.ability.bo.RoleBO;

import java.util.List;

public interface RoleService {

    List<RoleBO> findRoleByUserId(Long id);

    List<RoleBO> findAllRole();

    List<RoleBO> findRoleByUsername(String username);

    void insertRole(RoleBO role);

    void deleteRole(Long id);

    void updateRole(RoleBO role);

    void updateRoleById(RoleBO role, Long id);

}
