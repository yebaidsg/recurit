package com.ozo.ability;

import com.ozo.ability.bo.PermsBO;

import java.util.List;

public interface PermsService {

    List<PermsBO> findPermsByRoleId(Long id);

    List<PermsBO> findAllPerms();
}
